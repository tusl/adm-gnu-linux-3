# Directorio estilos #

En este directorio se cuenta con el archivo de estilos tusl.style utilizado por todas las materias de la tecnicatura. Este archivo es el utilizado por los scripts de generación para realizar la conversión a PDF.

## Comando de conversión a PDF con uso de un estilo particular ##

```bash

rst2pdf -s tusl.style entrada.rst -o salida.pdf

```
