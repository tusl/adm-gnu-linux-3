# Directorio unidades #

Este directorio contiene un archivo .rst por cada unidad de la materia. Los mismos son utilizados, junto al archivo _estilos/tusl.style_ y los archivos de imagenes en la carpeta _imagenes_ para generar los archivos PDFs que serán alojados en el directorio _pdfs/unidades/_