﻿.. image:: ../caratulas/unidad01.pdf
	:scale: 86%

Introducción
============

En esta materia daremos continuidad a lo aprendido en *Administración
GNU/Linux I y II*, profundizando en un conjunto de herramientas y
conceptos que están presentes en la vida diaria de un administrador de
sistemas.

En particular, haremos especial énfasis en los sistemas de
almacenamiento; la implementación de políticas de seguridad en una red
por medio de cortafuegos (firewalls); la instauración de redes privadas
virtuales para poder brindar un servicio de teletrabajo (entre otras
cosas); la monitorización de recursos y servicios que nos permitan
predecir futuros problemas, o nos alerten de la caída de uno de los
servicios brindados, etc. Veremos también una de las tecnología más
utilizadas en la actualidad, la virtualización, la que nos permitirá
entre otras cosas aprovechar al máximo los recursos de los servidores, y también
abordaremos una herramienta que nos va a permitir automatizar el proceso de
despliegue (deploys) de nuevas aplicaciones o sistemas.

Si bien todas las distribuciones GNU/Linux modernas soportan las
tecnologías que desarrollaremos, por una cuestión de practicidad nos
enfocaremos en la distribución Debian 9.3 (Strech). Independientemente
de esto, contar con los conceptos claros ayuda y mucho a la hora de
trabajar con una distribución u otra, dado que por lo general son muy
pocos los cambios entre estas. Es nuestro objetivo hacer foco en los conceptos,
y dotarlos de herramientas que les simplifiquen y agilicen el trabajo a la
hora de administrar sistemas GNU/Linux.

Bienvenidos…

Unidad 1: Sistemas de almacenamiento de datos (RAID y LVM)
==========================================================

Tanto el sistema operativo como los datos generados o servidos por el
mismo, deben alojarse en uno o más dispositivos de almacenamiento. Los
conocemos como volúmenes y se representan mediante dispositivos de
bloque, que se encuentran dentro del directorio /dev (por ejemplo
/dev/sda ó /dev/hda). Todos los dispositivos, independientemente de su
tecnología, con el tiempo fallan, ocasionando una falta de
disponibilidad de los datos y servicios alojados en los mismos.

Lamentablemente es muy difícil predecir cuando estos fallarán, e incluso
si no fallaran, con el tiempo los recursos ofrecidos por estos
dispositivos pueden ser insuficientes para las demandas de los sistemas
(poco espacio disponible, baja performance de lectura o escritura,
etc.).

Las tecnologías que abordaremos en esta unidad vienen a dar soluciones a
estos problemas, permitiéndonos una mayor flexibilidad para acompañar el
crecimiento y la vida de los servicios alojados en nuestros servidores,
como así también la posibilidad de afrontar contingencias (como la falla
espontánea de un disco rígido) sin pérdida de servicio. Las tecnologías
en cuestión son RAID y LVM.

Tanto RAID como LVM son técnicas para abstraer los volúmenes montados de
sus correspondientes dispositivos físicos (discos duros reales o
particiones de los mismos). El primero protege los datos contra fallos
de hardware agregando redundancia mientras que el segundo hace más
flexible la gestión de los volúmenes y los independiza del tamaño real
de los discos subyacentes. En ambos casos se crean nuevos dispositivos
de bloques en el sistema que pueden ser utilizados, tanto para crear
sistemas de archivos, como espacios de intercambio, sin necesidad de que
se asocien a un disco físico concreto. RAID y LVM tienen orígenes
bastante diferentes pero su funcionalidad a veces se solapa, por lo que
a menudo se mencionan juntos.

RAID
----

El acrónimo RAID (Redundant Array of Independent Disks), traducido como
«conjunto redundante de discos Independientes» es un sistema que se
utiliza para agrupar dos o más volúmenes físicos y considerarlos como un
único volumen lógico con ciertos beneficios. Inicialmente fue pensado
para evitar pérdida de datos en caso de fallas de uno o más discos
duro, pero como veremos, también puede aportar otros beneficios. El
principio general es bastante simple: se almacenan los datos en varios
discos físicos en lugar de sólo uno, con un nivel de redundancia
configurable. Dependiendo de esta cantidad de redundancia, y aún en caso
de fallo inesperado del disco, se puede reconstruir los datos que el mismo
contenía desde los discos restantes, de forma totalmente automática.

Implementaciones de RAID
~~~~~~~~~~~~~~~~~~~~~~~~

Se puede implementar RAID tanto con hardware dedicado (módulos RAID
integrados en las tarjetas controladoras de discos) o por software (un
módulo del núcleo). Ya sea por hardware o software, un sistema RAID con
suficiente redundancia puede mantenerse operativo de forma transparente
cuando fallen uno o más disco; las capas superiores (las aplicaciones) inclusive
pueden seguir accediendo a los datos a pesar del fallo.

Cuando se implementa RAID con hardware, generalmente se configura desde
la herramienta de gestión del BIOS y el núcleo tratará el RAID como un
solo disco que funcionará como un disco físico estándar, aunque el
nombre del dispositivo podría ser diferente.

En el caso de implementaciones de RAID por software, es el núcleo(kernel) el
encargado de realizar la abstracción necesaria y administrar el RAID.
Esta implementación ofrece una mayor flexibilidad, dado que permite
conformar incluso RAIDs entre particiones de un mismo disco (una misma
controladora). Más adelante veremos como realizar esta tarea en
GNU/Linux, mediante el uso de la herramienta mdadm.

Niveles de RAID
~~~~~~~~~~~~~~~

Las diferentes configuraciones de RAID se denominan niveles. Y cada una
de ellas ofrecen diferentes beneficios, tanto en lo referente a
redundancia, como en el acceso a disco, mejorando la performance de
lectura ó escritura. Existen numerosos niveles de RAID, por
una cuestión de practicidad, solo haremos énfasis en los más utilizados.

RAID 0
^^^^^^

En este tipo de RAID los datos se dividen en tiras y se guardan
equitativamente entre los discos que conforman el RAID. Es decir, si
tenemos un RAID 0 de 2 discos, a la hora de guardar un archivo, este se
va a dividir en un número par de tiras, guardando las tiras pares en un
disco, y las impares en el otro.

.. figure:: ../imagenes/unidad01/image_1.png
   :alt: RAID0
   :align: center

   Fig.1 - RAID 0

Se utiliza principalmente para proporcionar un mejor rendimiento de
lectura (NOTE: Siempre y cuando la lectura sea secuencial.) y escritura
ya que los datos se recuperan de dos o más discos en simultáneo (se
realiza en paralelo la lectura y escritura, por lo que cada disco solo
debe realizar la mitad del trabajo). Como desventaja, este tipo de RAID
**no ofrece redundancia**, dado que cada tira se almacena en un solo
disco, por lo que **ante la falla de uno de los discos del raid, todos los
datos se pierden** (solo tendríamos la mitad de cada archivo). RAID
0 también puede utilizarse como forma de crear un pequeño número de
grandes discos virtuales a partir de un gran número de pequeños discos
físicos. Una buena implementación de un RAID 0 dividirá las operaciones
de lectura y escritura en bloques de igual tamaño, por lo que
distribuirá la información equitativamente entre los dos discos.

RAID 1
^^^^^^

Este nivel, también conocido como "espejado RAID" (mirroring) es la
configuración más simple y una de las más utilizadas. En su forma
estándar, utiliza dos discos físicos del mismo tamaño y provee un
volumen lógico nuevamente del mismo tamaño. Los datos se almacenan en
ambos discos de forma idéntica (se duplican), de ahí el apodo de
espejado.

.. figure:: ../imagenes/unidad01/image_2.png
   :alt: RAID1
   :align: center

   Fig.2 - RAID1

Cuando uno de los discos falla, los datos continúan disponibles en el otro.
Durante este evento el administrador del sistema puede ser notificado
del incidente, e intervenir para reemplazar el disco que ha fallado.

Una vez reemplazado el disco que ha fallado, el RAID nuevamente se puede
rearmar para brindar redundancia. Incluso en configuraciones de discos
hot-plug (de reemplazo en caliente), se pueden reemplazar los discos
defectuosos sin necesidad de reiniciar el sistema, simplemente se quitan
y reemplazan por discos nuevos.

La principal desventaja de este nivel de raid es su costo, dado que se
incrementa por 2. Imaginemos que tenemos un servidor con 8 discos de
1TB, si implementáramos RAID 1, estaríamos desperdiciando la mitad de
los discos en favor de la redundancia (solo tendríamos una capacidad de
almacenamiento de 4TB en vez de 8TB). Como veremos más adelante, existen
otros niveles que ofrecen una mejor relación costo/beneficio, sin dejar
de lado la redundancia.

RAID 5
^^^^^^

Este tipo de RAID utiliza un método de detección de errores conocido
como *Paridad*. Existen diferentes forma de calcular la paridad, pero la
idea siempre es que si en un conjunto de datos (un RAID en este caso),
falta uno de ellos (supongamos un bloque alojado en uno de los discos),
este se puede reconstruir a partir de los datos restantes del conjunto, y
la paridad.

Para entender que es la paridad, podemos asemejarla a una ecuación
algebraica sencilla en la que la Paridad es el cálculo de la suma de
todos los datos (en la práctica será un checksum de datos). Si se
produce un error en un disco ese dato pasa a ser una incógnita que se
puede calcular despejándola de la ecuación. Por ejemplo:

.. code:: bash

  A + B + C = PARIDAD

Si A = 1, B = 2 y C = 3 entonces

.. code:: bash

  1 + 2 + 3 = 6

Supongamos ahora que el dato B se ha perdido, entonces, contando con el
dato A, el dato C y la PARIDAD, podemos obtener el valor de B de la
ecuación:

.. code:: bash

  1 + B + 3 = 6,

que despejando nos da

.. code:: bash

  B = 6 - 1 - 3

es decir

.. code:: bash

 B = 2

Una vez entendido esto, podemos decir que un RAID 5 es una división de
datos a nivel de bloques que distribuye la información de paridad entre
todos los discos miembros del conjunto. Es decir, los datos en primer
instancia se dividen en N - 1 bloques, donde N es la cantidad de discos
que conforman el RAID, sobre este conjunto se calcula la paridad, y esta
información (los bloques y la paridad) se distribuye entre los
diferentes discos. Por ejemplo supongamos que tenemos 4 discos, en este
caso el dato se divide en 3 bloques, se calcula la paridad, y cada una
de las partes (los 3 bloques de datos y la paridad) se envían a discos
diferentes.

.. figure:: ../imagenes/unidad01/image_3.png
   :alt: RAID5
   :align: center

   Fig.3 - RAID5

De este modo y aplicando la técnica antes mencionada, si uno de los
discos falla, con los discos restantes se puede reconstruir el valor
faltante por lo que el sistema sigue funcionando sin problemas.

El RAID 5 ha logrado popularidad gracias a su bajo coste de redundancia.
Como desventaja, necesita un mínimo de 3 discos para ser implementado.
Este tipo de RAID además de la redundancia, aporta una mejor performance
de lectura. Por ejemplo, si observamos la figura anterior, una petición
de lectura del bloque "A1" sería servida por el disco 0. Una petición de
lectura simultánea del bloque "B1" tendría que esperar, pero una
petición de lectura de "B2" podría atenderse concurrentemente ya que
sería servida por el disco 1. Como desventaja, cada vez que un bloque de
datos se escribe ó modifica en un RAID 5, se genera un bloque de paridad
dentro de la misma división (tira). Por este motivo, generalmente el
RAID 5 se implementa con soporte hardware para el cálculo de la paridad.
Las escrituras en un RAID 5 son costosas en términos de operaciones de
disco y tráfico entre los discos y la controladora. Los bloques de
paridad no se leen en las operaciones de lectura de datos, ya que esto
sería una sobrecarga innecesaria y disminuiría el rendimiento. Sin
embargo, los bloques de paridad se leen cuando la lectura de un sector
de datos provoca un error de CRC. En este caso, el sector en la misma
posición relativa dentro de cada uno de los bloques de datos restantes
en la división y dentro del bloque de paridad en la división se utilizan
para reconstruir el sector erróneo. El error CRC se oculta así al resto
del sistema. De la misma forma, si falla un disco del conjunto, los
bloques de paridad de los restantes discos son combinados
matemáticamente con los bloques de datos de los restantes discos para
reconstruir los datos del disco que ha fallado "al vuelo". El fallo de
un segundo disco provoca la pérdida completa de los datos.

Modo degradado
~~~~~~~~~~~~~~

Se denomina modo degradado al estado del RAID cuando ha fallado al menos
uno de los discos, y por consiguiente el ARRAY realiza la reconstrucción
de la información a partir de los discos restantes que conforman el
arreglo. Por supuesto, este «modo degradado» puede tener un impacto en
el rendimiento y se reduce la redundancia, por lo que otro fallo de
disco puede llevar a la pérdida de datos. En la práctica uno intentará
estar en este modo sólo el tiempo que tome reemplazar el disco
defectuoso. Una vez que se instala el nuevo disco, el sistema RAID puede
reconstruir los datos necesarios para volver a un modo seguro. Las
aplicaciones no notan cambio alguno, además de la posible disminución en
la velocidad de acceso, mientras que el array esté en modo degradado o
durante la fase de reconstrucción.

Discos de reserva (spare)
~~~~~~~~~~~~~~~~~~~~~~~~~

Todas las implementaciones pueden soportar el uso de uno o más discos de
reserva, unidades preinstaladas que pueden usarse inmediatamente, y casi
siempre automáticamente, tras el fallo de un disco del RAID. Esto reduce
notablemente el tiempo en el que se está en modo degradado, dado que
inmediatamente se comienza la reconstrucción del RAID, y por lo tanto se
vuelve a contar con redundancia.

Ejemplo práctico: Creando RAID por software con mdadm
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si bien la mayoría de las distribuciones permiten crear RAIDs durante el
proceso de instalación de una manera muy sencilla e intuitiva, veremos
como realizar esta tarea una vez que contamos con un sistema operativo
instalado y funcionando. La herramienta principal para crear y
administrar RAIDs en GNU/Linux es mdadm (Multiple Device Administrator).
Su uso es muy sencillo como veremos a continuación con un ejemplo
práctico.

Para simplificar las cosas, utilizaremos virtualbox y vagrant. El primero como
habrán visto en materias anteriores, nos permite crear y administrar maquinas
virtuales, el segundo, nos permite "simplificar" ese proceso de creación de
modo de contar con una maquina virtual según nuestras especificaciones, en
cuestión de minutos y sin tener que instalar o configurar nada. Veremos Vagrant
mas adelante, por lo que de momento no entraremos en detalles acerca de su
funcionamiento, sino simplemente veremos mínimamente como utilizarlo.

Empecemos por instalar todo lo necesarios

.. code:: bash

	sudo apt install vagrant virtualbox git

Luego debemos crear una carpeta con cualquier nombre, supongamos
"maquina-virtual-01", ingresamos dentro de la misma y descargamos el
siguiente archivo de https://gitlab.com/tusl/adm-gnu-linux-3/raw/master/maquinas-virtuales/unidad01/ejemplopracticoRAID/Vagrantfile

.. code:: bash

	mboscovich@tesla:~$ mkdir maquina-virtual-01
	mboscovich@tesla:~$ cd maquina-virtual-01
	mboscovich@tesla:~/maquina-virtual-01$ wget https://gitlab.com/tusl/adm-gnu-linux-3/raw/master\
	/maquinas-virtuales/unidad01/ejemplopracticoRAID/Vagrantfile

por último ejecutamos

.. code:: bash

	mboscovich@tesla:~/maquina-virtual-01$ vagrant up
	Bringing machine 'default' up with 'virtualbox' provider...
	==> default: Box 'mboscovich/AdmGNULinux3-TUSL-MV1' could not be found. Attempting to find and install...
	    default: Box Provider: virtualbox
	    default: Box Version: >= 0
	==> default: Loading metadata for box 'mboscovich/AdmGNULinux3-TUSL-MV1'
	    default: URL: https://vagrantcloud.com/mboscovich/AdmGNULinux3-TUSL-MV1
	==> default: Adding box 'mboscovich/AdmGNULinux3-TUSL-MV1' (v1.0) for provider: virtualbox
	    default: Downloading: https://vagrantcloud.com/mboscovich/boxes/AdmGNULinux3-TUSL-MV1/versions/1.0/providers/virtualbox.box
	==> default: Box download is resuming from prior download progress
	==> default: Successfully added box 'mboscovich/AdmGNULinux3-TUSL-MV1' (v1.0) for 'virtualbox'!
	==> default: Importing base box 'mboscovich/AdmGNULinux3-TUSL-MV1'...
	==> default: Matching MAC address for NAT networking...
	==> default: Checking if box 'mboscovich/AdmGNULinux3-TUSL-MV1' is up to date...
	==> default: Setting the name of the VM: AdmGNULinux3-TUSL-MV1
	==> default: Clearing any previously set network interfaces...
	==> default: Preparing network interfaces based on configuration...
	    default: Adapter 1: nat
	    default: Adapter 2: hostonly
	==> default: Forwarding ports...
	    default: 22 (guest) => 2222 (host) (adapter 1)
	==> default: Running 'pre-boot' VM customizations...
	==> default: Booting VM...
	==> default: Waiting for machine to boot. This may take a few minutes...
	    default: SSH address: 127.0.0.1:2222
	    default: SSH username: vagrant
	    default: SSH auth method: private key
	    default:
	    default: Vagrant insecure key detected. Vagrant will automatically replace
	    default: this with a newly generated keypair for better security.
	    default:
	    default: Inserting generated public key within guest...
	    default: Removing insecure key from the guest if it's present...
	    default: Key inserted! Disconnecting and reconnecting using new SSH key...
	==> default: Machine booted and ready!
	==> default: Checking for guest additions in VM...
	    default: The guest additions on this VM do not match the installed version of
	    default: VirtualBox! In most cases this is fine, but in rare cases it can
	    default: prevent things such as shared folders from working properly. If you see
	    default: shared folder errors, please make sure the guest additions within the
	    default: virtual machine match the version of VirtualBox you have installed on
	    default: your host and reload your VM.
	    default:
	    default: Guest Additions Version: 5.2.6
	    default: VirtualBox Version: 5.1
	==> default: Configuring and enabling network interfaces...
	==> default: Mounting shared folders...
	    default: /vagrant => /home/mboscovich/maquina-virtual-01

Puede que el comando anterior demore un poco la primera vez, dado que debe
descargar de internet unos 440Mb, pero una vez que termine nos creara en
Virtualbox un Debian 9.3 con 2 discos de 10Gbytes cada uno, listo para que
empecemos a trabajar con el.

El usuario que utilizaremos en esta maquina virtual será vagrant, y la password
es vagrant. Este usuario cuenta con permisos de sudo, por lo que podremos
instalar y configurar todo como si fueramos root.

Para conectarnos a la maquina virtual podemos utilizar el visor de consola de
Virtualbox, ó podemos desde nuestra maquina conectarnos via ssh a la IP
192.168.33.10

.. code:: bash

	mboscovich@tesla:~/maquina-virtual-01$ ssh vagrant@192.168.33.10
	vagrant@192.168.33.10's password:
	Linux stretch 4.9.0-5-amd64 #1 SMP Debian 4.9.65-3+deb9u2 (2018-01-04) x86_64

	The programs included with the Debian GNU/Linux system are free software;
	the exact distribution terms for each program are described in the
	individual files in /usr/share/doc/*/copyright.

	Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
	permitted by applicable law.
	Last login: Tue Feb 13 14:26:27 2018 from 10.0.2.2
	vagrant@stretch:~$


Partimos de un sistema funcionando con un disco rígido de 10Gbytes y 3
particiones, la primera utilizada para montar el sistema de archivos
raíz, la segunda es la partición extendida, y la tercera es utilizada para el
área de intercambio.

.. code:: bash

	vagrant@stretch:~$ sudo fdisk -l /dev/sda
	Disk /dev/sda: 9.9 GiB, 10632560640 bytes, 20766720 sectors
	Units: sectors of 1 * 512 = 512 bytes
	Sector size (logical/physical): 512 bytes / 512 bytes
	I/O size (minimum/optimal): 512 bytes / 512 bytes
	Disklabel type: dos
	Disk identifier: 0x0ed2170f

	Device     Boot    Start      End  Sectors  Size Id Type
	/dev/sda1  *        2048 18669567 18667520  8.9G 83 Linux
	/dev/sda2       18671614 20764671  2093058 1022M  5 Extended
	/dev/sda5       18671616 20764671  2093056 1022M 82 Linux swap / Solaris

A su vez dicha maquina virtual cuenta con un disco extra
de igual tamaño que el primero, pero sin ninguna partición de momento.

.. code:: bash

	vagrant@stretch:~$ sudo fdisk -l /dev/sdb
	Disk /dev/sdb: 9.9 GiB, 10632560640 bytes, 20766720 sectors
	Units: sectors of 1 * 512 = 512 bytes
	Sector size (logical/physical): 512 bytes / 512 bytes
	I/O size (minimum/optimal): 512 bytes / 512 bytes
	Disklabel type: dos
	Disk identifier: 0x0ed2170f

Nuestro objetivo sera armar un RAID1, espejando el disco completo (todas las
particiones). De este modo practicaremos el caso donde debemos partir de un
sistema funcionando sin redundancia, le agregamos un disco extra y armamos un
RAID1 (espejo) para que dicho servidor cuente con esta característica.

En primer lugar debemos instalar mdadm y rsync (lo utilizaremos luego)

.. code:: bash

	vagrant@stretch:~$ sudo apt update
	vagrant@stretch:~$ sudo apt install mdadm rsync

Cargamos algunos módulos necesarios del kernel

.. code:: bash

	vagrant@stretch:~$ sudo modprobe linear
	vagrant@stretch:~$ sudo modprobe raid1

En segundo lugar debemos estar seguro que disco es el que tiene el
sistema raíz, y que disco es el nuevo que se ha anexado. La manera
sencilla de ver esto es por medio del comando df -h /

.. code:: bash

	vagrant@stretch:~$ df -h /
	Filesystem      Size  Used Avail Use% Mounted on
	/dev/sda1       8.7G  1.4G  7.0G  16% /

Como vemos, el dispositivo /dev/sda contiene el dispositivo actualmente
en uso. Si hacemos un

.. code:: bash

	vagrant@stretch:~$ ls -l /dev/sd*
	brw-rw---- 1 root disk 8,  0 Feb 13 15:37 /dev/sda
	brw-rw---- 1 root disk 8,  1 Feb 13 15:37 /dev/sda1
	brw-rw---- 1 root disk 8,  2 Feb 13 15:37 /dev/sda2
	brw-rw---- 1 root disk 8,  5 Feb 13 15:37 /dev/sda5
	brw-rw---- 1 root disk 8, 16 Feb 13 15:37 /dev/sdb

Veremos que el segundo disco fue representado como /dev/sdb, por lo que
/dev/sda es el disco que actualmente contiene la información (con las
particiones que queremos espejar) y sdb será el dispositivo sobre el que
replicaremos y mantendremos espejado el disco sda. En
primer lugar debemos replicar la tabla de particiones de manera exacta
entre ambos discos, previamente borrando cualquier información de
particiones que exista en el disco /dev/sdb

.. code:: bash

	vagrant@stretch:~$ sudo wipefs -a /dev/sdb
	vagrant@stretch:~$ sudo sfdisk -d /dev/sda | sudo sfdisk --force /dev/sdb

Ahora debemos cambiar el tipo de partición de las dos particiones del
disco /dev/sdb a "Linux raid autodetect". Esto se hace mediante el
comando fdisk

.. code:: bash

	vagrant@stretch:~$ sudo fdisk /dev/sdb

	Welcome to fdisk (util-linux 2.29.2).
	Changes will remain in memory only, until you decide to write them.
	Be careful before using the write command.


	Command (m for help): t
	Partition number (1,2,5, default 5): 1
	Partition type (type L to list all types): fd

	Changed type of partition 'Linux' to 'Linux raid autodetect'.

	Command (m for help): t
	Partition number (1,2,5, default 5): 5
	Partition type (type L to list all types): fd

	Changed type of partition 'Linux swap / Solaris' to 'Linux raid autodetect'.

	Command (m for help): wq
	The partition table has been altered.
	Calling ioctl() to re-read partition table.
	Syncing disks.

Debemos asegurarnos de que el disco no contenga información de
anteriores raid, si es un disco nuevo se puede omitir, pero nunca está
demás:

.. code:: bash

	vagrant@stretch:~$ sudo mdadm --zero-superblock /dev/sdb1
	mdadm: Unrecognised md component device - /dev/sdb1
	vagrant@stretch:~$ sudo mdadm --zero-superblock /dev/sdb5
	mdadm: Unrecognised md component device - /dev/sdb5

El mensaje "mdadm: Unrecognised md component device - /dev/sdb5" es
normal, y simplemente nos quiere decir que no se encontró información de
configuraciones de raid anteriores.

Ahora creamos el raid, le especificamos el nivel, le decimos que
particiones formarán parte, y le agregamos una opción llamada missing,
que quiere decir que uno de los dos discos por ahora falta
(el disco sda, que actualmente está en uso y por ende no lo podemos
utilizar).

.. code:: bash

	vagrant@stretch:~$ sudo mdadm --create /dev/md0 --level=1 --raid-disks=2 missing /dev/sdb1
	mdadm: Note: this array has metadata at the start and
	    may not be suitable as a boot device.  If you plan to
	    store '/boot' on this device please ensure that
	    your boot-loader understands md/v1.x metadata, or use
	    --metadata=0.90
	Continue creating array? y
	mdadm: Defaulting to version 1.2 metadata
	mdadm: array /dev/md0 started.
	vagrant@stretch:~$ sudo mdadm --create /dev/md1 --level=1 --raid-disks=2 missing /dev/sdb5
	mdadm: Note: this array has metadata at the start and
	    may not be suitable as a boot device.  If you plan to
	    store '/boot' on this device please ensure that
	    your boot-loader understands md/v1.x metadata, or use
	    --metadata=0.90
	Continue creating array? y
	mdadm: Defaulting to version 1.2 metadata
	mdadm: array /dev/md1 started.

El raid será identificado con los dispositivos de bloque /dev/md0 (que
hará referencia al espejo entre las particiones /dev/sda1 y /dev/sdb1) y
/dev/md1 (que será el espejo de las particiones sda5 y sdb5). A partir
de ahora no haremos más referencia a sda1 o sda5, sino que en su lugar
utilizaremos los dispositivos de bloque /dev/md0 y /dev/md1.

Una forma interesante de ver el estado del raid, es por medio del
archivo /proc/mdstat, el que almacena información del estado del raid

.. code:: bash

	vagrant@stretch:~$ cat /proc/mdstat
	Personalities : [linear] [raid1]
	md1 : active raid1 sdb5[1]
	      1045952 blocks super 1.2 [2/1] [_U]

	md0 : active raid1 sdb1[1]
	      9325568 blocks super 1.2 [2/1] [_U]

	unused devices: <none>

Si observamos con detenimiento, veremos que nos informa los raid en uso
(en este caso 2), el tipo de estos raid (raid1), y los dispositivos
asociados a los mismos, que por ahora solo son sdb1 y sdb5. Otro dato
interesante es lo que se observa al final donde dice "[ _U ]" esto
quiere decir que de los dos dispositivos que deberían estar asociados al
raid, solo uno de ellos forma parte del mismo (esto es debido a que
todavía no le hemos dicho que sda1 y sda2 deben formar parte).

El siguiente paso consiste en formatear los dispositivos con el mismo
sistema de archivos utilizados en las particiones sda.

.. code:: bash

	vagrant@stretch:~$ sudo mkfs.ext4 /dev/md0
	mke2fs 1.43.4 (31-Jan-2017)
	Creating filesystem with 2331392 4k blocks and 582912 inodes
	Filesystem UUID: 02c5fea9-86de-49f8-980e-4d96f57068b7
	Superblock backups stored on blocks:
		32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632

	Allocating group tables: done
	Writing inode tables: done
	Creating journal (16384 blocks): done
	Writing superblocks and filesystem accounting information: done

	vagrant@stretch:~$ sudo mkswap /dev/md1
	Setting up swapspace version 1, size = 1021.4 MiB (1071050752 bytes)
	no label, UUID=28eb58e7-bf95-4bd4-81e7-513ebf026b78

Ahora debemos actualizar la información del archivo
/etc/mdadm/mdadm.conf con la situación actual

.. code:: bash

	vagrant@stretch:~$ sudo su -c "mdadm --examine --scan >> /etc/mdadm/mdadm.conf"

Modificamos el archivo /etc/fstab, para que en el próximo inicio utilice
el dispositivo /dev/md0 para montar el /, y /dev/md1 para la swap. Para
esto comentamos las entradas anteriores y agregamos lo siguiente:

.. code:: bash

	# /etc/fstab: static file system information.
	#
	# Use 'blkid' to print the universally unique identifier for a
	# device; this may be used with UUID= as a more robust way to name devices
	# that works even if disks are added and removed. See fstab(5).
	#
	# <file system> <mount point>   <type>  <options>       <dump>  <pass>
	# / was on /dev/vda1 during installation
	#UUID=55871c45-7602-4627-b51c-71694a01c164 /               ext4    errors=remount-ro 0       1
	/dev/md0	/               ext4    errors=remount-ro 0       1
	# swap was on /dev/vda5 during installation
	#UUID=0fa20137-e558-4350-82f1-67cc070c32ba none            swap    sw              0       0
	/dev/md1 	none            swap    sw              0       0
	/dev/sr0        /media/cdrom0   udf,iso9660 user,noauto     0       0

Debemos obtener la versión del kernel actualmente en uso

.. code:: bash

	vagrant@stretch:~$ uname -a
	Linux stretch 4.9.0-5-amd64 #1 SMP Debian 4.9.65-3+deb9u2 (2018-01-04) x86_64 GNU/Linux


Con esta versión debemos crear un archivo llamado /etc/grub.d/09_swraid1_setup y poner lo siguiente

.. code:: bash

	#!/bin/sh -e
	cat << EOF

	menuentry 'Debian GNU/Linux, con Linux 4.9.0-5-amd64 y RAID' --class debian --class gnu-linux --class gnu --class os {
	insmod part_msdos
	insmod ext2
	insmod mdraid1x
	set root='(md/0)'
	linux /boot/vmlinuz-4.9.0-5-amd64 root=/dev/md0 net.ifnames=0 ro quiet
	initrd /boot/initrd.img-4.9.0-5-amd64
	}
	EOF

Como verán, a grub le decimos que el root debe montarlo desde /dev/md0.

NOTA: Es importante asegurarse que estas versiones corresponden al
kernel actualmente instalado, para lo que es recomendable asegurarse que
los archivos /boot/vmlinuz-4.9.0-5-amd64 y /boot/initrd.img-4.9.0-5-amd64
existan.

Luego debemos darle permisos de ejecución a dicho script

.. code:: bash

	vagrant@stretch:~$ sudo chmod a+x /etc/grub.d/09_swraid1_setup

Editamos el archivo /etc/default/grub y descomentamos las siguientes
líneas

.. code:: bash

  GRUB_DISABLE_LINUX_UUID=true
  GRUB_TERMINAL=console

A continuación creamos el archivo /etc/initramfs-tools/conf.d/mdadm y
ponemos lo siguiente para asegurarnos de que grub inicie incluso si el
RAID esta degradado

.. code:: bash

  BOOT_DEGRADED=true

Luego ejecutamos update-grub2, para que esta configuración sea tenida en
cuenta por grub

.. code:: bash

	vagrant@stretch:~$ sudo update-grub2
	Generating grub configuration file ...
	Found linux image: /boot/vmlinuz-4.9.0-5-amd64
	Found initrd image: /boot/initrd.img-4.9.0-5-amd64
	Found linux image: /boot/vmlinuz-4.9.0-4-amd64
	Found initrd image: /boot/initrd.img-4.9.0-4-amd64
	done

Actualizamos el ramdisk a la nueva situación

.. code:: bash

	vagrant@stretch:~$ sudo update-initramfs -u
	update-initramfs: Generating /boot/initrd.img-4.9.0-5-amd64

Instalamos grub en los dos discos (para asegurarnos de que esté
correctamente instalado y actualizado en el sector de inicio de los
mismos).

Para esto ejecutamos

.. code:: bash

	vagrant@stretch:~$ sudo dpkg-reconfigure grub-pc

Le damos aceptar a las tres primeras preguntas sobre los argumentos del
kernel y demás, y al finalizar seleccionamos los discos /dev/sda y /dev/sdb para
que se instale el grub.

Ahora debemos replicar la información que está en la partición /dev/sda1
en el dispositivo de bloques /dev/md0, de modo que podamos iniciar el
sistema desde esta partición (el raid), antes de anexar el disco
/dev/sda1 al array md0.

.. code:: bash

	vagrant@stretch:~$ sudo mkdir /mnt/md0
	vagrant@stretch:~$ sudo mount /dev/md0 /mnt/md0
	vagrant@stretch:~$ sudo rsync -auHx --exclude=/proc/* --exclude=/sys/* /* /mnt/md0
	vagrant@stretch:~$ sudo umount /mnt/md0

A continuación es tiempo de reiniciar y asegurarnos de que el sistema
levante con el nuevo kernel y su configuración de raid.

Una vez iniciado el sistema, debemos anexar el disco sda al raid. Para
esto procedemos de la siguiente manera:

Modificamos el tipo de partición tanto de sda1 como de sda5, tal como
hicimos con sdb1 y sdb5.

.. code:: bash

	vagrant@stretch:~$ sudo fdisk /dev/sda

	Welcome to fdisk (util-linux 2.29.2).
	Changes will remain in memory only, until you decide to write them.
	Be careful before using the write command.


	Command (m for help): t
	Partition number (1,2,5, default 5): 1
	Partition type (type L to list all types): fd

	Changed type of partition 'Linux' to 'Linux raid autodetect'.

	Command (m for help): t
	Partition number (1,2,5, default 5): 5
	Partition type (type L to list all types): fd

	Changed type of partition 'Linux swap / Solaris' to 'Linux raid autodetect'.

	Command (m for help): w
	The partition table has been altered.
	Calling ioctl() to re-read partition table.
	Syncing disks.

Luego debemos anexar dichas particiones al raid

.. code:: bash

	vagrant@stretch:~$ sudo mdadm --add /dev/md0 /dev/sda1
	mdadm: added /dev/sda1
	vagrant@stretch:~$ sudo mdadm --add /dev/md1 /dev/sda5
	mdadm: added /dev/sda5

Ahora si miramos el estado del raid veremos que se están empezando a
sincronizar los discos

.. code:: bash

	vagrant@stretch:~$ cat /proc/mdstat
	Personalities : [raid1] [linear] [multipath] [raid0] [raid6] [raid5] [raid4] [raid10]
	md0 : active raid1 sda1[2] sdb1[1]
	      9325568 blocks super 1.2 [2/1] [_U]
	      [=================>...]  recovery = 88.0% (8209408/9325568) finish=0.0min speed=206715K/sec

	md1 : active raid1 sda5[2] sdb5[1]
	      1045952 blocks super 1.2 [2/1] [_U]
	      	resync=DELAYED

	unused devices: <none>

Esta operación puede demorar según la cantidad de información que
contengan las particiones. Una vez que la sincronización ha terminado,
deberíamos ver algo como lo siguiente

.. code:: bash

	vagrant@stretch:~$ cat /proc/mdstat
	Personalities : [raid1] [linear] [multipath] [raid0] [raid6] [raid5] [raid4] [raid10]
	md0 : active raid1 sda1[2] sdb1[1]
	      9325568 blocks super 1.2 [2/2] [UU]

	md1 : active raid1 sda5[2] sdb5[1]
	      1045952 blocks super 1.2 [2/2] [UU]

	unused devices: <none>

Si observan ahora, los dos raid está conformados y sincronizados. Esto
lo podemos ver al observar donde dice **[2/2] [UU]** que básicamente
nos está diciendo que de 2 dispositivos que tiene el raid, los dos
están activos, y actualizados.

Ahora debemos actualizar nuevamente la información del archivo
/etc/mdadm/mdadm.conf con la situación final

.. code:: bash

		vagrant@stretch:~$ sudo su -c "mdadm --examine --scan >> /etc/mdadm/mdadm.conf"

Actualizamos nuevamente grub

.. code:: bash

	vagrant@stretch:~$ sudo grub-mkdevicemap -n
	vagrant@stretch:~$ sudo update-grub
	Generating grub configuration file ...
	Found linux image: /boot/vmlinuz-4.9.0-5-amd64
	Found initrd image: /boot/initrd.img-4.9.0-5-amd64
	Found linux image: /boot/vmlinuz-4.9.0-4-amd64
	Found initrd image: /boot/initrd.img-4.9.0-4-amd64
	done
	vagrant@stretch:~$ sudo grub-install /dev/sda ; sudo grub-install /dev/sdb
	Installing for i386-pc platform.
	Installation finished. No error reported.
	Installing for i386-pc platform.
	Installation finished. No error reported.

Con esto concluimos la configuración del RAID 1. Podemos hacer pruebas
desconectando uno de los discos y asegurándonos de que el sistema aún
continúa iniciando.

LVM (Gestor de volúmenes lógicos)
---------------------------------

LVM es un gestor de volúmenes lógicos, de allí su nombre (Logical Volume
Manager). Es otra forma de abstraer volúmenes lógicos de su soporte
físico, que a diferencia de RAID está enfocado en ofrecer mayor
flexibilidad en lugar de aumentar confiabilidad, de hecho LVM no nos
aporta redundancia y confiabilidad. Es común encontrar implementaciones
que combinan LVM y RAID, de modo de aprovechar todos los beneficios
aportados por ambos sistemas. Por un lado podemos brindar redundancia
por medio de un sistema RAID, y a su vez administrar este RAID como un
único volumen que puede ser dividido tantas veces sea necesario, por
medio de la flexibilidad que aporta LVM. LVM permite modificar un
volumen lógico de forma transparente a las aplicaciones; por ejemplo, es
posible agregar nuevos discos, migrar sus datos y eliminar discos
antiguos sin desmontar el volumen, ó también en caso de que nos
estemos quedando sin espacio en un servidor, y no podamos detenerlo,
gracias a LVM podemos conectar un nuevo disco y anexarlo para que nos
permita incrementar la capacidad y resolver el problema.

Conceptos
~~~~~~~~~

La flexibilidad aportada por LVM se consigue con un nivel de abstracción
que incluye tres conceptos que veremos a continuación, pero que se
encuentran representados en el siguiente diagrama.

.. figure:: ../imagenes/unidad01/image_4.jpg
   :alt: Conceptos de LVM
   :align: center
   :scale: 80%

   Fig.4 - Conceptos de LVM


Volumen Físico (Physical Volume, PV)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Es la entidad más cercana al hardware: pueden ser particiones en un
disco, un disco completo o inclusive cualquier dispositivo de bloque
(también un RAID, porque no).

Grupo de volúmenes (Volume Group, VG)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Los volúmenes físicos se agrupan en los denominados Grupos de volúmenes
(VG), lo que puede compararse con discos virtuales y extensibles. Los
VGs son abstractos y no aparecerán como un dispositivo de bloques en
/dev, simplemente son una abstracción para agrupar los distintos
dispositivos, y considerar a todos estos como un único disco virtual, un
gran disco que suma las capacidades aportadas por los dispositivos que
lo componen.

Volúmenes lógicos (Logical Volume, LV)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

El tercer tipo de objeto es el volumen lógico(LV) que es una porción de
un grupo de volúmenes(VG); si continuamos con la analogía de un grupo de
volúmenes como un disco abstracto, un volúmen lógico se compara a una
partición. El LV será un dispositivo de bloque que tendrá un elemento en
/dev y puede ser utilizado como cualquier partición física (usualmente,
almacenar un sistema de archivos o espacio de intercambio). Lo
importante es que la división de un VG en varios LVs es completamente
independiente de sus componentes físicos (los PVs). Se puede dividir un
VG con un solo componente físico (un disco por ejemplo) en una docena de
volúmenes lógicos; del mismo modo, un VG puede utilizar varios discos
físicos y aparecer como sólo un volúmen lógico grande. La única
limitación es que, obviamente, el tamaño total asignado a un LV no puede
ser mayor que la capacidad total de los PVs en el grupo de volúmenes.
Generalmente tiene sentido, sin embargo, mantener el mismo tipo de
homogeneidad entre los componentes físicos de un VG y dividir el VG en
volúmenes lógicos que tendrán patrones de uso similares. Por ejemplo, si
el hardware disponible incluye discos rápidos y discos lentos, podría
agrupar los discos rápidos en un VG y los lentos en otro; puede asignar
pedazos del primero a aplicaciones que necesiten acceso rápido a los
datos y mantener el segundo para tareas menos exigentes.

Ejemplo práctico: Uso de LVM en servidor de archivos y DB
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para entender un poco más estos conceptos, veremos un ejemplo sencillo.
Partiremos de un equipo con 2 discos SAS de 500Gbytes y 10k RPM, un
disco SAS de 300Gbytes y 15k RPM y un disco Sata de 7.2k RPM. Supongamos que
este equipo se utilizará como servidor de bases de datos, y a la vez como
servidor de archivos.

El equipo entonces cuenta con 5 discos de tecnologías diferentes, y rendimientos
muy distintos. Los discos son de tecnologías Sata (Serial Ata), SAS
(Serial Attached SCSI) y SCSI. La tecnología Sata es la menos performante, y
la SAS la más performante de ellas. Las capacidades son las siguientes:

-  /dev/sda: Disco SAS de 500 Gbytes y 10k RPM

-  /dev/sdb: Disco SAS de 500 Gbytes y 10k RPM

-  /dev/sdc: Disco SAS de 300 Gbytes y 15k RPM

-  /dev/sdd: Disco SCSI de 300 Gbytes y 15k RPM

-  /dev/sde: Disco Sata de 10 Gbytes y 7,2k RPM

Para simplificar el ejemplo, dejaremos de lado la redundancia y solo nos
centraremos en la flexibilidad y la escalabilidad, este último un factor
más que importante, sobre todo a la hora de dimensionar el crecimiento
que puede tener el servidor de archivos.

La idea es contar con 2 grupos de volúmenes (VGs), uno compuesto por los
discos /dev/sda y /dev/sdb, que será utilizado para alojar el servidor
de archivos. El otro VG estará conformado por el disco /dev/sdc
únicamente, y será utilizado para alojar allí las bases de datos, dado
que es un dispositivo muy performante. El disco /dev/sde será utilizado para
montar el sistema raíz. Y por último, el disco /dev/sdd lo utilizaremos más
tarde.

Luego crearemos 3 volúmenes lógicos, uno que utilizaremos para los homes
de los usuarios( /home ), uno para montar el servidor de archivos
(/srv/fileserver ) y el restante para montar el directorio que contendrá
las bases de datos.

La configuración se resumen en el siguiente diagrama

.. figure:: ../imagenes/unidad01/image_5.png
   :alt: Configuración de ejemplo
   :align: center
   :scale: 70%

   Fig.5 - Configuración de ejemplo


Empezamos por descargar la definición de vagrant para la maquina virtual que
utilizaremos

.. code:: bash

	mboscovich@tesla:~$ mkdir maquina-virtual-02
	mboscovich@tesla:~$ cd maquina-virtual-02
	mboscovich@tesla:~/maquina-virtual-02$ wget https://gitlab.com/tusl/adm-gnu-linux-3/raw/master\
	/maquinas-virtuales/unidad01/ejemplopracticoLVM/Vagrantfile
	mboscovich@tesla:~/maquina-virtual-02$ vagrant up
	Bringing machine 'default' up with 'virtualbox' provider...
	==> default: Box 'mboscovich/AdmGNULinux3-TUSL-MV2' could not be found. Attempting to find and install...
	    default: Box Provider: virtualbox
	    default: Box Version: >= 0
	==> default: Loading metadata for box 'mboscovich/AdmGNULinux3-TUSL-MV2'
	    default: URL: https://vagrantcloud.com/mboscovich/AdmGNULinux3-TUSL-MV2
	==> default: Adding box 'mboscovich/AdmGNULinux3-TUSL-MV2' (v1.0) for provider: virtualbox
	    default: Downloading: https://vagrantcloud.com/mboscovich/boxes/AdmGNULinux3-TUSL-MV2/versions/1.0/providers/virtualbox.box
	==> default: Successfully added box 'mboscovich/AdmGNULinux3-TUSL-MV2' (v1.0) for 'virtualbox'!
	==> default: Importing base box 'mboscovich/AdmGNULinux3-TUSL-MV2'...
	==> default: Matching MAC address for NAT networking...
	==> default: Checking if box 'mboscovich/AdmGNULinux3-TUSL-MV2' is up to date...
	==> default: Setting the name of the VM: AdmGNULinux3-TUSL-MV2
	==> default: Clearing any previously set network interfaces...
	==> default: Preparing network interfaces based on configuration...
	    default: Adapter 1: nat
	    default: Adapter 2: hostonly
	==> default: Forwarding ports...
	    default: 22 (guest) => 2222 (host) (adapter 1)
	==> default: Running 'pre-boot' VM customizations...
	==> default: Booting VM...
	==> default: Waiting for machine to boot. This may take a few minutes...
	    default: SSH address: 127.0.0.1:2222
	    default: SSH username: vagrant
	    default: SSH auth method: private key
	    default:
	    default: Vagrant insecure key detected. Vagrant will automatically replace
	    default: this with a newly generated keypair for better security.
	    default:
	    default: Inserting generated public key within guest...
	    default: Removing insecure key from the guest if it's present...
	    default: Key inserted! Disconnecting and reconnecting using new SSH key...
	==> default: Machine booted and ready!
	==> default: Checking for guest additions in VM...
	    default: The guest additions on this VM do not match the installed version of
	    default: VirtualBox! In most cases this is fine, but in rare cases it can
	    default: prevent things such as shared folders from working properly. If you see
	    default: shared folder errors, please make sure the guest additions within the
	    default: virtual machine match the version of VirtualBox you have installed on
	    default: your host and reload your VM.
	    default:
	    default: Guest Additions Version: 5.2.6
	    default: VirtualBox Version: 5.1
	==> default: Configuring and enabling network interfaces...
	==> default: Mounting shared folders...
	    default: /vagrant => /home/mboscovich/maquina-virtual-02

Para conectarnos a la maquina virtual, debemos utilizar en este caso la IP
192.168.33.11 y el usuario vagrant, tal y como lo hicimos en el ejemplo anterior

.. code:: bash

	mboscovich@tesla:~/maquina-virtual-01$ ssh vagrant@192.168.33.11
	vagrant@192.168.33.11's password:
	Linux stretch 4.9.0-5-amd64 #1 SMP Debian 4.9.65-3+deb9u2 (2018-01-04) x86_64

	The programs included with the Debian GNU/Linux system are free software;
	the exact distribution terms for each program are described in the
	individual files in /usr/share/doc/*/copyright.

	Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
	permitted by applicable law.
	Last login: Tue Feb 13 14:26:27 2018 from 10.0.2.2
	vagrant@stretch:~$

Bien, ahora que estamos en la maquina virtual, pongamos manos a la obra y
empecemos por instalar LVM

.. code:: bash

		vagrant@stretch:~$ sudo apt-get install lvm2

Luego debemos definir los volúmenes físicos, que como habíamos dicho serán
sda, sdb y sdc.

.. code:: bash

	vagrant@stretch:~$ sudo pvcreate /dev/sda /dev/sdb /dev/sdc
	  Physical volume "/dev/sda" successfully created.
	  Physical volume "/dev/sdb" successfully created.
	  Physical volume "/dev/sdc" successfully created.

En este caso hemos definido los volúmenes utilizando los discos
completos, pero bien podríamos haber utilizado particiones.

Podemos ver los volúmenes físicos que hemos creado, utilizando el
comando pvdisplay

.. code:: bash

	vagrant@stretch:~$ sudo pvdisplay -C
	  PV         VG Fmt  Attr PSize   PFree
	  /dev/sda      lvm2 ---  500.00g 500.00g
	  /dev/sdb      lvm2 ---  500.00g 500.00g
	  /dev/sdc      lvm2 ---  300.00g 300.00g

Ahora es momento de agrupar estos volúmenes físicos en grupos de
volúmenes, que como hemos mencionado serán denominados VG_Default y
VG Performante. Para esto debemos utilizar el comando vgcreate, especificando el
nombre que le queremos dar al grupo y los PV que lo conformarán

.. code:: bash

	vagrant@stretch:~$ sudo vgcreate VG_Default /dev/sda /dev/sdb
	  Volume group "VG_Default" successfully created
	vagrant@stretch:~$ sudo vgcreate VG_Performante /dev/sdc
	  Volume group "VG_Performante" successfully created

Con el comando vgdisplay podemos ver como han sido definidos y que
grupos de volúmenes existen hasta ahora

.. code:: bash

	vagrant@stretch:~$ sudo vgdisplay -C
	  VG             #PV #LV #SN Attr   VSize   VFree
	  VG_Default       2   0   0 wz--n- 799.99g 799.99g
	  VG_Performante   1   0   0 wz--n- 500.00g 500.00g

Como datos relevantes, vemos la cantidad de PVs asignados al VG, y cual
es el tamaño máximo del mismo.

Incluso si hacemos nuevamente un pvdisplay, veremos información relevante

.. code:: bash

	vagrant@stretch:~$ sudo pvdisplay -C
	  PV         VG             Fmt  Attr PSize   PFree
	  /dev/sda   VG_Default     lvm2 a--  500.00g 500.00g
	  /dev/sdb   VG_Default     lvm2 a--  500.00g 500.00g
	  /dev/sdc   VG_Performante lvm2 a--  300.00g 300.00g

En particular destacar que ahora vemos a que "Grupo de Volúmenes" (VG) esta
asociado cada "Volúmen físico" (PV).

Solo resta definir los volúmenes lógicos (LVs), y como es de esperar, hay un
comando lvcreate.

Creemos el LV que se utilizará para los homes, le daremos inicialmente
100G

.. code:: bash

	vagrant@stretch:~$ sudo lvcreate -n lv_home --size 100G VG_Default
	  Logical volume "lv_home" created.

Si ahora hacemos un lvdisplay, veremos detalles sobre el volumen creado

.. code:: bash

	vagrant@stretch:~$ sudo lvdisplay
	--- Logical volume ---
	LV Path                /dev/VG_Default/lv_home
	LV Name                lv_home
	VG Name                VG_Default
	LV UUID                Ct1I4e-9qDL-f2hf-x9fZ-XLdA-0Rfs-PeMXtT
	LV Write Access        read/write
	LV Creation host, time stretch, 2018-02-24 13:45:21 +0000
	LV Status              available
	# open                 0
	LV Size                100.00 GiB
	Current LE             25600
	Segments               1
	Allocation             inherit
	Read ahead sectors     auto
	- currently set to     256
	Block device           254:0

Lo más relevante son el "LV Path", que básicamente nos dice como se
llama el dispositivo de bloques que representa el LV, y que utilizaremos
para montar un filesystem o un área de intercambio. También nos informa a que
VG pertenece (VG Name), y el tamaño del mismo (LV Size).
Solo hemos definido un LV, si miramos nuevamente el estado del VG
veremos que ya no están disponibles 100G.

.. code:: bash

	vagrant@stretch:~$ sudo vgdisplay -C VG_Default
	  VG         #PV #LV #SN Attr   VSize   VFree
	  VG_Default   2   1   0 wz--n- 999.99g 899.99g

Por lo tanto si queremos crear el siguiente LV, el que utilizaremos para
el servidor de archivos, debemos asignarle como tamaño máximo 899,99G

.. code:: bash

	vagrant@stretch:~$ sudo lvcreate -n lv_fileserver --size 899.99g VG_Default
	  Rounding up size to full physical extent 899.99 GiB
	  Logical volume "lv_fileserver" created.

Si nuevamente realizamos un vgdisplay veremos que ya no cuenta con
espacio

.. code:: bash

		vagrant@stretch:~$ sudo vgdisplay -C
	  VG             #PV #LV #SN Attr   VSize   VFree
	  VG_Default       2   2   0 wz--n- 999.99g      0
	  VG_Performante   1   0   0 wz--n- 300.00g 300.00g

A continuación solo resta crear el último LV que nos hemos planteado, el que
se utilizara para las bases de datos, y que pertenecera al "Grupo de volúmenes"
VG_Performante. Utilizaremos todo el espacio disponible para el mismo

.. code:: bash

	vagrant@stretch:~$ sudo lvcreate -n lv_bases --size 299.99g VG_Performante
	  Rounding up size to full physical extent 299.99 GiB
	  Logical volume "lv_bases" created.

Si estuvo atento, habrá observado que como tamaño no le hemos asignado
300GB, sino que en su lugar es de 299.99GB (lo mismo tuvimos que hacer
con lv_fileserver). Esto se debe a que no tenemos disponible los 300GB,
sino que una parte (ínfima) se utiliza para guardar la información de
LVM. Si hubiéramos tratado de crear el LV con 300G nos hubiera dado un
error, informando que el VG no tiene espacio suficiente.

Continuemos. Ahora los diferentes dispositivos de bloques son :

-  /dev/VG_Default/lv_home

-  /dev/VG_Default/lv_fileserver

-  /dev/VG_Performante/lv_bases

Como mencionamos anteriormente, sobre los mismos se pueden crear
filesystems, o utilizarlos como área de intercambio.

Creemos ahora por ejemplo un filesystem sobre lv_home

.. code:: bash

	vagrant@stretch:~$ sudo mkfs.ext4 /dev/VG_Default/lv_home
	mke2fs 1.43.4 (31-Jan-2017)
	Creating filesystem with 26214400 4k blocks and 6553600 inodes
	Filesystem UUID: 5a1c0aa8-cf67-4110-82a8-c4e4defdfdff
	Superblock backups stored on blocks:
		32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208,
		4096000, 7962624, 11239424, 20480000, 23887872

	Allocating group tables: done
	Writing inode tables: done
	Creating journal (131072 blocks): done
	Writing superblocks and filesystem accounting information: done

y montémoslo como tal.

*NOTA: Por cuestiones de simplicidad lo montamos temporalmente, lo recomendable
es definirlo en el archivo fstab para que se automonte en cada reinicio.*

.. code:: bash

	vagrant@stretch:~$ sudo mount /dev/VG_Default/lv_home /home/

Si vemos el tamaño asignado

.. code:: bash

	vagrant@stretch:~$ df -h /home/
	Filesystem                      Size  Used Avail Use% Mounted on
	/dev/mapper/VG_Default-lv_home   98G   61M   93G   1% /home

Ahora supongamos que ha pasado el tiempo y el sistema empezó a escalar en
la cantidad de usuarios y 100G ya no son suficientes. Es en estos casos
donde LVM toma relevancia, porque simplemente podemos comprar un nuevo
disco, anexarlo al VG_Default, y luego redimensionar el LV con los
homes. Incluso no es necesario que la tecnología del disco sea similar a
las utilizadas en el VG.

Siguiendo con nuestro ejemplo, supongamos que compramos un disco SCSI de
300GB, el que anexaremos al VG_Default para redimensionar el LV. Para simular
este proceso, utilizaremos el disco /dev/sdd con el que la maquina virtual ya
cuenta.

Primero lo definimos como PV

.. code:: bash

	vagrant@stretch:~$ sudo pvcreate /dev/sdd
	  Physical volume "/dev/sdd" successfully created.

Lo asignamos al VG_Default, para esto debemos utilizar el comando
vgextend, porque precisamente estamos extendiendo las capacidades del
mismo

.. code:: bash

	vagrant@stretch:~$ sudo vgextend VG_Default /dev/sdd
	  Volume group "VG_Default" successfully extended

Si hacemos un vgdisplay veremos que ahora contamos con un VG de 1,3TB, y
300GB libres

.. code:: bash

	vagrant@stretch:~$ sudo vgdisplay -C
	  VG             #PV #LV #SN Attr   VSize   VFree
	  VG_Default       3   2   0 wz--n-   1.27t 200.00g
	  VG_Performante   1   1   0 wz--n- 300.00g   4.00m

del mismo modo extendemos el LV, con el comando lvextend

.. code:: bash

	vagrant@stretch:~$ sudo lvextend --size +100G /dev/VG_Default/lv_home
	  Size of logical volume VG_Default/lv_home changed from 100.00 GiB (25600 extents) to 200.00 GiB (51200 extents).
	  Logical volume VG_Default/lv_home successfully resized.

En este caso le decimos con le sume +100GB al LV, el que es especificado
mediante su path.

Si ahora hacemos un lvdisplay veremos que el LV es de 200G

.. code:: bash

	vagrant@stretch:~$ sudo lvdisplay -C
	  LV            VG             Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
	  lv_fileserver VG_Default     -wi-a----- 899.99g
	  lv_home       VG_Default     -wi-a----- 200.00g
	  lv_bases      VG_Performante -wi-a----- 299.99g

sin embargo si hacemos un df -h veremos que el filesystem sigue siendo
de 100GB. Esto es debido a que nos falta redimensionar el filesystem
para que tome el nuevo tamaño disponible

.. code:: bash

	vagrant@stretch:~$ sudo resize2fs /dev/VG_Default/lv_home
	resize2fs 1.43.4 (31-Jan-2017)
	Resizing the filesystem on /dev/VG_Default/lv_home to 52428800 (4k) blocks.
	The filesystem on /dev/VG_Default/lv_home is now 52428800 (4k) blocks long.

Ahora si, el filesystem cuenta con los 200GB.

.. code:: bash

	vagrant@stretch:~$ df -h /home
	Filesystem                      Size  Used Avail Use% Mounted on
	/dev/mapper/VG_Default-lv_home  197G   60M  187G   1% /home

Con esto concluimos la Unidad 1. Las 2 tecnologías que hemos visto son
muy importantes, ya que nos permiten afrontar problemas de escalabilidad
e incluso contingencias por fallas en el hardware de los dispositivos de
almacenamiento, por lo que es indispensable su uso en sistemas en
entornos productivos
