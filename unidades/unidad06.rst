.. image:: ../caratulas/unidad06.pdf
	:scale: 86%

Unidad 6: Administración Centralizada
=====================================

Introducción
------------
Un administrador de sistemas se encuentra por lo general con
decenas, cientos y hasta incluso miles de hosts a administrar. Llevar adelante
esta tarea de forma manual es práctimanete imposible.

Pensemos por un momento lo que implicaría agregar en una infraestructura de
100 ó más instancias virtuales, a un nuevo administrador de sistemas que se
a sumado al equipo de trabajo. Tendríamos que ingresar uno por uno a todos los
hosts y agregar el usuario, permitirle el acceso ssh, permitir que ejecute
comandos con sudo, etc; ó imaginemos que tenemos que realizar el deploy de una
nueva aplicación que involucra cambios en varios servidores web, en
redirectores HTTP y varios motores de bases de datos: tendríamos que ir uno por
uno realizando las modificaciones necesarias en estos equipos, con el riesgo de
cometer errores y por sobre todo con un gran tiempo offline del servicio. Si a
esto le sumamos que todas estas tareas a su vez previamente deben ser testeadas
en un entorno de testing ó stagging, o a veces incluso ambos; prácticamente
estaríamos perdiendo todo el pelo en un solo cambio.

Infraestructuras como la mencionada anteriormente son muy comunes, convirtiendo
a la automatización en algo totalmente indispensable.

Administración centralizada
---------------------------
La Administración Centralizada, o también conocida como administración de
configuración (Configuration management) consiste en administrar los cambios en
la configuración de un sistema utilizando un método definido, manteniendo así
el sistema y su integridad a lo largo del tiempo. Inicialmente fue concebida para
documentar las configuraciones en los sistemas, es decir, saber como estaba
cada sistema configurado. Con el paso del tiempo y la evolución de diferentes
herramientas, se convirtió en un modo de definir el estado deseado de un sistema.
Podemos definir "como" debe estar configurado un sistema y luego son estas
herramientas las que se encargan automáticamente de garantizar que dicho sistema
se mantengan de ese modo, aplicando los cambios necesarios en su configuración.
Por ejemplo, podemos definir que tal directorio debe tener tales permisos, o que
debe tener instalado tales paquetes de software y otros no pueden estar
instalados, ó que el sistema debe tener los usuarios administradores definidos, etc.

Infraestructura como código
---------------------------
Si definimos el conjunto de las configuraciones desde un equipo central, definiendo
recetas que especifican como deben estar configurados los diferentes nodos de
nuestra infraestructura, en lugar de ingresar a los nodos y configurarlos,
estamos en presencia de lo que se conoce como "Infraestructura como código"
(Infraestructure as code). Al tratarse de código (recetas), podemos tener la
definición de toda una infraestructura en un repositorio bajo control de versión
(svn, git, bazzar, etc), lo que ofrece una potencia muy grande, y una versatilidad
mayor aún, agilizando enormemente las tareas diarias de administración. En este
contexto, solo tenemos que limitarnos a aplicar cambios en las recetas, y
dejar que luego una herramienta se encargue de la configuración de los nodos,
algo muy conveniente cuando el número de hosts a administrar es muy grande.

Si bien existen un montón de herramientas que pueden ayudarnos a llevar adelante
esta tarea (por ejemplo Puppet, CFEngine, Chef entre otros), en esta unidad nos
centraremos en  Ansible, una herramienta muy completa que se ha vuelto muy popular
recientemente y que ofrece y promete mucho.

Ansible
-------
Ansible nació en el año 2012 como un proyecto menor en github, y rápidamente fue
captando adeptos y colaboradores, que lo convirtieron en una de las herramientas
de automatización y administración mas potentes. La misma combina instalación
multi-nodo, ejecuciones de tareas ad hoc y administración de configuraciones.
Maneja nodos a través de SSH y no requiere ningún software remoto adicional, es
decir es agentless, dado que no requiere la instalación de ningún agente en los
nodos a administrar, una de las principales ventajas respecto de otras
herramientas similares. Dispone de módulos que trabajan sobre JSON y la salida
estándar puede ser escrita en cualquier lenguaje. Utiliza YAML para describir
las recetas de configuraciones reusables. La plataforma fue creada por Michael
DeHaan, también autor de la aplicación de aprovisionamiento Cobbler y co-autor
del framework para administración remota Func.

Conceptos
~~~~~~~~~

Playbooks
~~~~~~~~~
Las recentas en Ansible son escritas en lo que se conoce como playbooks(ó libros
de jugadas). Estos son archivos YAML de lectura sencilla tanto para humanos como
para máquinas. En los playbooks se utiliza una sintaxis muy sencilla para
describir que es lo que Ansible debe realizar con tal o cual nodo o conjunto de
nodos.

Empecemos con un ejemplo. En primer lugar le diremos a Ansible donde se debe
aplicar la receta (el playbook). Esto se especifica con la sentencia hosts:

.. code:: json

	---
	- hosts: all

Esto quiere decir, en todos los host definidos en el inventario. El inventario
en Ansible es un archivo en texto plano que define sobre que nodos
se trabajará. Este archivo entre otras cosas permite definir grupos
de host, variables, rangos de ips, etc. La ubicación por defecto de
este archivo es /etc/ansible/hosts, pero podemos definir un archivo cualquiera y
luego pasarle por parámetros el mismo en la ejecución del playbook.
Un ejemplo de inventario sería el siguiente

.. code:: json

	nodo1.intranet

	[servidoresWeb]
	nodo2.intranet

	[servidoresDB]
	nodo3.intranet
	nodo2.intranet

Este archivo cuenta con 3 hosts, y dos grupos. Como se puede observar, un host
puede pertenecer a mas de un grupo, y no todos los host deben pertenecer a uno.
La sentencia anterior definida en el playbook (host: all) aplica sobre todos los
nodos definidos en el inventario, es decir sobre nodo1, nodo2 y nodo3 en este
caso. Es muy importante aclarar que Ansible trabaja utilizando ssh, motivo por el
cual es necesario previamente realizar un intercambio de claves, de modo de evitar
tener que ingresar la password cada vez. Por otro lado, la conexión hacia el nodo
se realizará utilizando el usuario que ejecuta el comando  Ansible, como veremos
más adelante, y utilizará los privilegios del mismo para realizar las tareas en
el nodo, por lo que es recomendable que dicho usuario tenga permisos de sudo en el
host.
Para realizar el intercambio de claves, solo debemos ejecutar el comando
ssh-copy-id de la siguiente manera

.. code:: bash

	usuario@mipc:~$ ssh-copy-id nodo1.intranet
	/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
	/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
	usuario@nodo1.intranet's password:

	Number of key(s) added: 1

	Now try logging into the machine, with:   "ssh 'nodo1.intranet'"
	and check to make sure that only the key(s) you wanted were added.


Ahora que Ansible sabe donde debe ejecutar la receta, y que hemos realizado
el intercambio de claves para conectarnos a los nodos, debemos decirle que es lo
que tiene que hacer, o garantizar. Esto se realiza mediante la sentencia task
(tareas), donde definimos de manera secuencial que es lo que debe garantizarse. Y
decimos garantizarse en vez de ejecutarse, porque Ansible se asegurara de que lo
definido se cumpla, y luego decidirá si debe ejecutar una acción o no. Para esto
Ansible se basa en los denominados "Módulos", que sirven para
diferentes tareas como testear conexiones, instalar software, setear permisos,
configuar servicios, etc, etc, etc. Hablaremos de ellos un poco más adelante.

Sigamos con nuestro playbook, y definamos como primer tarea que se debe garantizar
conectividad a los nodos, es decir, Ansible debe ser capaz de conectarse a los
mismo (de momento solo nodo1.intranet). Para ello utilizaremos el módulo "ping"
de la siguiente manera:

.. code:: json

	---
	- hosts: all
	  tasks:
	  - name: Garantizar conectividad al nodo
	    ping:

La ejecución de este playbook se realiza por medio del comando ansible-playbook,
especificando el playbook en cuestión y el inventario

.. code:: bash

	usuario@mipc:~$ ansible-playbook -i inventario playbook.yml

El comando anterior debería mostrar una salida como la siguiente

.. code:: bash

	usuario@mipc:~$ ansible-playbook -i inventario playbook.yml

	PLAY ***************************************************************************

	TASK [setup] *******************************************************************
	ok: [nodo1.intranet]

	TASK [Garantizar conectividad a los nodos] *************************************
	ok: [nodo1.intranet]

	PLAY RECAP *********************************************************************
	nodo1.intranet             : ok=2    changed=0    unreachable=0    failed=0

En este caso vemos que la tarea "Garantizar conectividad a los nodos" se ejecuto
correctamente. Incluso al final, en la sección PLAY RECAP (resumen de la jugada),
se muestra un resumen de las tareas que se ejecutaron, sobre que nodos y el
resultado obtenido.

Habrán notado que hemos definido 1 sola tarea, pero se han ejecutado
2. Esto es debido a que Ansible siempre ejecuta de manera inicial la tarea "setup",
que como veremos más adelante, se encarga de relevar información del nodo y definir
ciertas variables propias del mismo, las que luego pueden utilizarse en el playbook...
pero no nos adelantemos, que aún falta.

De momento no tiene demasiado valor agregado saber que nos podemos conectar, pero
es un primer paso. Hagamos algo un poco mas interesante, instalemos mediante ansible
el Servidor Web Apache. Para esto modifiquemos el playbook de la siguiente manera,
y agreguemos tareas que se encarguen de garantizar que Apache este instalado y
corriendo. Utilizaremos 2 módulos nuevos, apt y service. El primero se encarga
de instalar paquetes y el segundo se encarga de garantizar el estado de un servicio

.. code:: bash

	 ---
	 - hosts: all
	   tasks:
	   - name: Garantizar conectividad a los nodos
	     ping:

	   - name: Instalar el servidor Web Apache
	     become: true
	     apt:
	       name: apache2
	       state: present

	   - name: Garantizar que el servidor Web Apache este corriendo
	     become: true
	     service:
	       name: apache2
	       state: started

Come verán, los modulos apt y services, a diferencia del móduloping, aceptan
parámetros, los cuales nos permiten definir, para el caso de apt, que paquete y
que estado debe tener en el nodo (present, latest, absent), o que servicio debe estar
en que estado (started, stopped), para el caso del móduloservice. Cada módulo
tiene sus propios parámetros y se cuenta con una documentación muy amplia en
http://docs.ansible.com/ .

Volviendo al playbook, notarán que hemos agregado la opción become a las
tareas "Instalar el servidor Web Apache" y "Garantizar que el servidor Web Apache
este corriendo", esto se debe a que dichas tareas necesitan permisos de root para
su ejecución. En este caso, la opción become convierte al usuario en super-usuario
utilizando sudo.
Para ejecutar el playbook debemos tener en cuenta que debemos ingresar la
contraseña del usaurio para que pueda hacer sudo(recordemos que Ansible se
conecta al nodo con el usuario que esta corriendo el comando ansible-playbook),
para ello agregamos el parámetro "--ask-sudo-pass" al comando ansible-playbook

.. code:: bash

	usuario@mipc:~$ ansible-playbook --ask-sudo-pass -i inventario playbook.yml
	SUDO password:

	PLAY ***************************************************************************

	TASK [setup] *******************************************************************
	ok: [nodo1.intranet]

	TASK [Garantizar conectividad a los nodos] *************************************
	ok: [nodo1.intranet]

	TASK [Instalar el servidor Web Apache] *****************************************
	changed: [nodo1.intranet]

	TASK [Garantizar que el servidor Web Apache este corriendo] ********************
	ok: [nodo1.intranet]

	PLAY RECAP *********************************************************************
	nodo1.intranet             : ok=4    changed=1    unreachable=0    failed=0

Perfecto, ya tenemos Apache instalado y corriendo en el nodo. Pero que pasa si
ejecutamos nuevamente el playbook?, veamos...

.. code:: bash

	usuario@mipc:~$ ansible-playbook --ask-sudo-pass -i inventario playbook.yml
	SUDO password:

	PLAY ***************************************************************************

	TASK [setup] *******************************************************************
	ok: [nodo1.intranet]

	TASK [Garantizar conectividad a los nodos] *************************************
	ok: [nodo1.intranet]

	TASK [Instalar el servidor Web Apache] *****************************************
	ok: [nodo1.intranet]

	TASK [Garantizar que el servidor Web Apache este corriendo] ********************
	ok: [nodo1.intranet]

	PLAY RECAP *********************************************************************
	nodo1.intranet             : ok=4    changed=0    unreachable=0    failed=0

Todas las tareas devuelven "ok", y ninguna esta marcada como "changed". Es decir
Ansible no tuvo que hacer nada porque todas las condiciones que definimos en el
playbook se cumplen, solo verificó que se cumpliera lo definido. Esto se denomina
**Idempotencia**, y es una propiedad que tienen los módulos en  Ansible, solo aplican
lo necesario para que se cumpla lo definido en la tarea. Esta es la principal
diferencia con un script de Bash, Phython ó Perl que podamos utilizar para
automatizar tareas. Por ejemplo, no se instalará un paquete que ya esta instalado,
o no se realizará un "start" de un servicio que ya se encuentra corriendo. Podemos
ejecutar N veces el playbook y siempre arrojara el mismo resultado, la condición
que hemos planteado en la receta.

Juguemos un poco más con el módulo apt, y ahora modifiquemos la receta para que
desinstale Apache, instale Nginx y MySQL

.. code:: bash

	---
	- hosts: all
	  tasks:
	  - name: Garantizar conectividad a los nodos
	    ping:
	  - name: Desinstalar el servidor web Apache
	    become: true
	    apt:
	      name: apache2
	      state: absent
	  - name: Instalar el servidor web Nginx
	    become: true
	    apt:
	      name: nginx
	      state: present
	      update-cache: yes
	  - name: Instalar el servidor de bases de datos MySQL
	    become: true
	    apt:
	      name: mysql-server-5.6
	      state: present
	      update-cache: yes

	  - name: Garantizar que el servidor web Nginx este corriendo
	    become: true
	    service:
	      name: nginx
	      state: started
	  - name:  Garantizar que el servidor de Bases de Datos MySQL esten corriendo
	    become: true
	    service:
	      name: mysql
	      state: started

Si ejecutamos el playbook vemos como se desinstala Apache e instalan los paquetes
seleccionados

.. code:: bash

	usuario@mipc:~$ ansible-playbook --ask-sudo-pass -i inventario playbook.yml
	SUDO password:

	PLAY ***************************************************************************

	TASK [setup] *******************************************************************
	ok: [nodo1.intranet]

	TASK [Garantizar conectividad a los nodos] *************************************
	ok: [nodo1.intranet]

	TASK [Desinstalar el servidor Web Apache] **************************************
	changed: [nodo1.intranet]

	TASK [Instalar el servidor web nginx] ******************************************
	changed: [nodo1.intranet]

	TASK [Instalar el servidor de Bases de Datos MySQL] ****************************
	changed: [nodo1.intranet]

	TASK [Garantizar que el servidor Web Nginx esten corriendo] ********************
	ok: [nodo1.intranet]

	TASK [Garantizar que el servidor de Bases de Datos MySQL esten corriendo] ******
	ok: [nodo1.intranet]

	PLAY RECAP *********************************************************************
	nodo1.intranet             : ok=7    changed=3    unreachable=0    failed=0

Para emprolijar un poco mas nuestra receta, podemos agrupar todas las tareas
comunes, mediante el uso de *loops*, y sacar el uso del móduloping, dado que con
solo ejecutar Ansible sabremos si hay conectividad o no. El playbook quedaría
simplificado de la siguiente manera:

.. code:: bash

	---
	- hosts: all
	  tasks:
	  - name: Desinstalar el servidor Web Apache
	    become: true
	    apt:
	      name: apache2
	      state: absent

	  - name: Instalar Nginx y MySQL
	    become: true
	    apt:
	      name: "{{ item }}"
	      state: present
	      update-cache: yes
	      with_items:
	        - nginx
	        - mysql-server-5.6

	  - name: Garantizar que Nginx y MySQL esten corriendo
	    become: true
	    service:
	      name: "{{ item }}"
	      state: started
	      with_items:
	        - nginx
	        - mysql

Este playbook es el mismo que el anterior, pero simplificado.
Si ha prestado atención, notará que hemos agregado también el parámetro
*update-cache: yes* en el módulo apt. Investigue sobre el mismo en
http://docs.ansible.com/ansible/apt_module.html

Inventario
^^^^^^^^^^
Hemos hablado un poco ya del inventario, pero es importante entender sus
características y como organizarlo.
El inventario es una parte importante de toda herramienta de administración
centralizada, dado que nos permite definir en donde se deben ejecutar las tareas.
Vimos que Ansible por defecto lee el inventario de /etc/ansible/hosts, pero también
podemos optar por definirlo en cualquier archivo e invocarlo mediante la opción
-i del comando ansible-playbook.

El inventario esta escrito en formato INI, pero también puede ser escrito en
formato JSON, utilizandose este último cuando el mismo se genera de forma
dinámica. Nosotros utilizaremos el formato INI.

Podemos listar a los nodos por su nombre de DNS ó su IP, e incluso podemos
utilizar rangos tanto de nombres como de IPs. Por ejemplo

.. code:: bash

	nodo1.intranet
	nodo2.intranet
	nodo[3:5].intranet
	192.168.1.201
	192.168.1.[202:2:210]
	192.168.1.100:2222

Los primeros 3 son definiciones utilizando el nombre de DNS de los hosts, o lo
definido en el archivo /etc/hosts del equipo donde se ejecuta ansible. El tercero
utiliza rangos de Python, y es equivalente a

.. code:: bash

	nodo3.intranet
	nodo4.intranet
	nodo5.intranet

La 4ta opción es por medio de una dirección IP, y la 5ta es utilizando rangos con
un paso de 2, es decir, la 5ta opción equivale a

.. code:: bash

	192.168.1.202
	192.168.1.204
	192.168.1.206
	192.168.1.208
	192.168.1.210

Por útlimo, la 6ta opción es especificando la dirección IP y el puerto donde escucha
SSH (recordemos que Ansible se comunica con los diferentes hosts por este medio)

Estas son las formas mas comunes de representar a los hosts.
Otra característica importante que vimos es que se pueden utilizar grupos:

.. code:: bash

	[web_servers]
	nodo1.intranet

	[database_servers]
	nodo1.intranet
	nodo2.intranet

	[loadbalancers]
	nodo[4:5].intranet

	[servidores_internos:children]
	web_servers
	database_servers

La última opción, que hace uso de :childrens, nos permite especificar grupos de
grupos. Es decir tanto el grupo web_servers como el grupo database_servers
pertenecen al grupo servidores_internos.
Estos grupos son útiles para luego especificar donde debe correr nuestro playbook,
tal como veremos en breve.

Si nos detenemos a pensar un poco en el archivo de inventario, veremos que no solo
es útil para  Ansible, sino que también nos permite tener documentada nuestra
infraestructura, sabiendo de primera mano que host hace que cosa.

Ejemplo práctico
================

Tomemos el toro por las hasta y probemos hacer una receta con ejemplos de la vida
diaria de un sysadmin, y de este modo aprendamos haciendo.
En este ejemplo encaremos la instalación y configuración de un Wordpress
con su entorno. Para esto nos basaremos en los pasos recomendados por Debian
(https://wiki.debian.org/WordPress), pero convertiremos los mismos en una receta
de ansible.

Wordpress requiere que tengamos instalados PHP, ciertos paquetes extras de php,
un servidor de bases de datos y un servidor web, optaremos por Apache2 y MySQL.
Nuestro playbook empezaría así

.. code:: bash

	- hosts: all
	  tasks:
	  - name: Instalar Worpress y sus dependencias
	    become: true
	    apt:
	      name: "{{ item }}"
	      state: present
	      update-cache: yes
	    with_items:
	      - wordpress
	      - curl
	      - apache2
	      - mysql-server

Siempre es recomendable ir ejecutando Ansible a medida que vamos escribiendo la
receta. Por lo que ejecutemos esta misma receta sobre el nodo1.intranet que
veniamos utilizando, aunque podríamos aplicarlo sobre cualquier equipo, incluso
uno recién instalado, una maquina virtual.

.. code:: bash

	usuario@mipc:~$ ansible-playbook --ask-sudo-pass -i inventario playbook.yml
	SUDO password:

	PLAY ***************************************************************************

	TASK [setup] *******************************************************************
	ok: [nodo1.intranet]

	TASK [Instalar Worpress y sus dependencias] ************************************
	changed: [nodo1.intranet] => (item=[u'wordpress', u'curl', u'apache2', u'mysql-server'])

	TASK [Agregar usuarios] ********************************************************
	ok: [nodo1.intranet]

	PLAY RECAP *********************************************************************
	nodo1.intranet             : ok=3    changed=1    unreachable=0    failed=0

Ahora según la documentación de Debian, debemos crear un archivo
/etc/apache2/sites-available/wp.conf y poner allí lo siguiente

.. code:: bash

	Alias /wp/wp-content /var/lib/wordpress/wp-content
	Alias /wp /usr/share/wordpress
	<Directory /usr/share/wordpress>
	    Options FollowSymLinks
	    AllowOverride Limit Options FileInfo
	    DirectoryIndex index.php
	    Require all granted
	</Directory>
	<Directory /var/lib/wordpress/wp-content>
	    Options FollowSymLinks
	    Require all granted
	</Directory>

Para realizar esta tarea, haremos uso del módulo copy de  Ansible, que se encarga
de copiar archivos desde la ubicación donde ejecutamos  Ansible, a un directorio
en el equipo de destino. Para esto creamos primero una carpeta files y dentro ponemos
un archivo llamado wp.conf con este contenido. Paso siguiente, debemos anexar a
nuestro playbook lo siguiente

.. code:: bash

  - name: Crear sitio de Wordpress en Apache
    become: true
    copy:
      src: files/wp.conf
      dest: /etc/apache2/sites-available/wp.conf
      owner: root
      group: root
      mode: 0644

Si lo ejecutamos nuevamente al playbook, deberíamos ver el siguiente cambio

.. code:: bash

	TASK [Crear sitio de Wordpress en Apache] **************************
	changed: [nodo1.intranet]

El paso siguiente en la documentación es "habilitar" el sitio, y en la misma
se propone hacerlo por medio del comando a2ensite. Este comando lo que hace es
crear un enlace simbólico en /etc/apache2/sites-enabled que apunte a la configuración
en /etc/apache2/sites-available, por lo que sabiendo esto aprovecharemos y en nuestra
receta haremos que se cree dicho enlace simbólico. Obviamente que para esto también
hay un módulo, y se llama *file*. Este módulo permite setear permisos, configurar
enlaces, definir atributos y muchas opciones mas (ver http://docs.ansible.com/ansible/file_module.html para
mayor información). Ahora lo utilizaremos para crear este enlace simbólico.
Agreguemos lo siguiente al playbook

.. code:: bash

  - name: Habilitar sitio de Wordpress (crear enlace simbólico)
    become: true
    file:
      src: /etc/apache2/sites-available/wp.conf
      dest: /etc/apache2/sites-enabled/wp.conf
      owner: root
      group: root
      state: link

Ahora con crear el enlace no alcanza, necesitamos recargar Apache
para que tome la misma (service apache2 reload). Esto lo podríamos hacer con el
módulo *service* que hemos utilizado anteriormente, agregando lo siguiente al playbook

.. code:: bash

	- name: Recargar apache
		become: true
		service:
			name: apache2
			state: reload

Pero esto implicaría que cada vez que ejecutemos el playbook se recargue la
configuración de Apache. Lo ideal es que solo se recargue cuando agregamos la
configuración. Es en estos casos donde entra en juego otro concepto
muy potente de  Ansible, los "handlers" (manejadores). Los Handlers son tareas que
se ejecutan ante determinadas circunstancias, como por ejemplo en nuestro caso,
solo queremos que se recargue el Apache cuando se creo el enlace simbólico y no
cada vez que se ejecuta el playbook. Para definirlos, se utiliza una sintaxis muy
parecida a "task", es decir:

.. code:: bash

  handlers:
    - name: Recargar apache
      service:
        name: httpd
        state: reloaded

Luego en el playbook debemos definir que cuando se produzca un cambio en la tarea
de habilitación del sitio, se llame a este manejador. Esto se hace por medio de la
palabra reservada "notify" (reescribimos la tarea de "Habilitar
sitio de Wordpress (crear enlace simbólico)" ), el playbook completo hasta ahora
debería ser de la siguiente manera:

.. code:: bash

	---
	- hosts: all
		tasks:
		- name: Instalar Worpress y sus dependencias
			become: true
			apt:
				name: "{{ item }}"
				state: present
				update-cache: yes
			with_items:
				- wordpress
				- curl
				- apache2
				- mysql-server

		- name: Copiar configuración de Wordpress para apache2
			become: true
			copy:
				src: files/wp.conf
				dest: /etc/apache2/sites-available/wp.conf
				owner: root
				group: root
				mode: 0644

		- name: Habilitar sitio de Wordpress (crear enlace simbólico)
			become: true
			file:
				src: /etc/apache2/sites-available/wp.conf
				dest: /etc/apache2/sites-enabled/wp.conf
				owner: root
				group: root
				state: link
			notify:
				- Recargar apache

		handlers:
			- name: Recargar apache
				service:
					name: apache2
					state: reloaded

Si ahora ejecutamos el playbook veremos que se creará el enlace simbólico y se
ejecutará la tarea "Recargar apache"

.. code:: bash

	usuario@mipc:~$ ansible-playbook -i inventario playbook.yml -K
	SUDO password:

	PLAY ***************************************************************************

	TASK [setup] *******************************************************************
	ok: [nodo1.intranet]

	TASK [Instalar Worpress y sus dependencias] ************************************
	ok: [nodo1.intranet] => (item=[u'wordpress', u'curl', u'apache2', u'mysql-server'])

	TASK [Copiar configuración de Wordpress para apache2] **************************
	ok: [nodo1.intranet]

	TASK [Habilitar sitio de Wordpress (crear enlace simbólico)] *******************
	changed: [nodo1.intranet]

	RUNNING HANDLER [Recargar apache] **********************************************
	changed: [nodo1.intranet]

	PLAY RECAP *********************************************************************
	nodo1.intranet             : ok=5    changed=2    unreachable=0    failed=0


pero si lo ejecutamos nuevamente no.

.. code:: bash

	usuario@mipc:~$ ansible-playbook -i inventario playbook.yml -K
	SUDO password:

	PLAY ***************************************************************************

	TASK [setup] *******************************************************************
	ok: [nodo1.intranet]

	TASK [Instalar Worpress y sus dependencias] ************************************
	ok: [nodo1.intranet] => (item=[u'wordpress', u'curl', u'apache2', u'mysql-server'])

	TASK [Copiar configuración de Wordpress para apache2] **************************
	ok: [nodo1.intranet]

	TASK [Habilitar sitio de Wordpress (crear enlace simbólico)] *******************
	ok: [nodo1.intranet]

	PLAY RECAP *********************************************************************
	nodo1.intranet             : ok=4    changed=0    unreachable=0    failed=0


Con eso concluímos la configuración de apache. Ahora debemos avanzar con la
configuración de Wordpress y de MySQL.
Para la configuración de wordpress debemos crear un archivo en
/etc/wordpress/config-nodo1.intranet.php con ciertas variables a definir
como el nombre de usuario de conexión a la base de datos, la password, la dirección
de conexión al motor, etc. El archivo propuesto por la documentación de debian es el siguiente

.. code:: bash

	<?php
		define('DB_NAME', 'wordpress');
		define('DB_USER', 'wordpress');
		define('DB_PASSWORD', 'password');
		define('DB_HOST', 'localhost');
		define('WP_CONTENT_DIR', '/var/lib/wordpress/wp-content');
	?>

Es un buen momento para hablar de las variables en ansible. Las
variables en Ansible se pueden definir en muchos lugares, como el inventario,
el playbook o un directorio...inclusive se pueden pasar como parámetros en la
ejecución de comando ansible-playbook. Estas luego pueden invocarse desde el playbook
y permiten adaptar el comportamiento de las tareas, según el valor de las mismas.
Para nuestro caso, y por una cuestión de simplicidad las definiremos en el mismo
playbook, mediante la palabra clave vars:

.. code:: bash

	  vars:
	    wordpress_db_name: 'wordpress_db'
	    wordpress_db_user: 'wordpress'
	    wordpress_db_password: 'superpassword'
	    wordpress_db_host: 'localhost'
	    wordpress_content_dir: '/var/lib/wordpress/wp-content'

A continuación deberíamos generar el archivo antes mencionado, haciendo uso de
estas variables que hemos definido y utilizando para ello el módulo template.

El módulo "template" utiliza el motor de templates Jinja2 de python, y
permite, por medio de una sintaxis muy sencilla, generar un archivo en el destino
utilizando valores definidos en variables. Si no conoce Jinja2 no se preocupe,
el uso que le daremos aquí es muy simple, pero si le interesa aprender
más al respecto, no estaría mal visitar http://jinja.pocoo.org/. Para seguir con
nuestro ejemplo y crear el archivo de configuración de Wordpress, debemos primero
crear un archivo .j2, por ejemplo wordpress_config.j2 con el siguiente contenido

.. code:: bash

	<?php
	define('DB_NAME', '{{ wordpress_db_name }}');
	define('DB_USER', '{{ wordpress_db_user }}');
	define('DB_PASSWORD', '{{ wordpress_db_password }}');
	define('DB_HOST', '{{ wordpress_db_host }}');
	define('WP_CONTENT_DIR', '{{ wordpress_content_dir }}');
	?>

Como pueden ver, el archivo es el mismo, solo hemos reemplazado los valores, por
las variables que hemos definido en el playbook. La sintaxis para invocar a las
variables es la doble llave "{{ }}"

Luego necesitamos crear una tarea que utilice el módulo template y genere el
archivo deseado en /etc/wordpress/config-nodo1.intranet.php

.. code:: bash

		- template:
		    src: wordpress_config.j2
		    dest:  /etc/wordpress/config-nodo1.intranet.php
		    owner: www-data
		    group: www-data
		    mode: 0644

Perfecto, con esto ya hemos configurado wordpress, solo nos resta crear la Bases
de datos, el usuario y configurar permisos para el usuario definido en la
configuración de wordpress. Obviamente que la base de datos no la crearemos a mano,
sino que nos valdremos de módulos para que realicen dichas tareas. Los módulos a
utilizar en este caso son mysql_user y mysql_db. El primero permite crear y
configurar usuarios, y el segundo bases de datos. Su uso es muy sencillo, pero a
diferencia de los demás módulos, requieren que el host destino tenga instalado
el paquete python-mysqldb, por lo que modificamos la tarea
"Instalar Worpress y sus dependencias", y lo agregamos para que se instale ;) :

.. code:: bash

  - name: Instalar Worpress y sus dependencias
    become: true
    apt:
      name: "{{ item }}"
      state: present
      update-cache: yes
    with_items:
      - wordpress
      - curl
      - apache2
      - mysql-server
      - python-mysqldb

Ahora sí, agregamos las dos tareas restantes y concluimos con la creación del
usuario y de mysql y de su base de datos.

.. code:: bash

  - name: Crear usuario wordpress
    become: true
    mysql_user:
      name: '{{ wordpress_db_user}}'
      password: '{{ wordpress_db_password }}'
      priv: '*.*:ALL'
      state: present

  - name: Crear base de datos de wordpress
    become: true
    mysql_db:
      name: '{{ wordpress_db_name }}'
      login_user: '{{ wordpress_db_user }}'
      login_password: '{{ wordpress_db_password }}'
      login_host: 'localhost'
      state: present

Listo, ejecutamos el playbook y si todo sale bien, deberíamos poder ingresar a
http://nodo1.intranet/wp/ y ver que ya se encuentra Instalado y configurado Wordpress :D .
Contar con esta receta que hemos definido nos ofrece una gran flexibilidad, dado
que podemos con la misma instalar cientos de Wordpress (solo cambiando las variables
que utilizaría cada uno), e incluso podemos destruir por completo el nodo, y
simplemente ejecutando ésta receta, tendríamos todo listo para empezar a utilizar
Wordpress, incluso sin la necesidad de tener que hacer SSH al nodo, ni una sola vez.

Si combinamos esto, con el uso de repositorios, realmente podríamos tener la definición
de casi toda nuestra infraestructura en un repositorio, algo muy ágil y potente. Incluso
si un nuevo administrador se incorpora a trabajar con nosotros, y entiende de  Ansible,
rápidamente puede comprender nuestra infraestructura y no tener que pasarse meses
estudiando como están configuradas las cosas. Ansible propone como su mayor ventaja
(bajo mi exclusivo criterio), la posibilidad de definir con un lenguaje claro y
sencillo nuestra infraestructura.

El playbook final es el siguiente

.. code:: bash

	---
	- hosts: all
	  vars:
	    wordpress_db_name: 'wordpress_db'
	    wordpress_db_user: 'wordpress'
	    wordpress_db_password: 'superpassword'
	    wordpress_db_host: 'localhost'
	    wordpress_content_dir: '/var/lib/wordpress/wp-content'

	  tasks:
	  - name: Instalar Worpress y sus dependencias
	    become: true
	    apt:
	      name: "{{ item }}"
	      state: present
	      update-cache: yes
	    with_items:
	      - wordpress
	      - curl
	      - apache2
	      - mysql-server
	      - python-mysqldb

	  - name: Copiar configuración de Wordpress para apache2
	    become: true
	    copy:
	      src: files/wp.conf
	      dest: /etc/apache2/sites-available/wp.conf
	      owner: root
	      group: root
	      mode: 0644

	  - name: Habilitar sitio de Wordpress (crear enlace simbólico)
	    become: true
	    file:
	      src: /etc/apache2/sites-available/wp.conf
	      dest: /etc/apache2/sites-enabled/wp.conf
	      owner: root
	      group: root
	      state: link
	    notify:
	      - Recargar apache

	  - name: Configurar wordpress
	    become: true
	    template:
	      src: wordpress_config.j2
	      dest:  /etc/wordpress/config-nodo1.intranet.php
	      owner: www-data
	      group: www-data
	      mode: 0644

	  - name: Crear usuario wordpress
	    become: true
	    mysql_user:
	      name: '{{ wordpress_db_user}}'
	      password: '{{ wordpress_db_password }}'
	      priv: '*.*:ALL'
	      state: present

	  - name: Crear base de datos de wordpress
	    become: true
	    mysql_db:
	      name: '{{ wordpress_db_name }}'
	      login_user: '{{ wordpress_db_user }}'
	      login_password: '{{ wordpress_db_password }}'
	      login_host: 'localhost'
	      state: present

	  handlers:
	    - name: Recargar apache
	      become: true
	      service:
	        name: apache2
	        state: reloaded
