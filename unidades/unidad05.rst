.. image:: ../caratulas/unidad05.pdf
	:scale: 86%

Unidad 5: Virtualización
========================

La virtualización es uno de los avances más grandes de la informática en los
últimos años. El término abarca varias abstracciones y técnicas de simulación
de equipos virtuales con un grado variable de independencia de hardware real.
Según Wikipedia es la creación a través de software de una versión virtual de
algún recurso tecnológico, como puede ser una plataforma de hardware, un sistema
operativo, un dispositivo de almacenamiento u otros recursos de red, y creo es
una de las mejores definiciones. Nosotros nos concentraremos en la virtualización
de plataformas de hardware.

Un servidor físico puede almacenar varios sistemas que funcionan de forma
simultánea y aislada, lo que se denomina consolidación. Veamos un poco este
concepto.

Consolidación
-------------

Era común encontrar data centers donde cada aplicación alojada tenía asignado
uno o más servidores, lo que si bien era efectivo desde el punto de vista del
servicio (mayor hardware disponible para afrontar la demanda), era muy poco
eficiente desde el punto de vista del uso de los recursos de cada servidor.

Estudios demuestran que la demanda de recursos, por lo general, tiene una
naturaleza cíclica en la que se observan momentos de mucha demanda y momentos
donde la misma es muy baja ó nula.

Este comportamiento se puede observar en la siguiente figura, donde se muestra
la demanda de recursos de dos aplicaciones diferentes a lo largo del tiempo:

.. figure:: ../imagenes/unidad05/image_1.png
   :alt: TOP
   :align: center
   :scale: 160 %

   Fig.1 Naturaleza cíclica del uso de recursos

Como se observa, gran parte del tiempo (cerca de un 60%) dichas
aplicaciones no demandan recursos de hardware, utilizando por lo general solo
el 20 ó 30% de su potencia de cómputo. Cada servidor tiene un costo casi
constante a lo largo del tiempo relacionado con su consumo eléctrico,
refrigeración, reemplazo de hardware defectuoso, etc; por lo que es costoso
desaprovechar estos recursos.

Una solución eficiente a este problema es la consolidación de servicios ó
simplemente consolidación. Consiste en agrupar dos o más servicios
(aplicaciones) en un mismo servidor físico, utilizando los recursos de este de
forma más eficiente (en promedio).

.. figure:: ../imagenes/unidad05/image_2.png
   :alt: TOP
   :align: center
   :scale: 160 %

   Fig.2 Consolidación de servicios

La virtualización es la forma mas utilizada en la actualiadad para consolidar
servicios y aprovechar al máximo los rescursos de hardware disponibles, dado que
permite en mayor o menor medida (dependiendo de la tecnología que se utilice
para virtualizar) un aislamiento de las aplicaciones que conviven en un mismo
hardware.


Conceptos básicos
-----------------

La virtualización se basa en una capa de abstracción entre el hardware de la
máquina física (que denominaremos host) y el sistema operativo de la máquina
virtual (virtual machine, que denominaremos guest). Esta capa, comunmente
denominada Hypervisor o VMM (Virtual Machine Monitor), divide los recursos
en uno o más entornos de ejecución.

El hypervisor maneja, gestiona y arbitra los cuatro recursos principales de una
computadora (CPU, Memoria, Dispositivos Periféricos y Conexiones de Red) y así
podrá repartir dinámicamente dichos recursos entre todas las máquinas virtuales
definidas en el computador central. Esto hace que se puedan tener varias
maquinas virtuales ejecutándose en el mismo ordenador físico.

Hay múltiples soluciones de virtualización, cada una con sus ventajas y
desventajas. Nos concentraremos en Xen, KVM y LXC; pero otras
implementaciones notables incluyen las siguientes:

- **QEMU**

 Es un emulador en software para un equipo completo; su rendimiento está
 lejos de la velocidad que uno podría conseguir si ejecutara nativamente, pero
 esto permite ejecutar en el hardware emulado sistemas operativos sin
 modificación o experimentales. También permite emular una arquitectura de
 hardware diferente: por ejemplo, un sistema amd64 puede emular una máquina arm.
 QEMU es software libre.

- **Bochs**

 Es otra máquina virtual libre, pero sólo emula la arquitectura x86 (i386 y
 amd64).

- **VMWare**

 Es una máquina virtual privativa; como es una de las más antiguas es también una
 de las más conocidas. Funciona sobre cimientos similares a los de QEMU.

- **VirtualBox**

 Es una máquina virtual que es software libre en su mayor parte
 (algunos componentes adicionales están disponibles bajo una licencia privativa).
 Es más joven que VMWare y limitada a las arquitecturas i386 y amd64, pero
 incluye cierta compatibilidad con instantáneas y otras funcionalidades
 interesantes.

XEN
---

Xen es una solución que utiliza una técnica denominada «paravirtualización». En
la misma el hypervisor es una fina capa de abstracción entre el hardware y los
sistemas superiores, que actúa como árbitro controlando el acceso al hardware
desde las máquinas virtuales. El hypervisor en este caso sólo gestiona unas pocas
instrucciones, las demás se ejecutan directamente en el hardware en nombre de
los sistemas. La principal ventaja es que no se degrada el rendimiento y los
sistemas se ejecutan a velocidades cercanas a la nativa; la desventaja es que el
núcleo de los sistemas operativos que uno desee utilizar en un hypervisor Xen
necesita ser adaptado para correr sobre Xen.

Conceptos en XEN
~~~~~~~~~~~~~~~~

El hypervisor es la capa más baja que se ejecuta directamente en el hardware,
inclusive debajo del núcleo (kernel). Este hypervisor puede dividir el resto del
software entre varios dominios («domains»), los que pueden interpretarse como
máquinas virtuales. Se conoce a uno de estos dominios (el primero en iniciar)
como dom0 y tiene un rol especial ya que sólo este dominio puede controlar al
hypervisor y la ejecución de otros dominios. Se conocen a los otros dominios
como domU. En otras palabras, desde el punto de vista del usuario, el dom0 es el
«anfitrión» de los demás sistemas de virtualización, mientras que los domU son
sus «huéspedes» (las maquinas virtuales).

Virtualización vs Paravirtualización
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Xen no solo permite utilizar la tecnica de paravirtualización, sino que también
nos ofrece otra técnica denominada HVM o 'Full Virtualization', es decir,
virtualización completa, que consiste en la instalación de un domU como si
fuera un host independiente.

El usar HVM tiene la ventaja de que se puede virtualizar casi cualquier cosa,
incluído Windows, ya que no es necesario tener un kernel especial para la
virtualización, pero su principal desventaja es el rendimiento, ya que ciertos
componentes deben ser emulados, es decir, se debe realizar una traducción
entre los pedidos de las maquinas virtuales y el hardware real.

Otra desventaja de HVM, es que necesitamos que el procesador soporte el conjunto
de instrucción de virtualización. Estas instrucciones en plataformas Intel son
llamadas *Intel-VT* y en plataformas AMD se llaman *AMD-V*. Cabe destacar que
en la actualidad, la mayor parte del hardware disponible ya las incluye.

KVM
---

KVM, acrónimo de máquina virtual basada en el núcleo («Kernel-based Virtual
Machine»), es primero que nada un módulo del núcleo que provee la mayor parte
de la infraestructura que puede usar un virtualizador, pero no es un
virtualizador en sí mismo. El control real de la virtualización es gestionado
por una aplicación basada en QEMU.

A diferencia de otros sistemas de virtualización, se integró KVM al núcleo
Linux desde el comienzo. Sus desarrolladores eligieron aprovechar el conjunto
de instrucciones de procesador dedicados a la virtualización (Intel-VT y AMD-V),
lo que mantiene a KVM liviano, elegante y no muy hambriento de recursos.

La contraparte, obviamente, es que KVM no funciona en ordenadores con
procesadores distintos a estos.

La técnica utilizada por KVM es la full-virtualización, por lo que cualquier
sistema operativo puede ejecutarse sobre el mismo, dado que no se requiere la
modificación de los mismos para su ejecución en un entorno virtualizado. Esto ha
dado a KVM cierto prestigio, dada su flexibilidad.


LXC
---
LXC se basa en una técnica conocida como *Virtualización basada en Sistema
Operativo*. No es estrictamente hablando un virtualizador, de hecho no
posee un hypervisor, sino que mas bien es un sistema para aislar grupos de
procesos entre sí aún cuando estos se ejecutan en el mismo equipo. Aprovecha un
conjunto de evoluciones recientes del núcleo Linux, conocidos colectivamente
como grupos de control («control groups»), mediante los que diferentes conjuntos
de procesos llamados «grupos» tienen diferentes visiones de ciertos aspectos de
todo el sistema.

Entre estos aspectos, los más notables son los identificadores de procesos,
la configuración de red y los puntos de montaje.

Un grupo de procesos aislados no podrá acceder a otros procesos en el sistema y
puede restringir su acceso al sistema de archivos a un subconjunto específico.
También puede tener su propia interfaz de red y tabla de enrutamiento y se puede
configurar para que sólo pueda ver un subconjunto de los dispositivos
disponibles que están presentes en el sistema.

Puede combinar estas funcionalidades para aislar una familia de procesos
completa que inicia desde el proceso init, y el conjunto resultante es muy
similar a una máquina virtual. El nombre oficial de esta configuración es
«contenedor» (de allí LXC: contenedores Linux, «LinuX Containers»), pero una
diferencia importante con máquinas virtuales «reales» como aquellas provistas
por Xen o KVM es que no hay un segundo núcleo; el contenedor utiliza el mismo
núcleo que el sistema anfitrión. Esto tiene tanto ventajas como desventajas:
las ventajas incluyen un rendimiento excelente debido a una falta completa de
sobrecarga y el hecho de que el núcleo tiene una visión global de todos los
procesos que ejecutan en el sistema por lo que la gestión de procesos puede
ser más eficiente que si existieran dos núcleos independientes administrando
conjuntos de tareas. La mayor de las desventajas es la imposibilidad de ejecutar
un núcleo diferente en un contenedor (sea una versión diferente de Linux o
directamente un sistema operativo distinto).

Diferencias entre las diferentes técnicas de virtualización
-----------------------------------------------------------

A continuación se muestra gráficamente las diferencias entre las diferentes
técnicas.

.. figure:: ../imagenes/unidad05/image_3.png
   :alt: TOP
   :align: center
   :scale: 60 %

   Fig.3 Diferencias entre las diferentes técnicas de virtualización

Ejemplo práctico
----------------

A continuación veremos como instalar y crear maquinas virtuales con XEN. La idea
de esta práctica es tener una noción mínima desde el punto de vista funcional
de esta herramienta que nos permite crear tanto instancias (maquinas virtuales)
full-virtualizadas como paravirtualizadas. También veremos como crear contenedores
con LXC.

Maquinas virtuales con XEN
~~~~~~~~~~~~~~~~~~~~~~~~~~


Utilizar Xen en Debian requiere tres componentes:

- El hypervisor en sí mismo.

- Un núcleo que ejecuta sobre dicho hypervisor.

- Si se trata de la arquitectura i386 también se necesita una biblioteca estándar
  con los parches apropiados para aprovechar Xen; la que se encuentra en el
  paquete libc6-xen.


Para poder evitar la molestia de seleccionar estos componentes a mano, tiene
disponibles varios metapaquetes para cada arquitectura. En el caso de la
arquitectura amd64, este se instala de la de la siguiente manera

.. code:: bash

    root@servidor:~# apt-get install xen-linux-system-amd64


Este metapaquete incluye una combinación de paquetes del núcleo e hypervisor que
funcionan bien en Debian Jessie. El hypervisor también incluirá xen-utils-4.4,
que contiene las herramientas para controlar el hypervisor desde el dom0. A su
vez, éste incluirá la biblioteca estándar apropiada.

Durante la instalación de todo esto, los scripts de configuración también
crearán un nuevo elemento en el menú del gestor de arranque Grub para iniciar
el núcleo elegido en un dom0 Xen. Sin embargo generalmente éste no será el
primero en la lista y, por lo tanto, no estará seleccionado de forma
predeterminada. Si este no es el comportamiento que desea, ejecutar lo
siguiente lo cambiará:

.. code:: bash

    root@servidor:~# mv /etc/grub.d/20_linux_xen /etc/grub.d/09_linux_xen
    root@servidor:~# update-grub

El siguiente paso es probar el comportamiento del dom0 en sí mismo; esto incluye
reiniciar para utilizar el hypervisor y núcleo Xen. El sistema debería iniciar
como siempre, con unos pocos mensajes adicionales en la consola durante los
primeros pasos de inicialización.

Ahora es el momento de instalar sistemas útiles en los sistemas domU, utilizando
las herramientas en xen-tools. Este paquete provee el programa **xen-create-image**,
que automatiza en gran parte esta tarea. El único parámetro obligatorio es
**--hostname**, que le da un nombre al domU; otras opciones son importantes, pero
puede guardarlas en el archivo de configuración /etc/xen-tools/xen-tools.conf y
si no las especificamos no se generará ningún error. Por lo tanto es importante
revisar el contenido de este archivo antes de crear imágenes o utilizar los
parámetros adicionales en la invocación de xen-create-image. Los parámetros
importantes a saber incluyen los siguientes:

- **--memory** para especificar la cantidad de RAM dedicada a este nuevo sistema
  creado (cabe aclarar que al momento de iniciar la maquina virtual,  esta
  memoria será tomada de la memoria principal del host).

- **--size** y **--swap** para definir el tamaño de los «discos virtuales»
  disponibles al domU;

- **--debootstrap** para causar que se instale el nuevo sistema con debootstrap;
  en tal caso, generalmente también utilizará la opción --dist (con el nombre de
  una distribución como jessie).

- **--dhcp** indica que el domU debe obtener su configuración de red a través de
  DHCP, mientras que **--ip** permite definir una dirección IP estática.

Por último, se debe elegir un método de almacenamiento para las imágenes a crear
(que el domU verá como discos duros). El método más simple, que corresponde a la
opción **--dir**, es crear un archivo en el dom0 para cada dispositivo que se le
provee al domU. La alternativa en sistemas que utilizan LVM es la opción **--lvm**
seguida del nombre de un grupo de volúmenes; xen-create-image luego creará un
nuevo volumen lógico dentro de dicho grupo y éste estará disponible en el domU
como un disco duro.

Finalmente podemos crear una maquina virtual con el siguiente comando:

.. code:: bash

	root@servidor:~# xen-create-image --hostname testxen --dhcp --dir /srv/testxen --size=2G --dist=jessie --role=udev

	[…]
	General Information
	--------------------
	Hostname       :  testxen
	Distribution   :  jessie
	Mirror         :  http://ftp.debian.org/debian/
	Partitions     :  swap            128Mb (swap)
		          /               2G    (ext3)
	Image type     :  sparse
	Memory size    :  128Mb
	Kernel path    :  /boot/vmlinuz-3.16.0-4-amd64
	Initrd path    :  /boot/initrd.img-3.16.0-4-amd64
	[…]
	Logfile produced at:
		 /var/log/xen-tools/testxen.log

	Installation Summary
	---------------------
	Hostname        :  testxen
	Distribution    :  jessie
	MAC Address     :  00:16:3E:8E:67:5C
	IP-Address(es)  :  dynamic
	RSA Fingerprint :  0a:6e:71:98:95:46:64:ec:80:37:63:18:73:04:dd:2b
	Root Password   :  adaX2jyRHNuWm8BDJS7PcEJ

Ahora tenemos una máquina virtual, pero no está ejecutando (por lo tanto sólo
utiliza espacio en el disco duro del dom0). Por supuesto, podemos crear más
imágenes, posiblemente con diferentes parámetros.

Antes de encender estas máquinas virtuales, necesitamos definir cómo accederemos
a ellas. Por supuesto, podemos considerarlas máquinas aisladas a las que sólo
podemos acceder a través de su consola de sistema, pero rara vez esto pueda ser
muy útil. La mayoría de las veces, consideraremos un domU como un servidor
remoto al que sólo podemos acceder a través de la red. Sin embargo,
sería un gran inconveniente agregar una tarjeta de red para cada domU; es por
esto que Xen permite crear interfaces virtuales que cada dominio puede ver y
utilizar de la forma estándar. Estas tarjetas, aunque sean virtuales, sólo serán
útiles cuando estén conectadas a una red, inclusive una virtual. Xen tiene varios
modelos de red para esto:

- El modelo más simple es el modelo puente («bridge»); todas las tarjetas de red
  eth0 (tanto en los sistemas domU como en el dom0) se comportarán como si
  estuvieran conectadas directamente a un switch Ethernet.

- Luego está el modelo enrutamiento («routing») en el que el dom0 se comporta
  como el router entre los sistemas domU y la red (física) externa.

- Finalmente, en el modelo NAT, nuevamente el dom0 se encuentra entre los
  sistemas domU y el resto de la red, pero no se puede acceder a los sistemas
  domU directamente desde afuera y el tráfico atraviesa una traducción de
  direcciones de red en el dom0.

Estos tres modos de red involucran una cantidad de interfaces con nombres
inusuales, como vif*, veth*, peth* y xenbr0. El hypervisor Xen los acomoda en la
distribución definida bajo el control de las herramientas en espacio de usuario.
Debido a que los modelos NAT y de enrutamiento sólo se adaptan a casos
particulares nos concentraremos en el modelo de puente (bridge).

La configuración estándar de los paquetes Xen no modifica la configuración de
red del sistema. Sin embargo, se configura el demonio xend para integrar las
interfaces de red virtuales en un puente de red preexistente (xenbr0 tiene
precedencia si existen varios de ellos). Por lo tanto, debemos configurar un
puente en /etc/network/interfaces (lo que requiere que instalemos el paquete
bridge-utils, razón por la que lo recomienda el paquete xen-utils-4.4) para
reemplazar el elemento eth0 existente:

.. code:: bash

	auto xenbr0
	iface xenbr0 inet dhcp
	    bridge_ports eth0
	    bridge_maxwait 0

Esto asocia la interfaz física eth0 al puente xenbr0, luego cada maquina virtual
que se cree se asociara a una interfaz en este puente, por lo que la misma estara
conectada a un switch virtual en el que también esta conectada la interfaz eth0.

Luego de reiniciar para asegurarse que se crea el puente automáticamente, podemos
iniciar el domU con las herramientas de control de Xen, en particular el programa
xl. Este programa permite varias manipulaciones de los dominios, entre ellas:
enumerarlos, iniciarlos y detenerlos.

.. code:: bash

	root@servidor:~# xl list
	Name                                        ID   Mem VCPUs      State   Time(s)
	Domain-0                                     0   463     1     r-----      9.8
	# xl create /etc/xen/testxen.cfg
	Parsing config from /etc/xen/testxen.cfg
	# xl list
	Name                                        ID   Mem VCPUs      State   Time(s)
	Domain-0                                     0   366     1     r-----     11.4
	testxen                                      1   128     1     -b----      1.1


Listo, nuestra máquina virtual está iniciando. Podemos acceder a ella de dos
formas. La forma usual es conectarnos «remotamente» a través de la red, como lo
haríamos con una máquina real; esto usualmente requerirá configurar un servidor
DHCP o alguna configuración de DNS. La otra forma, que puede ser la única forma
si la configuración de red era incorrecta, es utilizar la consola hvc0 ejecutando
xl console:

.. code:: bash

	root@servidor:~# xl console testxen
	[…]

	Debian GNU/Linux 8 testxen hvc0

	testxen login:

Uno puede abrir una sesión, tal como si estuviera sentado frente al teclado de
la máquina virtual. Puede desconectarse de esta consola con la combinación de
teclas Control+].

Una vez que el domU está ejecutando, puede utilizarlo como cualquier otro servidor
(al fin y al cabo es un sistema GNU/Linux). Sin embargo, su existencia como
máquina virtual permite cierta funcionalidad adicional. Por ejemplo, puede pausar
y resumir temporalmente un domU, ejecutando xl pause y xl unpause. Sepa que aunque
un domU pausado no utiliza el procesador, la memoria reservada a él sigue en uso.
Puede ser interesante considerar las órdenes xl save y xl restore: guardar un
domU libera los recursos utilizados por este domU, incluyendo la RAM. Cuando
restaure (o resuma) un domU, éste no notará nada a excepción del paso del tiempo.
Si un domU está ejecutando cuando se apague el dom0, los scripts empaquetados
automáticamente guardarán el domU y lo restaurarán cuando vuelva a iniciar. Esto,
por supuesto, tiene los mismos inconvenientes estándar que cuando hiberna un
equipo portátil, por ejemplo; en particular, si se suspende por demasiado tiempo
al domU, pueden expirar las conexiones de red. Sepa también que, hasta el momento,
Xen es incompatible con gran parte de la gestión de energía ACPI, lo que evita
que pueda suspender el sistema anfitrión (dom0).

Contenedores con LXC
~~~~~~~~~~~~~~~~~~~~

El primer paso será instalar LXC y otros paquetes necesarios

.. code:: bash

	root@servidor:~# apt-get install lxc rsync debootstrap


A continuación, y al igual que se hizo en el caso de XEN, debemos configurar
la red para que trabaje en modo bridge. A este bridge conectaremos los diferentes
contenedores, los cuales tendrán una interfaz virtual.

Para gestionar los bridge necesitamos del paquete bridge-utils

.. code:: bash

	root@servidor:~# apt-get install bridge-utils

Al igual que en el caso de anterior, debemos definir un bridge y asociar la
interfaz física a dicho bridge. Editamos para esto el archivo
**/etc/network/interface**

.. code:: bash

	#auto eth0
	#iface eth0 inet dhcp

	auto br0
	iface br0 inet dhcp
	  bridge-ports eth0

Configuremos ahora el sistema que utilizará el contenedor. Debido a que esta
«máquina virtual» no ejecutará directamente sobre el hardware, son necesarios
algunos ajustes comparados con un sistema de archivos estándar, especialmente
en aquello que involucra al núcleo, los dispositivos y las consolas. Afortunadamente,
el paquete lxc incluye scripts que automatizan la mayoría de esta configuración.
Por ejemplo, las siguientes órdenes instalará un contenedor Debian:

.. code:: bash

	root@servidor:~# lxc-create -n testlxc -t debian
	debootstrap is /usr/sbin/debootstrap
	Checking cache download in /var/cache/lxc/debian/rootfs-jessie-amd64 …
	Downloading debian minimal ...
	I: Retrieving Release
	I: Retrieving Release.gpg
	[…]
	Download complete.
	Copying rootfs to /var/lib/lxc/testlxc/rootfs…
	[…]
	Root password is 'n6D4nsZT', please change !
	root@servidor:~#

Inicialmente se crea el sistema de archivos en /var/cache/lxc y luego
es mudado a su directorio de destino. Esto permite crear contenedores idénticos
mucho más rápido ya que luego sólo necesita copiarlo.

El script de creación de plantillas acepta la opción --arch para especificar la
arquitectura del sistema a instalar y la opción --release si desea instalar algo
diferente a la versión estable actual de Debian.

El sistema de archivos recientemente creado ahora contiene un sistema Debian
mínimo y, de forma predeterminada, el contenedor no tendrá interfaz de red
(con el permiso de la interfaz local de loopback). Debido a que esta no es la
configuración deseada, editaremos el archivo de configuración del contenedor
(/var/lib/lxc/testlxc/config) y agregar algunos elementos lxc.network.*:

.. code:: bash

	lxc.network.type = veth
	lxc.network.flags = up
	lxc.network.link = br0
	lxc.network.hwaddr = 4a:49:43:49:79:20

Estas líneas significan, respectivamente, que se creará una interfaz virtual en
el contenedor; que será iniciada automáticamente cuando inicie el contenedor;
que será conectada automáticamente al puente br0 en el anfitrión; y que su
dirección MAC será la especificada. En caso que esta última línea no exista o
esté desactivada, se generará una dirección MAC aleatoria.

Otro elemento útil en dicho archivo es la configuración del nombre del equipo:

.. code:: bash

	lxc.utsname = testlxc


Ahora que nuestra máquina virtual está lista, iniciemos el contenedor:

.. code:: bash

	root@servidor:~# lxc-start --daemon --name=testlxc
	root@servidor:~# lxc-console -n testlxc
	Debian GNU/Linux 8 testlxc tty1

	testlxc login: root
	Password:
	Linux testlxc 3.16.0-4-amd64 #1 SMP Debian 3.16.7-ckt11-1 (2015-05-24) x86_64

	The programs included with the Debian GNU/Linux system are free software;
	the exact distribution terms for each program are described in the
	individual files in /usr/share/doc/*/copyright.

	Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
	permitted by applicable law.
	root@testlxc:~#

Ahora estamos dentro del contenedor; nuestro acceso a los procesos está
restringido a aquellos iniciados dentro del mismo contenedor y nuestro acceso
al sistema de archivos está limitado de forma similar al subconjunto dedicado
del sistema de archivos completo (/var/lib/lxc/testlxc/rootfs). Podemos salir a
la consola con *Control+a+q*.

Tenga en cuenta que ejecutamos el contenedor como un proceso en segundo plano
gracias a la opción --daemon de lxc-start. Podemos interrumpir el contenedor
ejecutando **lxc-stop --name=testlxc**.

El paquete lxc contiene un script de inicialización que puede automatizar el
inicio de uno o más contenedores cuando el sistema principal arranca
(confía en el comando lxc-autostart el cual inicia los contenedores que tienen
la opción lxc.start.auto configurada a 1). Se puede obtener un control más
detallado del orden de inicio con lxc.start.order y lxc.group: por defecto, el
script de inicialización inicia los contenedores que son parte del grupo onboot
y luego los contenedores que no forman parte de este grupo. En ambos casos el
orden dentro de un grupo es definido por la opción lxc.start.order.
