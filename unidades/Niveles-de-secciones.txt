Titulo 1
========

Subtitulo
---------

Titulo 2
~~~~~~~

Titulo 3
^^^^^^^^

Titulo 4
''''''''

Titulo 5 
""""""""

Titulo 6
~~~~~~~~ 

Titulo 7
^^^^^^^^ 

Titulo 8
________

Titulo 9
********

Titulo 10
+++++++++

Titulo 11
#########

Titulo 12
<<<<<<<<<

Titulo 13
>>>>>>>>>

.. figure:: ../imagenes/unidad05/image_1.png
   :alt: TOP
   :align: center
   :scale: 160 %


.. code:: bash
	root@servidor:~# lxc-create -n testlxc -t debian
	debootstrap is /usr/sbin/debootstrap
	Checking cache download in /var/cache/lxc/debian/rootfs-jessie-amd64 …
	Downloading debian minimal ...
	I: Retrieving Release 
	I: Retrieving Release.gpg 
	[…]
	Download complete.
	Copying rootfs to /var/lib/lxc/testlxc/rootfs…
	[…]
	Root password is 'n6D4nsZT', please change !
	root@servidor:~# 
