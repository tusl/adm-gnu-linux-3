.. image:: ../caratulas/unidad04.pdf
	:scale: 86%

Unidad 4: Monitorización de servicios y recursos
================================================

Una de las herramientas más importantes en la vida de un administrador de
sistemas es el monitoreo de los servicios y recursos que administra. Esto
permite ver el comportamiento de los sistemas, anticipar posibles fallas,
predecir la falta de recursos o reaccionar de forma inmediata ante un
inconveniente. Es una herramienta muy valiosa que permite estar siempre un paso
adelante, y que por lo tanto vale la pena tomar con seriedad. Un viejo amigo
solía decir que un buen administrador es un completo desconocido para sus
usuarios, dado que anticipa gran parte de los posibles inconvenientes, y por lo
tanto no tiene que escuchar las quejas de estos. El monitoreo es la clave si
decidimos emprender ese camino.

Herramientas de monitoreo
-------------------------

Existe una gran variedad de herramientas para monitorear, que van desde la
posibilidad de monitorear un equipo a monitorear varios equipos en simultáneo.

Herramientas de monitoreo para un solo host
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Una de las formas más básicas de monitoreo de un servidor, es por medio de
herramientas que nos muestren el estado actual del equipo. A continuación
hablaremos de las mas utilizadas

TOP
^^^

Top nos permite una mirada rápida y detallada de nuestro sistema, el mismo
muestra en tiempo real un listado de los procesos que se están ejecutando,
indicando el uso de CPU, RAM, tiempo de ejecución, etc.

.. figure:: ../imagenes/unidad04/image_1.png
   :alt: TOP
   :align: center
   :scale: 98 %

   Fig.1 Herramienta TOP

HTOP
^^^^

Al igual que TOP, esta herramienta nos permite visualizar todos los procesos
en ejecución de una forma más visual, e identificar aquellos procesos que
consumen demasiada memoria o procesador, a la vez que vemos como se distribuye
la carga entre los distintos núcleos del sistema.

.. figure:: ../imagenes/unidad04/image_2.png
   :alt: HTOP
   :align: center
   :scale: 98 %

   Fig.2 HTOP

IOTOP
^^^^^

Esta herramienta, de aspecto similar a top, es utilizada para ver el uso que
hacen los procesos de la entrada/salida del sistema. Principalmente el acceso
al disco rígido, tanto para lectura como escritura. Muy interesante para
detectar procesos que leen o escriben demasiado en el disco, y por lo tanto
generan carga en el sistema.

.. figure:: ../imagenes/unidad04/image_3.png
   :alt: IOTop
   :align: center
   :scale: 98 %

   Fig.3 IOTOP


IFTOP
^^^^^

Siguiendo en la misma línea, IFTOP nos permite visualizar las diferentes
conexiones desde nuestro equipo hacia equipos externos, y analizar el uso de
ancho de banda que consumen las distintas conexiones, como así también el
consumo total de las interfaces.

.. figure:: ../imagenes/unidad04/image_4.png
   :alt: Monitoreo interno vs Monitoreo externo
   :align: center
   :scale: 98 %

   Fig.4 IFTOP


DSTAT
^^^^^
Dstat es un reemplazo de los anteriores, que permite visualizar todos los
recursos del sistema de forma instantánea. Por ejemplo el acceso al disco
en combinación con las interrupciones a la controladora de disco, o 	comparar
el ancho de banda de la red, el uso del cpu, la carga del sistema y la I/O
del disco en el mismo intervalo.

.. figure:: ../imagenes/unidad04/image_8.png
  :alt: Monitoreo interno vs Monitoreo externo
  :align: center
  :scale: 180 %

  Fig.5 Dstat

Herramientas de monitoreo para multiples hosts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A la hora de monitorear varios hosts o servidores en una red, las herramientas
antes mencionadas no son de mucha utilidad. Para estos casos necesitamos
herramientas que se encarguen de relevar y analizar los recursos y servicios de
forma automática, informándonos ante anomalías ó inconvenientes, y
permitiéndonos hacer una análisis de los datos históricos para dimensionar el
uso y anticipar futuros problemas (por ejemplo el uso del almacenamiento).

Formas de realizar el monitoreo
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

La monitorización puede ser tanto interna como externa, dependiendo del lugar
donde se ubique el equipo encargado de monitorear los servidores o la red.

Para ciertas circunstancias la monitorización externa es mucho más fiable,
puesto que es independiente de los problemas que puede haber dentro de la red
donde se encuentra/an el/los equipo/s a monitorear. Sin embargo, la
monitorización interna es mucho mas flexible y segura, dado que se puede
monitorear un número mayor de variables, sin exponer demasiado los equipos, o la
información de estos.

.. figure:: ../imagenes/unidad04/image_5.png
   :alt: Monitoreo interno vs Monitoreo externo
   :align: center
   :scale: 40 %

   Fig.6 Monitoreo interno vs Monitoreo externo

Que podemos monitorear
^^^^^^^^^^^^^^^^^^^^^^

Podríamos decir que casi todo se puede monitorear, desde cuestiones como:
uso de cpu, memoria, espacio libre de disco, cantidad de procesos corriendo,
cantidad de usuarios conectados, cantidad de inodos libres, etc; a cuestiones
como el proceso de login dentro de un sistema, el tiempo de respuesta ante un
determinado proceso web, la temperatura y humedad en el centro de datos ó la
energía consumida por el mismo, etc.

Para poder monitorear estas variables debemos contar con algún
mecanismo de obtención de estos datos. Existen diversos métodos, siendo los más
utilizados el protocolo SNMP y el uso de agentes. SNMP (simple network managed
protocol), es un estandar muy difundido que explicaremos más adelante. Respecto
de los agentes, estos son programas que se instalan en los equipos a monitorear,
y cada cierto tiempo envían información a un nodo central, o también puede ser
que sean consultados periódicamente por este nodo. Cada herramienta propone el
suyo, y la mayoría cuentan con cierta flexibilidad, permitiéndonos monitorear
un sin número de variables predefinidas o incluso crear nuestras propias
variables.


Componentes de un sistema de monitoreo
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Un buen sistema de monitoreo debe ser capaz de censar periódicamente
determinadas variables del sistema; reaccionar ante determinadas circunstancias y
emitir una notificación o alerta; informar cuando el inconveniente se ha resuelto,
o las variables monitoreadas vuelven a un estado normal. A su vez debe soportar
el monitoreo de muchos sistemas en simultáneo. Para llevar adelante este cometido
la mayoría de los sistemas de monitoreo cuentan con los siguientes componentes:

- Agentes para la obtención de datos: Estos pueden ser internos, o externos. En
  el caso de los agentes internos, se instalan en los sistemas, recolectan
  información y periódicamente la envían al servidor de monitoreo. Los agentes
  externos por lo general se ejecutan en el servidor de monitoreo y consideran
  al sistema a monitorear como una caja negra, realizando determinados test de
  manera periódica como por ejemplo medir el tiempo de respuesta de un ping,
  si un puerto se encuentra abierto, o realizar determinadas consultas por
  medio del protocolo HTTP.

- Sistema de notificaciones: Ante determinadas condiciones, el sistema debe
  ser capaz de emitir alertas. Estas por lo general son canalizadas a través de
  diversos medios como e-mails, SMS, jabber o simplemente la visualización en un
  tablero web.

- Base de datos de históricos: El poder almacenar los valores de determinadas
  variables a lo largo del tiempo, nos permite analizar el comportamiento de un
  sistema respecto de un conjunto de variables, para saber entre otras cosas,
  como evoluciona el uso, que podemos considerar como una situación normal, y en
  que períodos ocurrieron eventos extraordinarios. Esta información es muy
  relevante a la hora de tomar decisiones respecto de por ejemplo adquirir nuevo
  equipamiento.


.. figure:: ../imagenes/unidad04/image_6.png
   :alt: Componentes de un sistema de monitoreo
   :align: center
   :scale: 70 %

   Fig.7 Componentes de un sistema de monitoreo

Protocolo SNMP
^^^^^^^^^^^^^^

El Protocolo Simple de Administración de Red o SNMP (del inglés Simple Network
Management Protocol) es un protocolo que facilita el intercambio de información
de administración entre dispositivos de red. Los dispositivos que normalmente
soportan SNMP incluyen routers, switches, servidores, estaciones de trabajo,
impresoras y muchos más.

Permite a los administradores supervisar el funcionamiento de la red, buscar y
resolver sus problemas, y planear su crecimiento.

Se compone de un conjunto de normas para la gestión de la red, incluyendo
una capa de aplicación del protocolo , una base de datos de esquema, y un
conjunto de objetos de datos. Las versiones de SNMP más utilizadas son SNMP
versión 1 (SNMPv1) y SNMP versión 2 (SNMPv2), aunque también existe la versión
SNMPv3.

Las variables accesibles a través de SNMP están organizadas en jerarquías. Estas
jerarquías y otros metadatos (tales como el tipo y la descripción de la
variable), se describen por Bases de Información de Gestión (MIB).

Los puertos utilizados por el protocolo son el 161 y 162. En el 161 escuchan los
dispositivos las peticiones de información (las cuales realiza el servidor de
monitoreo de forma periódica). El puerto 162 se usa a la inversa, son los
dispositivos los que envían información al servidor de monitoreo cuando se cumplen
determinadas situaciones (por ejemplo una tormenta de broadcast en un puerto
determinado). Este último mecanismo en SNMP se denomina Traps (trampas).
Este protocolo puede ser instalado en GNU/Linux, mediante el paquete **snmpd**.

Herramientas de monitoreo
^^^^^^^^^^^^^^^^^^^^^^^^^

Son muchas las herramientas disponibles en GNU/Linux que cumplen con todo lo
anterior. Entre las mas interesantes podemos destacar:

- Nagios

- Cactic

- Zabbix

Hablaremos un poco de Zabbix, ya que es uno de los mas completos y potentes.

Zabbix
^^^^^^

Fue diseñado para monitorear la performance y disponibilidad de diferentes
componentes de una infraestructura IT. Permite obtener diferentes tipos de datos
de una red en tiempo real de manera sencilla, eficiente y escalable. Permite
monitorear miles de dispositivos sin perder la flexibilidad y sin tener
que realizar grandes cambios en el equipamiento utilizado para el monitoreo.

Cuenta con monitoreo distribuido mediante el uso de proxies de zabbix, lo que
nos permite por ejemplo monitorear diferentes sucursales de una empresa, desde
una única ubicación, sin tener que recolectar todos los datos en un nodo central,
es decir: ubicar un proxy de zabbix en cada sucursal y gestionarlo desde un
nodo central, lo que permite aprovechar las ventajas ofrecidas tanto por el
monitoreo interno como el externo.

Su interfaz web es muy intuitiva y sencilla de usar, y cuenta con gráficos de
diferentes tipos para analizar múltiples variables en simultáneo.

Su sistema de alarmas o triggers es muy sofisticado y flexible, y nos permite
personalizar cuando una alarma debe emitirse y porque medio (por ejemplo, que se
muestre en el panel web por un minuto, y que a los 5 minutos se envíe un e-mail).

Pero sin lugar a dudas, el agente de zabbix es uno de los mas destacados de esta
herramienta, dado que por defecto nos permite monitorear múltiples variables
predefinidas de un host, con un mínimo de configuraciones para conseguirlo. Otra
de sus virtudes es el uso de templates, los que simplifican la configuración de
los hosts a monitorear.

Conceptos
'''''''''

Es importante entender los diferentes términos del mundo zabbix para rápidamente
entrar en sintonía. Los mas releevantes son los siguientes:

- Host

	dispositivo de red con IP que se desea monitorear.

- Host group

	Una agrupación lógica de hosts. Pueden contener tanto hosts como
	templates. Se utilizan principalmente para otorgar permisos de acceso a un
	determinado grupo de usuarios.

- Item

	Una variable que se puede medir en el tiempo. Es realmente el corazón
	del monitoreo, puede ser el espacio disponible de disco, la memoria libre, la
	cantidad de instancias de un servicio corriendo, etc.

- Trigger

	Expresión lógica que define un límite que es utilizado para evaluar
	los valores de los ítems. Cuando el límite se supera los triggers cambian del
	estado "OK" al estado "Problem" disparando eventos asociados a este cambio (por
	ejemplo enviar un e-mail notificando el problema). Cuando el valor del/los item/s
	vuelve a estar por debajo del límite, el estado del trigger cambia de "Problem" a
	"OK" originando un nuevo evento, el que puede asociarse con la acción de enviar
	un nuevo e-mail notificando la vuelta a la normalidad.

- Evento

	Ocurrencia de algo que requiere atención, como por ejemplo el cambio de
	estado de un trigger.

- Action (Acción)

	Reacción asociada a un evento. Las acciones consisten en
	operaciones como envió de e-mails, ejecución de scripts ó comandos remotos, etc.
	La ejecución de comandos puede ser muy útil a la hora de tomar una primer medida
	de acción de forma automática, por ejemplo supongamos que el servidor web se ha
	detenido: podríamos intentar levantarlo mediante la ejecución de un comando.

- Notification (Notificación)

	Mensaje acerca de un evento, que es enviado al usuario por medio de un canal
	predefinido.

- Remote command (Comando remoto)

	Comando predefinido que es ejecutado en un host monitoreado, luego de que se
	cumpla un/unas condición/es predeterminadas.

- Template (Plantilla)

	Conjunto de entidades como items, triggers, graficas, etc) listas para ser
	aplicadas a uno o varios hosts.

- Application (Aplicación)

	Agrupación de items en un grupo lógico.

- Web scenario (Escenario web)

	Una o multiples peticiones HTTP utilizadas para chequear la disponibilidad de un
	sitio web.

- Zabbix agent (Agente de zabbix)

	Un proceso ejecutandose en el host objeto del monitoreo, activamente
	monitoreando los recursos locales de dicho host.

Uso de zabbix
`````````````
El frontend de zabbix es muy intuitivo, y nos presenta en un primer pantallazo
un estado general de los sistemas monitoreados y del servidor
de monitoreo.

.. figure:: ../imagenes/unidad04/image_7.png
   :alt: Tablero de control de zabbix
   :align: center
   :scale: 130 %

Entre la información más importante, podemos destacar los últimos triggers que
se dispararon con sus notificaciones, y el estado general del servidor.

Configurar zabbix consiste en agregar host para monitorear, asociar a cada uno
de ellos determinados templates (muchos ya predefinidos) o items, y configurar
determinadas alarmas (triggers).


Ejemplo práctico
^^^^^^^^^^^^^^^^

Para investigar un poco mas zabbix, utilizaremos una de las versiones disponibles
para descargar desde la página de Zabbix (https://www.zabbix.com/download),
denominada *Zabbix Appliance*. Estas imágenes están instaladas y
pre-configuradas, listas para un rápido deploy. Utilizaremos la versión
disponible para Virtualbox, en formato "Open virtualization format (.ovf)".

Nuestro objetivo en esta práctica será monitorear la maquina virtual que hemos
utilizado en la unidad 1.

Se propone seguir el video tutorial donde se explica el uso y configuración
de los hosts en zabbix.
