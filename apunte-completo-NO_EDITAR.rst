.. image:: caratulas/apunte-completo.pdf
	:scale: 86%
	
	
﻿.. image:: caratulas/unidad01.pdf
	:scale: 86%

Introducción
============

En esta materia daremos continuidad a lo aprendido en *Administración
GNU/Linux I y II*, profundizando en un conjunto de herramientas y
conceptos que están presentes en la vida diaria de un administrador de
sistemas.

En particular, haremos especial énfasis en los sistemas de
almacenamiento; la implementación de políticas de seguridad en una red
por medio de cortafuegos (firewalls); la instauración de redes privadas
virtuales para poder brindar un servicio de teletrabajo (entre otras
cosas); la monitorización de recursos y servicios que nos permitan
predecir futuros problemas, o nos alerten de la caída de uno de los
servicios brindados, etc. Veremos también una de las tecnología más
utilizadas en la actualidad, la virtualización, la que nos permitirá
entre otras cosas aprovechar al máximo los recursos de los servidores, y
agilizar las tareas de despliegue (deploys) de nuevas aplicaciones o
sistemas.

Si bien todas las distribuciones GNU/Linux modernas soportan las
tecnologías que desarrollaremos, por una cuestión de practicidad nos
enfocaremos en la distribución Debian 8.4 (Jessie). Independientemente
de esto, contar con los conceptos claros ayuda y mucho a la hora de
trabajar con una distribución u otra, dado que por lo general son muy
pocos los cambios entre estas. Es nuestro objetivo hacer foco en los conceptos,
y dotarlos de herramientas que les simplifiquen y agilicen el trabajo a la
hora de administrar sistemas GNU/Linux.

Bienvenidos…

Unidad 1: Sistemas de almacenamiento de datos (RAID y LVM)
==========================================================

Tanto el sistema operativo como los datos generados o servidos por el
mismo, deben alojarse en uno o más dispositivos de almacenamiento. Los
conocemos como volúmenes y se representan mediante dispositivos de
bloque, que se encuentran dentro del directorio /dev (por ejemplo
/dev/sda ó /dev/hda). Todos los dispositivos, independientemente de su
tecnología, con el tiempo fallan, ocasionando una falta de
disponibilidad de los datos y servicios alojados en los mismos.

Lamentablemente es muy difícil predecir cuando estos fallarán, e incluso
si no fallaran, con el tiempo los recursos ofrecidos por estos
dispositivos pueden ser insuficientes para las demandas de los sistemas
(poco espacio disponible, baja performance de lectura o escritura,
etc.).

Las tecnologías que abordaremos en esta unidad vienen a dar soluciones a
estos problemas, permitiéndonos una mayor flexibilidad para acompañar el
crecimiento y la vida de los servicios alojados en nuestros servidores,
como así también la posibilidad de afrontar contingencias (como la falla
espontánea de un disco rígido) sin pérdida de servicio. Las tecnologías
en cuestión son RAID y LVM.

Tanto RAID como LVM son técnicas para abstraer los volúmenes montados de
sus correspondientes dispositivos físicos (discos duros reales o
particiones de los mismos). El primero protege los datos contra fallos
de hardware agregando redundancia mientras que el segundo hace más
flexible la gestión de los volúmenes y los independiza del tamaño real
de los discos subyacentes. En ambos casos se crean nuevos dispositivos
de bloques en el sistema que pueden ser utilizados, tanto para crear
sistemas de archivos, como espacios de intercambio, sin necesidad de que
se asocien a un disco físico concreto. RAID y LVM tienen orígenes
bastante diferentes pero su funcionalidad a veces se solapa, por lo que
a menudo se mencionan juntos.

RAID
----

El acrónimo RAID (Redundant Array of Independent Disks), traducido como
«conjunto redundante de discos Independientes» es un sistema que se
utiliza para agrupar dos o más volúmenes físicos y considerarlos como un
único volumen lógico con ciertos beneficios. Inicialmente fue pensado
para evitar pérdida de datos en caso de fallas de uno o más discos
duro, pero como veremos, también puede aportar otros beneficios. El
principio general es bastante simple: se almacenan los datos en varios
discos físicos en lugar de sólo uno, con un nivel de redundancia
configurable. Dependiendo de esta cantidad de redundancia, y aún en caso
de fallo inesperado del disco, se puede reconstruir los datos sin
pérdida desde los discos restantes, de forma automática.

Implementaciones de RAID
~~~~~~~~~~~~~~~~~~~~~~~~

Se puede implementar RAID tanto con hardware dedicado (módulos RAID
integrados en las tarjetas controladoras de discos) o por software (un
módulo del núcleo). Ya sea por hardware o software, un sistema RAID con
suficiente redundancia puede mantenerse operativo de forma transparente
cuando falle un disco; las capas superiores (las aplicaciones) inclusive
pueden seguir accediendo a los datos a pesar del fallo.

Cuando se implementa RAID con hardware, generalmente se configura desde
la herramienta de gestión del BIOS y el núcleo tratará el RAID como un
solo disco que funcionará como un disco físico estándar, aunque el
nombre del dispositivo podría ser diferente.

En el caso de implementaciones de RAID por software, es el núcleo(kernel) el
encargado de realizar la abstracción necesaria y administrar el RAID.
Esta implementación permite una mayor flexibilidad, dado que permite
conformar incluso RAIDs entre particiones de un mismo disco (una misma
controladora). Más adelante veremos como realizar esta tarea en
GNU/Linux, mediante el uso de la herramienta mdadm.

Niveles de RAID
~~~~~~~~~~~~~~~

Las diferentes configuraciones de RAID se denominan niveles. Y cada una
de ellas ofrecen diferentes beneficios, tanto en lo referente a
redundancia, como en el acceso a disco, mejorando la performance de
lectura ó escritura por ejemplo. Existen numerosos niveles de RAID, por
una cuestión de practicidad, solo haremos énfasis en los más utilizados.

RAID 0
^^^^^^

En este tipo de RAID los datos se dividen en tiras y se guardan
equitativamente entre los discos que conforman el RAID. Es decir, si
tenemos un RAID 0 de 2 discos, a la hora de guardar un archivo, este se
va a dividir en un número par de tiras, guardando las tiras pares en un
disco, y las impares en el otro.

.. figure:: imagenes/unidad01/image_1.png
   :alt: RAID0
   :align: center

   Fig.1 - RAID 0

Se utiliza principalmente para proporcionar un mejor rendimiento de
lectura (NOTE: Siempre y cuando la lectura sea secuencial.) y escritura
ya que los datos se recuperan de dos o más discos en simultáneo (se
realiza en paralelo la lectura y escritura, por lo que cada disco solo
debe realizar la mitad del trabajo). Como desventaja, este tipo de RAID
**no ofrece redundancia**, dado que cada tira se almacena en un solo
disco, por lo que **ante la falla de uno de los discos del raid, todos los
datos se pierden** (solo tendríamos la mitad de cada archivo). RAID
0 también puede utilizarse como forma de crear un pequeño número de
grandes discos virtuales a partir de un gran número de pequeños discos
físicos. Una buena implementación de un RAID 0 dividirá las operaciones
de lectura y escritura en bloques de igual tamaño, por lo que
distribuirá la información equitativamente entre los dos discos.

RAID 1
^^^^^^

Este nivel, también conocido como "espejado RAID" (mirroring) es la
configuración más simple y una de las más utilizadas. En su forma
estándar, utiliza dos discos físicos del mismo tamaño y provee un
volumen lógico nuevamente del mismo tamaño. Los datos se almacenan en
ambos discos de forma idéntica (se duplican), de ahí el apodo de
espejado.

.. figure:: imagenes/unidad01/image_2.png
   :alt: RAID1
   :align: center

   Fig.2 - RAID1

Cuando falla un disco, los datos continúan disponibles en el otro.
Durante este evento el administrador del sistema puede ser notificado
del incidente, e intervenir para reemplazar el disco que ha fallado.
Durante esta contingencia, los datos seguirán estando disponibles. Una
vez reemplazado el disco que ha fallado, el RAID nuevamente se puede
rearmar para brindar redundancia. Incluso en configuraciones de discos
hot-plug (de reemplazo en caliente), se pueden reemplazar los discos
defectuosos sin necesidad de reiniciar el sistema, simplemente se quitan
y reemplazan por discos nuevos.

La principal desventaja de este nivel de raid es su costo, dado que se
incrementa por 2. Imaginemos que tenemos un servidor con 8 discos de
1TB, si implementáramos RAID 1, estaríamos desperdiciando la mitad de
los discos en favor de la redundancia (solo tendríamos una capacidad de
almacenamiento de 4TB en vez de 8TB). Como veremos más adelante, existen
otros niveles que ofrecen una mejor relación costo/beneficio, sin dejar
de lado la redundancia.

RAID 5
^^^^^^

Este tipo de RAID utiliza un método de detección de errores conocido
como *Paridad*. Existen diferentes forma de calcular la paridad, pero la
idea siempre es que si en un conjunto de datos (un RAID en este caso),
falta uno de ellos (supongamos un bloque alojado en uno de los discos),
este se puede reconstruir a partir de los datos restantes del conjunto y
la paridad. Para entender la paridad podemos asemejarla a una ecuación
algebraica sencilla en la que la Paridad es el cálculo de la suma de
todos los datos (en la práctica será un checksum de datos). Si se
produce un error en un disco ese dato pasa a ser una incógnita que se
puede calcular despejándola de la ecuación. Por ejemplo:

.. code:: bash

  A + B + C = PARIDAD

Si A = 1, B = 2 y C = 3 entonces

.. code:: bash

  1 + 2 + 3 = 6

Supongamos ahora que el dato B se ha perdido, entonces, contando con el
dato A, el dato C y la PARIDAD, podemos obtener el valor de B de la
ecuación:

.. code:: bash

  1 + B + 3 = 6,

que despejando nos da

.. code:: bash

  B = 6 - 1 - 3

es decir

.. code:: bash

 B = 2

Una vez entendido esto, podemos decir que un RAID 5 es una división de
datos a nivel de bloques que distribuye la información de paridad entre
todos los discos miembros del conjunto. Es decir, los datos en primer
instancia se dividen en N - 1 bloques, donde N es la cantidad de discos
que conforman el RAID, sobre este conjunto se calcula la paridad, y esta
información (los bloques y la paridad) se distribuye entre los
diferentes discos. Por ejemplo supongamos que tenemos 4 discos, en este
caso el dato se divide en 3 bloques, se calcula la paridad, y cada una
de las partes (los 3 bloques de datos y la paridad) se envían a discos
diferentes.

.. figure:: imagenes/unidad01/image_3.png
   :alt: RAID5
   :align: center

   Fig.3 - RAID5

De este modo y aplicando la técnica antes mencionada, si uno de los
discos falla, con los discos restantes se puede reconstruir el valor
faltante por lo que el sistema sigue funcionando sin problemas.

El RAID 5 ha logrado popularidad gracias a su bajo coste de redundancia.
Como desventaja, necesita un mínimo de 3 discos para ser implementado.
Este tipo de RAID además de la redundancia, aporta una mejor performance
de lectura. Por ejemplo, si observamos la figura anterior, una petición
de lectura del bloque "A1" sería servida por el disco 0. Una petición de
lectura simultánea del bloque "B1" tendría que esperar, pero una
petición de lectura de "B2" podría atenderse concurrentemente ya que
sería servida por el disco 1. Como desventaja, cada vez que un bloque de
datos se escribe ó modifica en un RAID 5, se genera un bloque de paridad
dentro de la misma división (tira). Por este motivo, generalmente el
RAID 5 se implementa con soporte hardware para el cálculo de la paridad.
Las escrituras en un RAID 5 son costosas en términos de operaciones de
disco y tráfico entre los discos y la controladora. Los bloques de
paridad no se leen en las operaciones de lectura de datos, ya que esto
sería una sobrecarga innecesaria y disminuiría el rendimiento. Sin
embargo, los bloques de paridad se leen cuando la lectura de un sector
de datos provoca un error de CRC. En este caso, el sector en la misma
posición relativa dentro de cada uno de los bloques de datos restantes
en la división y dentro del bloque de paridad en la división se utilizan
para reconstruir el sector erróneo. El error CRC se oculta así al resto
del sistema. De la misma forma, si falla un disco del conjunto, los
bloques de paridad de los restantes discos son combinados
matemáticamente con los bloques de datos de los restantes discos para
reconstruir los datos del disco que ha fallado "al vuelo". El fallo de
un segundo disco provoca la pérdida completa de los datos.

Modo degradado
~~~~~~~~~~~~~~

Se denomina modo degradado al estado del RAID cuando ha fallado al menos
uno de los discos, y por consiguiente el ARRAY realiza la reconstrucción
de la información a partir de los discos restantes que conforman el
arreglo. Por supuesto, este «modo degradado» puede tener un impacto en
el rendimiento y se reduce la redundancia, por lo que otro fallo de
disco puede llevar a la pérdida de datos. En la práctica uno intentará
estar en este modo sólo el tiempo que tome reemplazar el disco
defectuoso. Una vez que se instala el nuevo disco, el sistema RAID puede
reconstruir los datos necesarios para volver a un modo seguro. Las
aplicaciones no notan cambio alguno, además de la posible disminución en
la velocidad de acceso, mientras que el array esté en modo degradado o
durante la fase de reconstrucción.

Discos de reserva (spare)
~~~~~~~~~~~~~~~~~~~~~~~~~

Todas las implementaciones pueden soportar el uso de uno o más discos de
reserva, unidades preinstaladas que pueden usarse inmediatamente, y casi
siempre automáticamente, tras el fallo de un disco del RAID. Esto reduce
notablemente el tiempo en el que se está en modo degradado, dado que
inmediatamente se comienza la reconstrucción del RAID, y por lo tanto se
vuelve a contar con redundancia.

Ejemplo práctico: Creando RAID por software con mdadm
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si bien la mayoría de las distribuciones permiten crear RAIDs durante el
proceso de instalación de una manera muy sencilla e intuitiva, veremos
como realizar esta tarea una vez que contamos con un sistema operativo
instalado y funcionando. La herramienta principal para crear y
administrar RAIDs en GNU/Linux es mdadm (Multiple Device Administrator).
Su uso es muy sencillo como veremos a continuación con un ejemplo
práctico.

Partiremos de un sistema funcionando con un disco rígido de 20Gbytes y 2
particiones, la primera utilizada para montar el sistema de archivos
raíz y la segunda utilizada para el área de intercambio. Partiendo de
esta base, anexaremos un nuevo disco rígido de 20Gbytes y armaremos un
RAID1, espejando el disco completo (ambas particiones).

NOTA: Para poder replicar esta situación, ponemos a disposición la siguiente
`maquina virtual <https://drive.google.com/file/d/0B_9EyRmjbURpM2l4cmRyYWlCOUU/view?usp=sharing>`__,
la que pueden utilizar para realizar las pruebas sin tener que instalar
un sistema completo. El usuario de acceso es root, y su password es root también.
En primer lugar debemos instalar mdadm y rsync (lo utilizaremos luego)

.. code:: bash

  root@servidor:~# apt-get update
  root@servidor:~# apt-get install mdadm rsync

Durante la instalación se nos consultará si queremos inicializar los
arrays, les ponemos la opción por defecto (all).

Cargamos algunos módulos necesarios del kernel

.. code:: bash

  modprobe linear
  modprobe raid0
  modprobe raid1
  modprobe raid5
  modprobe raid6
  modprobe raid10

En segundo lugar debemos estar seguro que disco es el que tiene el
sistema raíz, y que disco es el nuevo que se ha anexado. La manera
sencilla de ver esto es por medio del comando df -h /

.. code:: bash

  root@servidor:~# df -h /
  S.ficheros Tamaño Usados Disp Uso% Montado en
  /dev/sda1    18G   967M  16G   6%       /

Como vemos, el dispositivo /dev/sda contiene el dispositivo actualmente
en uso. Si hacemos un

.. code:: bash

  root@servidor:~# ls -l /dev/sd*
  brw-rw---- 1 root disk 8, 0  nov 6 18:20 /dev/sda
  brw-rw---- 1 root disk 8, 1  nov 6 18:20 /dev/sda1
  brw-rw---- 1 root disk 8, 2  nov 6 18:20 /dev/sda2
  brw-rw---- 1 root disk 8, 16 nov 6 18:20 /dev/sdb


Veremos que el segundo disco fue representado como /dev/sdb, por lo que
/dev/sda es el disco que actualmente contiene la información (con las
particiones que queremos espejar) y sdb será el dispositivo sobre el que
realizaremos, replicaremos y mantendremos espejado el disco sda. En
primer lugar debemos replicar la tabla de particiones de manera exacta
entre ambos discos, previamente borrando cualquier información de
particiones que exista en el disco /dev/sdb

.. code:: bash

  root@servidor:~# wipefs -a /dev/sdb
  root@servidor:~# sfdisk -d /dev/sda | sfdisk --force /dev/sdb

Ahora debemos cambiar el tipo de partición de las dos particiones del
disco /dev/sdb a "Linux raid autodetect". Esto se hace mediante el
comando fdisk

.. code:: bash

  root@servidor:~# fdisk /dev/sdb
  Bienvenido a fdisk (util-linux 2.25.2).
  Los cambios solo permanecerán en la memoria, hasta que decida escribirlos.
  Tenga cuidado antes de utilizar la orden de escritura.

  Orden (m para obtener ayuda): t
  Número de partición (1,2, valor predeterminado 2): 1
  Código hexadecimal (escriba L para ver todos los códigos): fd
  Se ha cambiado el tipo de la partición 'Linux' a 'Linux raid autodetect'.
  Orden (m para obtener ayuda): t Número de partición (1,2, valor predeterminado 2):2
  Código hexadecimal (escriba L para ver todos los códigos): fd
  Se ha cambiado el tipo de la partición 'Linux swap / Solaris' a 'Linux raid autodetect'.

  Orden (m para obtener ayuda): wq
  Se ha modificado la tabla de particiones. Llamando a ioctl() para volver a leer la tabla de particiones.
  Se están sincronizando los discos.

Debemos asegurarnos de que el disco no contenga información de
anteriores raid, si es un disco nuevo se puede omitir, pero nunca está
demás:

.. code:: bash

  root@servidor:~# mdadm --zero-superblock /dev/sdb1

  mdadm: Unrecognised md component device - /dev/sdb1
  root@servidor:~# mdadm --zero-superblock /dev/sdb2

  mdadm: Unrecognised md component device - /dev/sdb2

El mensaje "mdadm: Unrecognised md component device - /dev/sdb2" es
normal, y simplemente nos quiere decir que no se encontró información de
configuraciones de raid anteriores.

Ahora creamos el raid, le especificamos el nivel, le decimos que
particiones formarán parte, y le agregamos una opción llamada missing,
que quiere básicamente decir que uno de los dos discos por ahora falta
(el disco sda, que actualmente está en uso y por ende no lo podemos
utilizar).

.. code:: bash

  root@servidor:~# mdadm --create /dev/md0 --level=1 --raid-disks=2 missing /dev/sdb1

  mdadm: Note: this array has metadata at the start and may not be suitable as a boot device.
  If you plan to store '/boot' on this device please ensure that your boot-loader understands md/v1.x metadata,
  or use --metadata=0.90 Continue creating array? y
  mdadm: Defaulting to version 1.2 metadata mdadm: array /dev/md0 started.

  root@servidor:~# mdadm --create /dev/md1 --level=1 --raid-disks=2 missing /dev/sdb2
  mdadm: Note: this array has metadata at the start and may not be suitable as a boot device.
  If you plan to store '/boot' on this device please ensure that your boot-loader understands md/v1.x metadata,
  or use --metadata=0.90 Continue creating array? y
  mdadm: Defaulting to version 1.2 metadata mdadm: array /dev/md1 started.

El raid será identificado con los dispositivos de bloque /dev/md0 (que
hará referencia al espejo entre las particiones /dev/sda1 y /dev/sdb1) y
/dev/md1 (que será el espejo de las particiones sda2 y sdb2). A partir
de ahora no haremos más referencia a sda1 o sda2, sino que en su lugar
utilizaremos los dispositivos de bloque /dev/md0 y /dev/md1.

Una forma interesante de ver el estado del raid, es por medio del
archivo /proc/mdstat, el que almacena información del estado del raid

.. code:: bash

  root@servidor:~# cat /proc/mdstat

  Personalities : [raid1]
  md1 : active raid1 sdb2[1] 1437696 blocks super 1.2 [2/1] [_U]
  md0 : active raid1 sdb1[1] 19514368 blocks super 1.2 [2/1] [_U]

Si observamos con detenimiento, veremos que nos informa los raid en uso
(en este caso 2), el tipo de estos raid (raid1), y los dispositivos
asociados a los mismos, que por ahora solo son sdb1 y sdb2. Otro dato
interesante es lo que se observa al final donde dice "[ _U ]" esto
quiere decir que de los dos dispositivos que deberían estar asociados al
raid, solo uno de ellos forma parte del mismo (esto es debido a que
todavía no le hemos dicho que sda1 y sda2 deben formar parte).

El siguiente paso consiste en formatear los dispositivos con el mismo
sistema de archivos utilizados en las particiones sda.

.. code:: bash

  root@servidor:~# mkfs.ext4 /dev/md0

  mke2fs 1.42.12 (29-Aug-2014) Se está creando El sistema de ficheros con 4634368 4k bloques y 1158720 nodos-i

  UUID del sistema de ficheros: 05dc3ad0-61f5-4a89-a706-ce90aa704d8b
  Respaldo del superbloque guardado en los bloques: 32768, 98304, 163840
  Reservando las tablas de grupo: hecho
  Escribiendo las tablas de nodos-i: hecho
  Creando el fichero de transacciones (32768 bloques): hecho
  Escribiendo superbloques y la información contable del sistema de ficheros: hecho

  root@servidor:~# mkswap /dev/md1
  Configurando espacio de intercambio versión 1, tamaño = 2413564 kiB sin etiqueta,
  UUID=fea69bae-74b9-4980-b39c-da7bce339341

Ahora debemos actualizar la información del archivo
/etc/mdadm/mdadm.conf con la situación actual

.. code:: bash

  root@servidor:~# mdadm --examine --scan >> /etc/mdadm/mdadm.conf

Modificamos el archivo /etc/fstab, para que en el próximo inicio utilice
el dispositivo /dev/md0 para montar el /, y /dev/md1 para la swap. Para
esto comentamos las entradas anteriores y agregamos lo siguiente:

.. code:: bash

    # /etc/fstab: static file system information.
    #
    # Use 'blkid' to print the universally unique identifier for a
    # device; this may be used with UUID= as a more robust way to name devices
    # that works even if disks are added and removed. See fstab(5).
    #
    # <file system> <mount point>   <type>  <options>       <dump>  <pass>
    # / was on /dev/sda1 during installation
    #UUID=12497bdc-78c9-4627-acb5-e2c4eaa6e658 /     ext4    errors=remount-ro 0       1
    /dev/md0 /               ext4    errors=remount-ro 0       1

    # swap was on /dev/sda2 during installation
    #UUID=31425623-0f05-4cb7-804c-5758885f556a none  swap    sw              0       0
    /dev/md1 none            swap    sw              0       0

    /dev/sr0        /media/cdrom0   udf,iso9660 user,noauto     0       0

Debemos obtener la versión del kernel actualmente en uso

.. code:: bash

  root@servidor:~# uname -a

  Linux servidor 3.16.0-4-586 #1 Debian 3.16.36-1+deb8u2 (2016-10-19) i686 GNU/Linux


Con esta versión debemos crear un archivo llamado /etc/grub.d/09_swraid1_setup y poner lo siguiente

.. code:: bash

    #!/bin/sh -e
    cat << EOF

    menuentry 'Debian GNU/Linux, con Linux 3.16.0-4-586 y RAID' --class debian --class gnu-linux --class gnu --class os {
    insmod part_msdos
    insmod ext2
    insmod mdraid1x
    set root='(md/0)'
    linux /boot/vmlinuz-3.16.0-4-586 root=/dev/md0 ro   quiet
    initrd /boot/initrd.img-3.16.0-4-586
    }
    EOF

Como verán, a grub le decimos que el root debe montarlo desde /dev/md0.

NOTA: Es importante asegurarse que estas versiones corresponden al
kernel actualmente instalado, para lo que es recomendable asegurarse que
los archivos /boot/vmlinuz-3.16.0-4-586 y /boot/initrd.img-3.16.0-4-586
existan.

Luego debemos darle permisos de ejecución a dicho script

.. code:: bash

  root@servidor:~# chmod a+x /etc/grub.d/09_swraid1_setup

Editamos el archivo /etc/default/grub y descomentamos las siguientes
líneas

.. code:: bash

  GRUB_DISABLE_LINUX_UUID=true
  GRUB_TERMINAL=console

A continuación creamos el archivo /etc/initramfs-tools/conf.d/mdadm y
ponemos lo siguiente para asegurarnos de que grub inicie incluso si el
RAID esta degradado

.. code:: bash

  BOOT_DEGRADED=true

Luego ejecutamos update-grub2, para que esta configuración sea tenida en
cuenta por grub

.. code:: bash

    root@servidor:~# update-grub2
    Generating grub configuration file ...
    Encontrada imagen de linux: /boot/vmlinuz-3.16.0-4-586
    Encontrada imagen de memoria inicial: /boot/initrd.img-3.16.0-4-586
    hecho

Actualizamos el ramdisk a la nueva situación

.. code:: bash

    root@servidor:~# update-initramfs -u
    update-initramfs: Generating /boot/initrd.img-3.16.0-4-586

Instalamos grub en los dos discos (para asegurarnos de que esté
correctamente instalado y actualizado en el sector de inicio de los
mismos).

Para esto ejecutamos

.. code:: bash

    root@servidor:~# dpkg-reconfigure grub-pc

Le damos aceptar a las dos primeras preguntas sobre los argumentos del
kernel y al finalizar seleccionamos los discos /dev/sda y /dev/sdb para
que se instale el grub.

Ahora debemos replicar la información que está en la partición /dev/sda1
en el dispositivo de bloques /dev/md0, de modo que podamos iniciar el
sistema desde esta partición (el raid), antes de anexar el disco
/dev/sda1 al array md0.

.. code:: bash

    root@servidor:~# mkdir /mnt/md0
    root@servidor:~# mount /dev/md0 /mnt/md0
    root@servidor:~# rsync -auHx --exclude=/proc/* --exclude=/sys/* /* /mnt/md0
    root@servidor:~# umount /mnt/md0

A continuación es tiempo de reiniciar y asegurarnos de que el sistema
levante con el nuevo kernel y su configuración de raid

Una vez iniciado el sistema, debemos anexar el disco sda al raid. Para
esto procedemos de la siguiente manera:

Modificamos el tipo de partición tanto de sda1 como de sda2, tal como
hicimos con sdb1 y sdb2

.. code:: bash

    root@servidor:~# fdisk /dev/sda

    Bienvenido a fdisk (util-linux 2.25.2).
    Los cambios solo permanecerán en la memoria, hasta que decida escribirlos.
    Tenga cuidado antes de utilizar la orden de escritura.


    Orden (m para obtener ayuda): t
    Número de partición (1,2, valor predeterminado 2): 1
    Código hexadecimal (escriba L para ver todos los códigos): fd

    Se ha cambiado el tipo de la partición 'Linux' a 'Linux raid autodetect'.

    Orden (m para obtener ayuda): t
    Número de partición (1,2, valor predeterminado 2): 2
    Código hexadecimal (escriba L para ver todos los códigos): fd

    Se ha cambiado el tipo de la partición 'Linux swap / Solaris' a 'Linux raid autodetect'.

    Orden (m para obtener ayuda): w
    Se ha modificado la tabla de particiones.
    Llamando a ioctl() para volver a leer la tabla de particiones.
    Se están sincronizando los discos.

Luego debemos anexar dichas particiones al raid

.. code:: bash

    root@servidor:~# mdadm --add /dev/md0 /dev/sda1
    mdadm: added /dev/sda1
    root@servidor:~# 	mdadm --add /dev/md1 /dev/sda2
    mdadm: added /dev/sda2

Ahora si miramos el estado del raid veremos que se están empezando a
sincronizar los discos

.. code:: bash

    root@servidor:~# cat /proc/mdstat
    Personalities : [raid1]
    md1 : active raid1 sda2[2] sdb2[1]
          1437696 blocks super 1.2 [2/1] [_U]
          	resync=DELAYED

    md0 : active raid1 sda1[2] sdb1[1]
          19514368 blocks super 1.2 [2/1] [_U]
          [====>................]  recovery = 22.0% (4308864/19514368) finish=2.8min speed=88426K/sec

    unused devices: <none>

Esta operación puede demorar según la cantidad de información que
contengan las particiones. Una vez que la sincronización a terminado,
deberíamos ver algo como lo siguiente

.. code:: bash

    root@servidor:~# cat /proc/mdstat
    Personalities : [raid1]
    md1 : active raid1 sda2[2] sdb2[1]
          1437696 blocks super 1.2 [2/2] [UU]

    md0 : active raid1 sda1[2] sdb1[1]
          19514368 blocks super 1.2 [2/2] [UU]

    unused devices: <none>

Si observan ahora, los dos raid está conformados y sincronizados. Esto
lo podemos ver al observar donde dice **[2/2] [UU]** que básicamente
nos está diciendo que 2 dos dispositivos que tiene el raid, los dos
dispositivos están activos, y actualizados.

Ahora debemos actualizar nuevamente la información del archivo
/etc/mdadm/mdadm.conf con la situación final

.. code:: bash

    root@servidor:~# mdadm --examine --scan >> /etc/mdadm/mdadm.conf


Actualizamos nuevamente grub

.. code:: bash

    root@servidor:~# grub-mkdevicemap -n
    root@servidor:~# update-grub
    root@servidor:~# grub-install /dev/sda ; grub-install /dev/sdb

Con esto concluimos la configuración del RAID 1. Podemos hacer pruebas
desconectando uno de los discos y asegurándonos de que el sistema aún
continúa iniciando.

LVM (Gestor de volúmenes lógicos)
---------------------------------

LVM es un gestor de volúmenes lógicos, de allí su nombre (Logical Volume
Manager). Es otra forma de abstraer volúmenes lógicos de su soporte
físico, que a diferencia de RAID está enfocado en ofrecer mayor
flexibilidad en lugar de aumentar confiabilidad, de hecho LVM no nos
aporta redundancia y confiabilidad. Es común encontrar implementaciones
que combinan LVM y RAID, de modo de aprovechar todos los beneficios
aportados por ambos sistemas. Por un lado podemos brindar redundancia
por medio de un sistema RAID, y a su vez administrar este RAID como un
único volumen que puede ser dividido tantas veces sea necesario, por
medio de la flexibilidad que aporta LVM. LVM permite modificar un
volumen lógico de forma transparente a las aplicaciones; por ejemplo, es
posible agregar nuevos discos, migrar sus datos y eliminar discos
antiguos sin desmontar el volumen, por ejemplo en caso de que nos
estemos quedando sin espacio en un servidor, y no podamos detenerlo,
gracias a LVM podemos conectar un nuevo disco y anexarlo para que nos
permita incrementar la capacidad y resolver el problema.

Conceptos
~~~~~~~~~

La flexibilidad aportada por LVM se consigue con un nivel de abstracción
que incluye tres conceptos que veremos a continuación, pero que se
encuentran representados en el siguiente diagrama.

.. figure:: imagenes/unidad01/image_4.jpg
   :alt: Conceptos de LVM
   :align: center
   :scale: 80%

   Fig.4 - Conceptos de LVM


Volumen Físico (Physical Volume, PV)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Es la entidad más cercana al hardware: pueden ser particiones en un
disco, un disco completo o inclusive cualquier dispositivo de bloque
(también un RAID, por ejemplo).

Grupo de volúmenes (Volume Group, VG)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Los volúmenes físicos se agrupan en los denominados Grupos de volúmenes
(VG), lo que puede compararse con discos virtuales y extensibles. Los
VGs son abstractos y no aparecerán como un dispositivo de bloques en
/dev, simplemente son una abstracción para agrupar los distintos
dispositivos, y considerar a todos estos como un único disco virtual, un
gran disco que suma las capacidades aportadas por los dispositivos que
lo componen.

Volúmenes lógicos (Logical Volume, LV)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

El tercer tipo de objeto es el volumen lógico(LV) que es una porción de
un grupo de volúmenes(VG); si continuamos con la analogía de un grupo de
volúmenes como un disco abstracto, un volúmen lógico se compara a una
partición. El LV será un dispositivo de bloque que tendrá un elemento en
/dev y puede ser utilizado como cualquier partición física (usualmente,
almacenar un sistema de archivos o espacio de intercambio). Lo
importante es que la división de un VG en varios LVs es completamente
independiente de sus componentes físicos (los PVs). Se puede dividir un
VG con un solo componente físico (un disco por ejemplo) en una docena de
volúmenes lógicos; del mismo modo, un VG puede utilizar varios discos
físicos y aparecer como sólo un volúmen lógico grande. La única
limitación es que, obviamente, el tamaño total asignado a un LV no puede
ser mayor que la capacidad total de los PVs en el grupo de volúmenes.
Generalmente tiene sentido, sin embargo, mantener el mismo tipo de
homogeneidad entre los componentes físicos de un VG y dividir el VG en
volúmenes lógicos que tendrán patrones de uso similares. Por ejemplo, si
el hardware disponible incluye discos rápidos y discos lentos, podría
agrupar los discos rápidos en un VG y los lentos en otro; puede asignar
pedazos del primero a aplicaciones que necesiten acceso rápido a los
datos y mantener el segundo para tareas menos exigentes.

Ejemplo práctico: Uso de LVM en servidor de archivos y DB
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para entender un poco más estos conceptos, veremos un ejemplo sencillo.
Supongamos que tenemos un servidor que se utiliza como servidor de bases
de datos, y a la vez como servidor de archivos. Este equipo cuenta con 4
discos de tecnologías diferentes, y rendimientos muy distintos. Los
discos son de tecnologías Sata (Serial Ata) y SAS (Serial Attached
SCSI). La tecnología Sata es la menos performante, y dentro de la SSD la
más performante de ellas. Las capacidades son las siguientes:

-  /dev/sda: Disco Sata de 20 Gbytes y 7,2k RPM

-  /dev/sdb: Disco SAS de 500 Gbytes y 10k RPM

-  /dev/sdc: Disco SAS de 500 Gbytes y 10k RPM

-  /dev/sdd: Disco SAS de 300 Gbytes y 15k RPM

Para simplificar el ejemplo, dejaremos de lado la redundancia y solo nos
centraremos en la flexibilidad y la escalabilidad, este último un factor
más que importante, sobre todo a la hora de dimensionar el crecimiento
que puede tener el servidor de archivos.

La idea es contar con 2 grupos de volúmenes (VGs), uno compuesto por los
discos /dev/sdb y /dev/sdc, y que será utilizado para alojar el servidor
de archivos. El otro VG estará conformado por el disco /dev/sdd
únicamente, y será utilizado para alojar allí las bases de datos, dado
que es un dispositivo muy performante. El disco /dev/sda, por
simplicidad, será utilizado para montar el sistema raíz.

Luego crearemos 3 volúmenes lógicos, uno que utilizaremos para los homes
de los usuarios( /home ), uno para montar el servidor de archivos
(/srv/fileserver ) y el restante para montar las bases de datos.

La configuración se resumen en el siguiente diagrama

.. figure:: imagenes/unidad01/image_5.png
   :alt: Configuración de ejemplo
   :align: center
   :scale: 80%

   Fig.5 - Configuración de ejemplo


Empezamos por instalar lvm

.. code:: bash

    root@servidor:~# cat /proc/mdstat
    Personalities : [raid1]
    md1 : active raid1 sda2[2] sdb2[1] 1437696 blocks super 1.2 [2/2] [UU]
    md0 : active raid1 sda1[2] sdb1[1] 19514368 blocks super 1.2 [2/2] [UU]

    unused devices:
    root@servidor:~# apt-get install lvm2

Luego debemos definir los volúmenes físicos

.. code:: bash

    root@servidor:~# pvcreate /dev/sda
    Physical volume "/dev/sda" successfully created
    root@servidor:~# pvcreate /dev/sdb
    Physical volume "/dev/sdb" successfully created
    root@servidor:~# pvcreate /dev/sdc
    Physical volume "/dev/sdc" successfully created

En este caso hemos definido los volúmenes utilizando los discos
completos, pero bien podríamos haber utilizado particiones.

Podemos ver los volúmenes físicos que hemos creado, utilizando el
comando pvdisplay

.. code:: bash

    root@servidor:~# pvdisplay -C
      PV         VG   Fmt  Attr PSize   PFree
      /dev/sda        lvm2 ---  500,00g 500,00g
      /dev/sdb        lvm2 ---  500,00g 500,00g
      /dev/sdc        lvm2 ---  300,00g 300,00g

Ahora es momento de agrupar estos volúmenes físicos en los grupos de
volúmenes que hemos definido (VG_Default y VG Performante). Para esto
debemos utilizar el comando vgcreate, especificando el nombre que le
queremos dar al grupo y los PV que lo conforman

.. code:: bash

    root@servidor:~# vgcreate VG_Default /dev/sda /dev/sdb
    root@servidor:~# vgcreate VG Performante /dev/sdc

Luego con el comando vgdisplay podemos ver como han sido definidos

.. code::bash

    root@servidor:~# vgdisplay -C
      VG             #PV #LV #SN Attr   VSize   VFree
      VG_Default       2   0   0 wz--n- 999,99g 999,99g
      VG Performante   1   0   0 wz--n- 300,00g 300,00g

Como datos relevantes, vemos la cantidad de PVs asignados al VG, y cual
es el tamaño máximo del mismo.

Incluso si hacemos nuevamente un pvdisplay, veremos información relevante

.. code::bash

   root@servidor:~# pvdisplay -C
     PV         VG             Fmt  Attr PSize   PFree
     /dev/sda   VG_Default     lvm2 a--  500,00g 500,00g
     /dev/sdb   VG_Default     lvm2 a--  500,00g 500,00g
     /dev/sdc   VG_Performante lvm2 a--  300,00g 300,00g


Solo resta definir los volúmenes lógicos,y como es de esperar, hay un
comando lvcreate, el que se usa de la siguiente manera lvcreate -n -L

Creemos el LV que se utilizara para los homes, le daremos inicialmente
100G

.. code:: bash

    root@servidor:~# lvcreate -n lv_home --size 100G VG_Default

Si ahora hacemos un lvdisplay, veremos detalles sobre el volumen creado

.. code:: bash

    root@servidor:~# lvdisplay
      --- Logical volume ---
      LV Path                /dev/VG_Default/lv_home
      LV Name                lv_home
      VG Name                VG_Default
      LV UUID                kqJAcE-UqVY-aehl-XNyH-DJLZ-D8Nl-687yAG
      LV Write Access        read/write
      LV Creation host, time servidor, 2016-11-20 11:32:15 -0300
      LV Status              available
      # open                 0
      LV Size                100,00 GiB
      Current LE             25600
      Segments               1
      Allocation             inherit
      Read ahead sectors     auto
      - currently set to     256
      Block device           254:0

Lo más relevante son el "LV Path", que básicamente nos dice como se
llama el dispositivo de bloques que representa el LV, y que utilizaremos
para montar un filesystem o un área de intercambio. También nos informa
a que VG pertenece (VG Name), y el tamaño del mismo (LV Size).

Solo hemos definido un LV, si miramos nuevamente el estado del VG
veremos que ya no están disponibles 100G.

.. code:: bash

    root@servidor:~# vgdisplay -C VG_Default
      VG         #PV #LV #SN Attr   VSize   VFree
      VG_Default   2   1   0 wz--n- 999,99g 899,99g

Por lo tanto si queremos crear el siguiente LV, el que utilizaremos para
el servidor de archivos, debemos asignarle como tamaño máximo 899,99G

.. code:: bash

    root@servidor:~# lvcreate -n lv_fileserver --size 899,99g VG_Default
      Rounding up size to full physical extent 899,99 GiB
      Logical volume "lv_fileserver" created

Si nuevamente realizamos un vgdisplay veremos que ya no cuenta con
espacio

.. code:: bash

    root@servidor:~# vgdisplay -C
      VG             #PV #LV #SN Attr   VSize   VFree
      VG_Default       2   2   0 wz--n- 999,99g      0
      VG_Performante   1   0   0 wz--n- 300,00g 300,00g

A continuación solo resta crear el último LV que nos hemos planteado

.. code:: bash

    root@servidor:~# lvcreate -n lv_bases --size 299,99g VG_Performante
      Rounding up size to full physical extent 299,99 GiB
      Logical volume "lv_bases" created

Si estuvo atento, habrá observado que como tamaño no le hemos asignado
300GB, sino que en su lugar es de 299,99GB (lo mismo tuvimos que hacer
con lv_fileserver). Esto se debe a que no tenemos disponible los 300GB,
sino que una parte (ínfima) se utiliza para guardar la información de
LVM. Si hubiéramos tratado de crear el LV con 300G nos hubiera dado un
error, informando que el VG no tiene espacio suficiente.

Los diferentes dispositivos de bloques son :

-  /dev/VG_Default/lv_home

-  /dev/VG_Default/lv_fileserver

-  /dev/VG_Performante/lv_bases

Como mencionamos anteriormente, sobre los mismos se pueden crear
filesystems, o utilizarlos como área de intercambio.

Creemos ahora por ejemplo un filesystem sobre lv_home

.. code:: bash

    root@servidor:~# mkfs.ext4 /dev/VG_Default/lv_home

y montémoslo como tal

*NOTA: Por cuestiones de simplicidad lo montamos temporalmente, lo recomendable es definirlo en el archivo fstab para que se automonte en cada reinicio.*

.. code:: bash

    root@servidor:~# mount /dev/VG_Default/lv_home /home/

Si vemos el tamaño asignado

.. code:: bash

    root@servidor:~# df -h /home/
    S.ficheros                     Tamaño Usados  Disp Uso% Montado en
    /dev/mapper/VG_Default-lv_home    99G    60M   94G   1% /home

Ahora supongamos que ha pasado el tiempo y el sistema empezó escalar en
la cantidad de usuarios y 100G ya no son suficientes. Es en estos casos
donde LVM toma relevancia, porque simplemente podemos comprar un nuevo
disco, anexarlo al VG_Default, y luego redimensionar el LV con los
homes. Incluso no es necesario que la tecnología del disco sea similar a
las utilizadas en el VG.

Siguiendo con nuestro ejemplo, compramos un disco de 500GB, el que
anexaremos al VG_Default para redimensionar el LV.

Primero lo definimos como PV

.. code:: bash

    root@servidor:~# pvcreate /dev/sdd
      Physical volume "/dev/sdd" successfully created

Lo asignamos al VG_Default, para esto debemos utilizar el comando
vgextend, porque precisamente estamos extendiendo las capacidades del
mismo

.. code:: bash

    root@servidor:~# vgextend VG_Default /dev/sdd
      Volume group "VG_Default" successfully extended

Si hacemos un vgdisplay veremos que ahora contamos con un VG de 1,5TB, y
500GB libres

.. code:: bash

    root@servidor:~# vgdisplay -C
      VG             #PV #LV #SN Attr   VSize   VFree
      VG_Default       3   2   0 wz--n-   1,46t 500,00g
      VG_Performante   1   1   0 wz--n- 300,00g   4,00m

del mismo modo extendemos el LV, con el comando lvextend

.. code:: bash

    root@servidor:~# lvextend --size +100G /dev/VG_Default/lv_home

En este caso le decimos con le sume +100GB al LV, el que es especificado
mediante su path.

Si ahora hacemos un lvdisplay veremos que el LV es de 200G

.. code:: bash

    root@servidor:~# lvdisplay -C
      LV            VG             Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
      lv_fileserver VG_Default     -wi-a----- 899,99g
      lv_home       VG_Default     -wi-a----- 200,00g
      lv_bases      VG_Performante -wi-a----- 299,99g

sin embargo si hacemos un df -h veremos que el filesystem sigue siendo
de 100GB. Esto es debido a que nos falta redimensionar el filesystem
para que tome el nuevo tamaño disponible

.. code:: bash

    root@servidor:~# resize2fs /dev/VG_Default/lv_home

Ahora si, el filesystem cuenta con los 200GB.

.. code:: bash

    root@servidor:~# df -h /home
    S.ficheros                     Tamaño Usados  Disp Uso% Montado en
    /dev/mapper/VG_Default-lv_home   197G    60M  188G   1% /home

Con esto concluimos la Unidad 1. Las 2 tecnologías que hemos visto son
muy importantes, ya que nos permiten afrontar problemas de escalabilidad
e incluso contingencias por fallas en el hardware de los dispositivos de
almacenamiento, por lo que es indispensable su uso en sistemas en
entornos productivos

.. image:: caratulas/unidad02.pdf
	:scale: 86%

Unidad 2: Configuración y políticas de Firewall
===============================================

Políticas de seguridad
----------------------

La palabra seguridad es una palabra muy amplia que involucra conceptos,
herramientas y procedimientos. Al precipitarse a implementar un conjunto
arbitrario de herramientas se corre el riesgo de enfocarse en los
aspectos de seguridad equivocados. Es fundamental antes de tomar
cualquier disposición, definir correctamente la "Política de seguridad"
que utilizaremos, y que guiará estas medidas. Para definir nuestra
política de seguridad, podemos empezar por responder unas pocas
preguntas:

-  ¿Qué estamos tratando de proteger?: La política de seguridad será
   diferente dependiendo de lo que deseamos proteger.

-  ¿Contra qué estamos tratando de protegernos? ¿Fuga de datos
   confidenciales? ¿Pérdida accidental de datos? ¿Pérdida de ingresos
   por interrupción del servicio?

-  ¿Contra quién estamos tratando de protegernos?: Las medidas de
   seguridad serán diferentes para protegerse contra el error de un
   usuario regular del sistema de lo que serían contra un grupo de
   atacantes determinado.

Habitualmente, se utiliza el término **riesgo** para referirse al conjunto de
estos tres factores: **qué proteger, qué necesitamos prevenir antes que suceda
y quién intentará hacer que suceda**. Modelar el riesgo requiere respuestas a
estas tres preguntas. A partir de este modelo de riesgo, podemos construir la
política de seguridad e implementarla con acciones concretas.

Vale la pena tomar en cuenta restricciones adicionales, dado que pueden
limitar el alcance de las políticas disponibles. ¿Hasta dónde estamos dispuestos
a llegar para asegurar un sistema?. Esta pregunta tiene un gran impacto en la
política a implementar. La respuesta es a menudo definida en términos de costos
monetarios, pero deben considerar otros elementos, tal como la cantidad
de inconvenientes impuestos a los usuarios del sistema o una degradación
en el rendimiento. Un buen principio a tener en cuenta es que un
perímetro corto y bien definido es más fácil de defender que una
frontera larga y sinuosa. Otro principio fundamental es que nunca
podremos eliminar un riesgo, simplemente podemos llevarlo a niveles de
ocurrencia o impacto aceptables. También es importante entender que una
medida para un riesgo determinado tiene fecha de vencimiento, es decir,
periódicamente debemos asegurarnos de que la misma siga cumpliendo con
su fin. Se debe diseñar en consecuencia también la organización de la
red: se deben concentrar los servicios sensibles en un pequeño número de
máquinas y estas máquinas sólo deben ser accesibles a través de un
número mínimo de puntos de control, asegurar estos puntos de control
será más fácil que asegurar todas la máquinas sensibles contra la
totalidad del mundo exterior. Es en este punto que se hace evidente la
utilidad del filtrado de red (incluyendo los firewalls). Puede
implementar este filtrado con hardware dedicado, pero posiblemente una
solución más simple y flexible sea utilizar un software de firewall como
el que se integra en el núcleo (kernel) de Linux. En esta unidad abordaremos
este firewall y veremos algunos ejemplos de uso.

Cortafuegos (Firewalls)
-----------------------

Un firewall es una pieza de equipo de cómputo con hardware y/o software
que ordena los paquetes entrantes o salientes de la red (que vienen
hacia o desde una red local) y sólo permite el paso de aquellos que
cumplen con ciertas condiciones predefinidas. Para cumplir con su
objetivo, es necesario que el flujo de información entre las redes pase
a través de este, por lo que es muy común encontrarlos delante o como la
puerta de enlace predefinida (Default Gateway) de la red.

.. figure:: imagenes/unidad02/image_0.png
   :alt: El firewall como Gateway
   :align: center
   :scale: 50 %


   Fig. 1 - El firewall como Gateway

Netfilter: el firewall de GNU/Linux
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El núcleo Linux incorpora el firewall netfilter. Este se administra y
controla por medio de los programas iptables e ip6tables, según el tipo
de tráfico que deseemos filtrar (IPv4 o IPv6).

Empecemos desde el principio: Cuando un paquete entra en el cortafuegos
(firewall), alcanza el hardware y es procesado en el núcleo por su
driver correspondiente. Después el paquete empieza a recorrer una serie
de etapas en el núcleo antes de ser enviado a la aplicación adecuada
(localmente), reenviado hacia otro host, o cualquier otra operación.
Este conjunto de etapas por las que atraviesa un paquete determinado es
netfilter.

.. figure:: imagenes/unidad02/image_1.png
   :alt: NetFilter
   :scale: 65 %
   :align: center


   Fig. 2 - NetFilter

Entender NetFilter es entender como se relacionan 4 conceptos
fundamentales:

-  Tablas

-  Cadenas

-  Reglas

-  Acciones

Las cadenas son conjuntos de reglas con un orden determinado, que se
verifican una después de la otra siempre y cuando ninguna se cumpla, es
decir, si una regla se cumple, las siguientes no se evalúan.

Las tablas
son un conjunto de cadenas que se aplican para tomar una decisión, o
realizar una modificación del flujo de datos en la red, por ejemplo,
filtrar las conexiones entrantes o salientes, aplicar una redirección de
puertos, o marcar paquetes para la priorización de tráfico. Como regla
general, podemos decir que las tablas están compuestas por cadenas, y
las cadenas por reglas, siendo las reglas las que determinar qué se debe
hacer con el tráfico que atraviesa o llega al firewall.

Tablas
^^^^^^

iptables cuenta con cuatro tablas, que son zonas en las que una cadena
de reglas se puede aplicar:

-  **Filter**: es la tabla por defecto y se refiere a las reglas de
   filtrado (aceptar, rechazar o ignorar un paquete).

-  **Nat**: se utiliza para la traducción de las direcciones de origen o
   destino de los paquetes, y sus puertos.

-  **Mangle**: se usa en la alteración de los paquetes de red
   especializados, por ejemplo el campo ToS (type of service).

-  **Raw**: permite otras modificaciones manuales en los paquetes antes
   de que lleguen al sistema de seguimiento de conexiones.

Cadenas
^^^^^^^

Como mencionamos anteriormente, las cadenas son agrupaciones de reglas
con un orden predefinido. El firewall utiliza cadenas estándares para
manejar paquetes en función de circunstancias predefinidas. Las mismas
se encuentran asociadas a una tabla particular, siendo las más utilizadas
y las que veremos en este capítulo, la tabla Filter y la tabla NAT:

-  **Tabla Filter**
		Contiene 3 cadenas predefinidas, y dependiendo del origen y destino del
		paquete, el mismo será procesado por alguna de las siguientes:

   -  Cadena INPUT: procesará los paquetes cuyo destino es el propio firewall.

   -  Cadena OUTPUT: procesará los los paquetes que se generan en el firewall.

   -  Cadena FORWARD: procesará los los paquetes que transitan a través del
      firewall (que no es ni su origen ni su destino).

-  **Tabla Nat**
		También contiene 3 cadenas predefinidas, y se utilizan para modificar los
		paquetes:

   -  Cadena PREROUTING: para modificar los paquetes tan pronto como llegan.

   -  Cadena POSTROUTING: para modificar los paquetes cuando están listos para
      seguir su camino.

   -  Cadena OUTPUT: para modificar los paquetes generados por el propio
      firewall.


No abordaremos las tablas mangle y raw ya que su uso excede el contenido
de esta materia, de cualquier modo, los invitamos a seguir investigando
sobre las mismas en la abundante documentación que se encuentra en
internet.

Reglas
^^^^^^

Las reglas son conjuntos de condiciones y una acción determinada que se
ejecutará cuando se cumplan dichas condiciones. Cuando se procesa un
paquete, el firewall examina la cadena apropiada, una regla tras otra;
cuando se cumplen las condiciones de una, se aplica la acción
especificada para continuar el procesamiento.

Acciones
^^^^^^^^

Al cumplirse el conjunto de condiciones de una regla, se debe aplicar la
acción asociada a la misma, las que pueden ser:

-  ACCEPT: permitir al paquete seguir su camino. Es decir, si se cumple
   esta regla, el paquete sigue su camino sin que se verifique el resto
   de las reglas de esa tabla.

-  REJECT: rechazar el paquete con un paquete de error ICMP (se informa
   que el paquete ha sido rechazado).

-  DROP: descartar, borrar el paquete. Al igual que REJECT, el paquete
   no sigue su rumbo, pero en este caso no se envía un paquete de error,
   por lo que el emisor no tiene forma de enterarse que ha pasado. El
   paquete simplemente es ignorado.

-  LOG: registrar (a través de syslogd) un mensaje con una descripción
   del paquete. Se debe tener en cuenta que esta acción no interrumpe el
   procesamiento y la ejecución de la cadena continúa con la regla
   siguiente, razón por la que para registrar los paquetes rechazados se
   necesita una regla LOG y una regla REJECT/DROP.

-  ULOG: similar a log, pero el registro es a través de ulogd.

-  NOMBRE_DE_LA_CADENA: saltar a la cadena dada y evalúa sus reglas. Esta
	 acción se utiliza para clasificar el trafico y agrupar paquetes especificos
	 para realizar acciones sobre los mismos de una forma mas ordenada, por ejemplo,
	 agrupar en una cadena todo el tráfico proviniente del exterior con destino
	 a un servidor especifico, o agrupar todo el trafico proviniente de un conjunto
	 de sub redes como pueden ser las redes wifi e intranet.

-  RETURN: interrumpir el procesamiento de la cadena actual y regresar a
   la cadena que la llamó; en el caso de que la cadena actual sea una
   estándar no hay cadena que la haya llamado, por lo que en su lugar se
   ejecutará la acción predeterminada (definida con la opción -P de
   iptables).

Exclusivas de la tabla NAT
''''''''''''''''''''''''''

-  SNAT: aplicar NAT de origen. Básicamente se puede cambiar el ip de
   origen, o el puerto de origen.

-  DNAT: aplicar NAT de destino. Similar a SNAT pero permite modificar
   el puerto de destino y la ip de destino. Su uso más habitual es el
   reenvío de puertos.

-  MASQUERADE: aplicar enmascaramiento (un caso especial de NAT de
   origen).

-  REDIRECT: redirigir un paquete a un puerto determinado del mismo
   firewall. Muy común en la implementación de un proxy web
   transparente.

Orden de evaluación de las cadenas
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Una vez que una conexión llega al firewall, la misma sigue un camino
predeterminado para recorrer las tablas y cadenas necesarias para su
evaluación. El orden depende principalmente del origen y destino de la
conexión, es decir, desde donde proviene la conexión, y hacia dónde va:

-  Si la conexión se originó en un equipo externo, y su destino es otro
   equipo distinto del firewall: la primer cadena que se evalúa es
   **prerouting**, luelgo **forward** y por último **postrouting**.

-  Si la conexión se originó en un equipo externo, y su destino es el
   firewall (por ejemplo si desde un equipo nos queremos conectar al
   firewall vía ssh): la primer cadena que se evalúa es **prerouting**
   y luego **input**.

-  Si la conexión se originó en el firewall, las cadenas que se evalúan
   son **output** y luego **postrouting**.

.. figure:: imagenes/unidad02/image_2.png
   :alt: Orden de evaluación de las cadenas
   :scale: 75 %
   :align: center

   Fig. 3 - Orden de evaluación de las cadenas

Por esta razón, si queremos filtrar las conexiones entrantes o salientes
a nuestra red, el firewall debe estar necesariamente en medio como
mencionamos previamente.

Sintaxis de iptables
~~~~~~~~~~~~~~~~~~~~

Los programas iptables e ip6tables permiten manipular las tablas,
cadenas y reglas. Sus parámetros más utilizados son:

Parámetros:
^^^^^^^^^^^

-  La opción **-t tabla** : indica en qué tabla operar (la tabla filter
   de forma predeterminada).

-  La opción **-N cadena**: crea una nueva cadena.

-  La opción **-X cadena** : elimina una cadena vacía y sin uso.

-  La opción **-A cadena regla**: añade una regla al final de la cadena dada.

-  La opción **-I cadena número_regla regla**: inserta una regla antes de la regla con
   número *número_regla*.

-  La opción **-D cadena número_regla** (o -D *cadena regla*) : elimina una
   regla en una cadena, la primera sintaxis identifica la regla que se desea
   eliminar por su número, mientras que la segunda la identifica por su contenido.

-  La opción **-F cadena** : vacía una cadena (borra todas sus reglas), si no
   menciona ninguna cadena, eliminará todas las reglas de la tabla.

-  La opción **-L cadena** : muestra las reglas de la cadena.

-  Por último, la opción **-P cadena acción** define la acción predeterminada
   o "política" para una cadena dada; tenga en cuenta que sólo las cadenas estándar
   puede tener dicha política.

Es importante que mencionemos las políticas por defecto de un firewall.
Estas pueden ser de dos tipos:

-  Aceptar por defecto.

-  Rechazar por defecto.

El tipo de política cambia completamente el comportamiento de nuestro
firewall, dado que por ejemplo si deseamos solamente bloquear un
conjunto de puertos determinados, lo recomendable es utilizar como
política por defecto "Aceptar", de modo que solo tengamos que agregar
las reglas específicas para bloquear el tráfico no deseado. Por el
contrario, la política de “Rechazar por defecto” es la más segura, pero
exige que tengamos un entendimiento mayor de nuestra red y su uso,
principalmente porque al negar todo por defecto, debemos habilitar uno a
uno los diferentes tipos de tráfico que deseamos permitir, y esto
incluye conocer con detenimiento el uso que se hace de nuestra red, para
evitar que la misma quede sin servicio (por ejemplo debemos permitir el
acceso al servicio de DNS, navegación, uso de ftp, ssh, ntp, etc.).

Veamos un ejemplo sencillo: supongamos que nuestra política de seguridad
establece que solamente se debe bloquear el tráfico smtp saliente de
nuestra red (red 10.0.0.0/24), para todos los usuarios excepto para el
servidor de correos cuya IP es 10.0.0.4. A continuación presentamos la
topología

.. figure:: imagenes/unidad02/image_3.png
   :alt: Ejemplo de red con firewall como gateway
   :scale: 75 %
   :align: center

   Fig. 4 - Ejemplo de red con firewall como gateway

En este caso nos conviene utilizar como política por defecto a
"Aceptar", y solo agregar las reglas pertinentes que cumplan con lo
establecido. Para este caso, el conjunto de reglas sería el siguiente:

.. code:: bash

    # Definimos la política por defecto en Aceptar

    iptables -P INPUT ACCEPT
    iptables -P OUTPUT ACCEPT
    iptables -P FORWARD ACCEPT

    # El servidor de correos interno puede salir al puerto TCP 25 (SMTP)

    iptables -t filter -A FORWARD -s 10.0.0.4 -p tcp --dport 25 -j ACCEPT

    # El resto de la red no puede salir al puerto TCP 25

    iptables -t filter -A FORWARD -s 10.0.0.0/24 -p tcp --dport 25 -j REJECT

Es importante resaltar que las reglas fueron anexadas a la tabla *forward*
debido a que el origen y el destino son distintos del
firewall (origen: equipos de la red interna, destino: equipos de otras
redes). A su vez, el orden en el que se evalúan las reglas es secuencial
(recuerden que la opción -A agrega las reglas al final de la cadena)
motivo por el cual primero permitimos la salida del servidor de correos
y luego si denegamos el resto de la red. Otro detalle importante es que
este conjunto de reglas se aplica y permanece en memoria, por lo que si
el servidor se reinicia las mismas se pierden. Lo recomendable es
guardarlas dentro de un script de bash, y ejecutar el mismo
automáticamente al inicio, o luego de levantar las interfaces de red,
como veremos más adelante.

Analicemos un poco más en detalle las reglas.

Reglas
^^^^^^

Cada regla es expresada como **condiciones -j acción opciones_acción**.
Si describe varias condiciones en la misma regla, entonces el criterio es
la conjunción ("y" lógico) de las condiciones, que son al menos tan restrictivas
como cada condición individual. Por ejemplo:

.. code:: bash

  iptables -t filter -A  FORWARD -s 10.0.0.4 -p tcp --dport 25 -j ACCEPT

En este caso las condiciones son 3:

- El origen (source) tiene que ser el host cuya IP es 10.0.0.4 .

- El protocolo tiene que ser TCP.

- El puerto de destino tiene que ser el 25.

La condición **-p protocolo** coincide con el campo de protocolo del paquete IP,
y los valores más comunes son **tcp, udp, icmp e icmpv6**. Anteponer la
condición con un signo de exclamación niega la condición, la cual equivale a
"todos los paquetes cuyo origen no sea la ip 10.0.0.4".

.. code:: bash

  iptables -t filter -A FORWARD ! -s 10.0.0.4 -p tcp --dport 25 -j DROP

Este mecanismo de negación no es específico de la opción -s y se puede aplicar
a todas las otras condiciones también. La condición **-s dirección** ó
**-s red/máscara** coincide con la dirección de origen del paquete. De igual
manera, **-d dirección** ó **-d red/máscara** coincide con la dirección de
destino.

La condición **-i interfaz** selecciona los paquetes
procedentes de la interfaz de red dada y **-o interfaz** selecciona los paquetes que salen a través de una interfaz específica.

Hay condiciones más específicas, dependiendo de las condiciones genéricas descriptas
anteriormente. Por ejemplo, puede complementar la condición **-p tcp** con condiciones sobre
los puertos TCP, cláusulas como **--source-port puerto** y **--destination-port puerto**.
La condición **--state estado** coincide con el estado de un paquete en una conexión.
El estado NEW describe un paquete que inicia una nueva conexión; ESTABLISHED coincide con paquetes
pertenecientes a una conexión ya existente y RELATED coincide con paquetes iniciando una
nueva conexión relacionada con una ya existente (lo cual es útil para las conexiones ftp-data
en el modo «activo» del protocolo FTP).

Veamos ahora un poco las opciones disponibles
por cada acción. Por ejemplo, la acción LOG tiene las siguientes opciones: **--log-priority**, con un
valor predeterminado de warning, indica la severidad de los mensajes
syslog; **--log-prefix** permite especificar un prefijo de texto para
diferenciar los mensajes registrados; **--log-tcp-sequence**,
**--log-tcp-options** y **--log-ip-options** indican datos
adicionales que se integrarán en el mensaje: el número de secuencia TCP,
opciones TCP y las opciones IP, respectivamente. La acción DNAT ofrece
la opción **--to-destination dirección:puerto** para indicar la
nueva dirección IP y/o puerto de destino. De manera similar, SNAT
proporciona **--to-source dirección:puerto** para indicar la nueva
dirección IP y/o puerto de origen. La acción REDIRECT ofrece la opción
**--to-ports puerto(s)** para indicar el puerto o rango de puertos al
que debe redirigir los paquetes.

Ejemplo práctico
~~~~~~~~~~~~~~~~

Siguiendo con el ejemplo de la red anterior. Supongamos ahora que la
política de seguridad establece que se debe bloquear todo el tráfico
saliente, ha excepción de los siguientes servicios:

-  Web

-  IMAP y IMAPs

-  DNS (se usarán los servidores DNS provistos por el proveedor de
   internet)

-  SSH (solo al firewall desde la red interna)

-  SMTP (solo el servidor de correos)

Dado que los equipos en la red interna tienen direccionamiento IP
privado, necesariamente tienen que ser enmascarados (NAT) para poder
salir a internet con la dirección IP del firewall, que en este caso es
el default gateway. El conjunto de reglas sería el siguiente:

.. code:: bash

    #!/usr/bin/env bash

    # Definimos la política por defecto en DROP

    iptables -P INPUT DROP
    iptables -P OUTPUT DROP
    iptables -P FORWARD DROP

    # El servidor de correos interno puede salir al puerto TCP 25 (SMTP) y TCP 465 (SMTPs)

    iptables -t filter -A FORWARD -s 10.0.0.4 -p tcp --dport 25 -j ACCEPT
    iptables -t filter -A FORWARD -s 10.0.0.4 -p tcp --dport 465 -j ACCEPT

    # Desde la subred local se puede salir a los puertos TCP 80(HTTP),443(HTTPs),
    # 143(IMAP), 993(IMAPs), 110(POP3), 995(POP3s), 53(DNS) y UDP 53 (DNS).

    iptables -t filter -A FORWARD -s 10.0.0.0/24 -p tcp --dport 80 -j ACCEPT
    iptables -t filter -A FORWARD -s 10.0.0.0/24 -p tcp --dport 443 -j ACCEPT
    iptables -t filter -A FORWARD -s 10.0.0.0/24 -p tcp --dport 143 -j ACCEPT
    iptables -t filter -A FORWARD -s 10.0.0.0/24 -p tcp --dport 993 -j ACCEPT
    iptables -t filter -A FORWARD -s 10.0.0.0/24 -p tcp --dport 110 -j ACCEPT
    iptables -t filter -A FORWARD -s 10.0.0.0/24 -p tcp --dport 995 -j ACCEPT
    iptables -t filter -A FORWARD -s 10.0.0.0/24 -p tcp --dport 53 -j ACCEPT
    iptables -t filter -A FORWARD -s 10.0.0.0/24 -p udp --dport 53 -j ACCEPT

    # Acceso SSH al firewall

    iptables -t filter -A INPUT -s 10.0.0.0/24 -p tcp --dport 22 -j ACCEPT

    # A su vez necesitamos que desde el firewall también se acceda a los servidores DNS

    iptables -t filter -A OUTPUT -s 10.0.0.1 -p tcp --dport 53 -j ACCEPT
    iptables -t filter -A OUTPUT -s 10.0.0.1 -p udp --dport 53 -j ACCEPT

    # Se enmascaran todas las conexiones para que cuando salga a
    # internet lo hagan con la IP pública del firewall

    iptables -t nat -A POSTROUTING -o eth1 -s 10.0.0.0/24 -j MASQUERADE


Como vemos, una política de seguridad muy simple se convierte en varias
reglas de iptables, las que como mencionamos previamente, deben estar en
un script de bash que se ejecute al inicio y las aplique, porque sino al
reiniciar el sistema se perderán. Hay varias estrategias para conseguir
esto, pero una de las más utilizadas es ejecutar este script de bash al
levantar la interfaz interna, por ejemplo. Para esto debemos guardar el
script con todas las reglas de iptables en
/usr/local/etc/mi_firewall.fw, y por medio del parámetro "up" en el
archivo /etc/network/interfaces invocarlo

.. code:: bash

    auto eth0

    iface eth0 inet static
        address 10.0.0.1
        network 10.0.0.0
        netmask 255.255.255.0
        broadcast 10.0.0.255
        up /usr/local/etc/mi_firewall.fw
        down /usr/local/etc/mi_firewall-clean.fw

Del mismo modo que existe parámetro "up", existe el parámetro "down" que
sirve para invocar un script cada vez que se baja una interfaz. Es
interesante para poder limpiar todas las reglas antes de aplicarlas
nuevamente (supongamos que cambiamos el script y queremos aplicar
nuevamente las reglas) . Un ejemplo del script "mi_firewall-clean.fw"
es el siguiente

.. code:: bash

    #! /usr/bin/env bash

    cat /proc/net/ip_tables_names | while read table; do
      iptables -t $table -L -n | while read c chain rest; do
          if test "X$c" = "XChain" ; then
            iptables -t $table -F $chain
          fi
      done
      iptables -t $table -X
    done

Básicamente lo que hace este script es obtener todas las tablas
definidas (que contengan reglas) y limpiarlas por medio del parámetro
-F.
De este modo si cambiamos las reglas modificando el script, simplemente bajando
y volviendo a subir la interfaz, estaríamos aplicando dicha regla.

Simplificando la creación de reglas con FWBuilder
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si bien es importante tener claros los conceptos y entender como
funciona iptables con todas sus opciones, a la hora de trabajar a diario
e implementar una política de seguridad extensa, con muchas reglas,
varias subredes, equipos, etc; escribir a mano estas órdenes se vuelve
algo tedioso. Lo recomendable es utilizar una herramienta de alto nivel
como fwbuilder. El principio que persigue esta herramienta es simple. En
el primer paso es necesario describir todos los elementos que
intervendrán en las reglas:

-  el propio firewall, con sus interfaces de red;

-  las redes, con sus rangos de direcciones IP correspondientes;

-  los servidores;

-  los puertos pertenecientes a los servicios alojados en los
   servidores.

Luego puede crear las reglas simplemente arrastrando y soltando acciones
en los objetos. Unos cuantos menús contextuales pueden cambiar la
condición (negarla, por ejemplo). A continuación, deberá elegir la
acción, configurarla y listo.

.. figure:: imagenes/unidad02/image_4.png
   :alt: FWBuilder
   :scale: 98 %
   :align: center

   Fig. 5 - Generación de reglas con FWBuilder

Luego fwbuilder puede generar un script de configuración del firewall
según las reglas que definió. Su arquitectura
modular le da la capacidad para generar scripts dirigidos a diferentes
sistemas (iptables para Linux, ipf para FreeBSD y pf para OpenBSD).

En la imagen anterior se ve el ejemplo del firewall que configuramos
anteriormente.

.. image:: caratulas/unidad03.pdf
	:scale: 86%

Unidad 3: Redes privadas virtuales
==================================

Una red virtual privada (VPN: Virtual Private Network) es una forma de
enlazar dos redes locales diferentes a través de Internet u otra red no
confiable, utilizando para ello un túnel. Este túnel
generalmente está cifrado por confidencialidad, pero no necesariamente.

Ejemplos comunes de uso son la posibilidad de conectar dos o más sucursales de
una empresa; permitir a los Administradores la conexión
desde su casa al centro de cómputos; o a los usuarios acceder a su equipo
dentro de la empresa desde un sitio remoto.

.. figure:: imagenes/unidad03/image_0.png
   :alt: VPN de acceso remoto
   :align: center
   :scale: 65 %

   Fig.1 Ejemplo de conexión VPN de un usuario remoto a la red interna

Arquitecturas de conexión VPN
-----------------------------

Podemos clasificar a una conexión VPN según la arquitectura de conexión
utilizada, es decir, extremo a extremo como se realiza la conexión, sobre que
tecnologías y como son vinculados.

VPN de acceso remoto
~~~~~~~~~~~~~~~~~~~~

Es quizás el modelo más utilizado actualmente, y consiste en usuarios o
proveedores que se conectan con la empresa desde sitios remotos
(domicilios, hoteles, etc.) utilizando Internet como vínculo de acceso.
En este tipo de arquitectura, el cliente inicia la conexión, establece el
vínculo y una vez autenticado tiene un nivel de acceso muy similar al que
tienen en la red local de la empresa. En esta arquitectura, el cliente pasa a
ser un equipo más en la red.

La Fig. 1 representa este caso de uso.

VPN punto a punto
~~~~~~~~~~~~~~~~~

Este esquema se utiliza para conectar oficinas remotas con la sede
central de la organización. El servidor VPN acepta las conexiones vía Internet
provenientes de los sitios y establece el túnel VPN punto a punto, es decir
entre el servidor de la central y el de la sucursal solamente. Si existieran
diferentes sucursales, las mismas no se verían directamente entre ellas, sino a
través del servidor central, dado que en esta arquitectura, el vinculo es uno
a uno.

.. figure:: imagenes/unidad03/image_1.png
   :alt: VPN punto a punto
   :align: center
   :scale: 65 %

   Fig.2 Ejemplo de conexión VPN punto a punto

Tunneling
~~~~~~~~~

La técnica de tunneling consiste en encapsular un protocolo de red sobre
otro (protocolo de red encapsulador) creando un túnel dentro de una red
de computadoras. El establecimiento de dicho túnel se implementa
incluyendo una PDU (unidades de datos de protocolo) determinada dentro
de otra PDU con el objetivo de transmitirla desde un extremo al otro del
túnel sin que sea necesaria una interpretación intermedia de la PDU
encapsulada. De esta manera se encaminan los paquetes de datos sobre
nodos intermedios que son incapaces de ver en claro el contenido de
dichos paquetes. El túnel queda definido por los puntos extremos y el
protocolo de comunicación empleado, que entre otros, podría ser SSH.


.. figure:: imagenes/unidad03/image_2.png
   :alt: VPN Tunneling
   :align: center
   :scale: 65 %

   Fig.3 Ejemplo de encapsulado de PDU

VPN over LAN
~~~~~~~~~~~~

Es una variante del tipo "acceso remoto" pero, en vez de utilizar Internet
como medio de conexión, emplea la misma red de área local (LAN) de la empresa.
Sirve para aislar zonas y servicios de la red interna. Esta capacidad lo hace
muy conveniente para mejorar las prestaciones de seguridad de las redes
inalámbricas (WiFi).

Un ejemplo clásico es un servidor con información sensible, como el que
aloja el sistema de sueldos, ubicado detrás de un equipo VPN, el cual
provee autenticación adicional más el agregado del cifrado, haciendo
posible que solamente el personal de recursos humanos habilitado pueda
acceder a la información. Otro ejemplo es la conexión a redes Wi-Fi
haciendo uso de túneles cifrados IPSec o SSL que además de pasar por los
métodos de autenticación tradicionales (WEP, WPA, direcciones MAC, etc.)
agregan las credenciales de seguridad del túnel VPN creado en la LAN
interna o externa.

.. figure:: imagenes/unidad03/image_3.png
   :alt: VPN over LAN
   :align: center
   :scale: 45 %

   Fig.3 Ejemplo de VPN over LAN

VPNs según el tipo de conexión
------------------------------

Otra forma de clasificar las VPNs, es según la forman en la que se realiza
la conexión entre los extremos. En este caso también podemos clasificarlas
en 4 grupos:

Conexión de acceso remoto
~~~~~~~~~~~~~~~~~~~~~~~~~

Una conexión de acceso remoto es realizada por un cliente o un usuario de una
computadora que se conecta a una red privada. Los paquetes enviados a través
de la conexión VPN son originados en el cliente de acceso remoto, el que se
autentica contra el servidor de acceso remoto, y este a su vez se autentica ante
el cliente, realizando una autenticación cruzada entre un nodo y el servidor de
VPN.

.. figure:: imagenes/unidad03/image_4.png
   :alt: Conexión de acceso remoto
   :align: center
   :scale: 60 %

   Fig.4 Ejemplo de Conexión de acceso remoto

Conexión VPN router a router
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Una conexión VPN router a router consiste en la unión de dos router por medio de
una VPN, uniendo de este modo ambas redes LAN.

En este tipo de conexión, los paquetes enviados desde cualquier router no se
originan en los routers, sino que en equipos pertenecientes a sus LANs. De este
modo se unen ambas redes de forma totalmente transparente para los usuarios de
las mismas.

.. figure:: imagenes/unidad03/image_5.png
   :alt: VPN router a router
   :align: center
   :scale: 55 %

   Fig.4 Ejemplo de VPN router a router

Conexión VPN firewall a firewall
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Una conexión VPN firewall es similar al caso anterior, con el agregado de una
capa extra de seguridad que permite aplicar reglas especificas a la conexión VPN.

.. figure:: imagenes/unidad03/image_6.png
   :alt: VPN firewall a firewall
   :align: center
   :scale: 55 %

   Fig.5 Ejemplo de VPN firewall a firewall

VPN en entornos móviles
~~~~~~~~~~~~~~~~~~~~~~~

La VPN móvil se establece cuando el punto de terminación de la VPN no está fijo a
una única dirección IP, sino que se mueve entre varias redes como pueden ser las
redes de datos de operadores móviles o distintos puntos de acceso de una red Wifi.
Las VPNs móviles se utilizan mucho en seguridad pública, dando acceso a las
fuerzas de orden público a aplicaciones críticas tales como bases de datos para
identificación de criminales. Mientras uno de los puntos de la conexión se mueve
entre distintas subredes de una red móvil, siempre se mantiene el vínculo con el
otro extremo.

.. figure:: imagenes/unidad03/image_7.png
   :alt: VPN en entornos móviles
   :align: center
   :scale: 65 %

   Fig.6 Ejemplo de VPN en entornos móviles

Ventajas del uso de VPNs
------------------------

- Integridad, confidencialidad y no repudio: al poder realizar un tunel
  encriptado de extremo a extremo, podemos tener la certeza (o casi) de que nadie
  puede ver los datos enviados (confidencialidad), e incluso tampoco alterarlos
  (integridad). A su vez, como los extremos se autentican, como veremos mas adelante,
  también podemos estar seguro de cual es el origen de los mismos (no repudio).

- Las VPN reducen los costos y son sencillas de usar: Principalmente porque no necesitamos
  de costosos enlaces punto a punto para unir diferentes LANs, sino que podemos hacer
  uso de internet.

- Facilita la comunicación entre dos usuarios en lugares distantes: Al poder unir
  diferentes redes, permite que los usuarios se puedan comunicar y acceder a
  sistemas y equipos tal y como si estuvieran dentro de la misma red interna,
  por lo que no se tienen que agregar mecanismos extras de autenticación, o
  reglas de redirección de puertos para que un usuario externo pueda hacer uso
  de los mismos.


OpenVPN
-------

La herramienta más utilizadas para armar VPNs en GNU/Linux es OpenVPN,
dado que es una solución eficiente, fácil de desplegar y mantener, basada en SSL/TLS.

Su configuración involucra crear interfaces de red virtuales en el
servidor VPN y en los clientes; es compatible con interfaces
**tun** (para túneles a nivel de IP) y **tap** (para túneles a nivel
Ethernet). En la práctica, usualmente se utilizan interfaces *tun* excepto
cuando los clientes VPN deban integrarse a la red local del servidor a
través de un puente Ethernet.

OpenVPN se basa en OpenSSL para toda la criptografía SSL/TLS y
funcionalidades asociadas (confidencialidad, autenticación, integridad,
no repudio). Se puede configurar con una llave privada compartida o
con un certificado X.509 basado en la infraestructura de llave pública.
Se prefiere fuertemente esta última configuración ya que permite más
flexibilidad cuando se enfrenta a un número creciente de usuarios
itinerantes que acceden a la VPN.

Infraestructura de llave pública: easy-rsa
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El algoritmo RSA es ampliamente utilizado en criptografía de llave
pública. Involucra un «par de llaves», compuestas de una llave privada y
una llave pública. Las dos llaves están fuertemente relacionadas entre
ellas y sus propiedades matemáticas son tales que un mensaje cifrado con
la llave pública sólo puede ser descifrado por alguien que conozca la
llave privada, lo que asegura confidencialidad. En la dirección opuesta,
un mensaje cifrado con la clave privada puede ser descifrado por
cualquiera que conozca la llave pública, lo que permite autenticar el
origen del mensaje ya que sólo pudo haber sido generado por alguien con
acceso a la llave privada. Cuando se asocia una función de hash digital
(MD5, SHA1 o una variante más reciente), esto lleva a un mecanismo de
firma que puede aplicarse a cualquier mensaje, y garantizar que el mismo
no ha sido alterado durante su traslado a lo largo de la red. Sin
embargo, cualquiera puede crear un par de llaves, almacenar cualquier
identidad en ella y pretender ser la identidad que elijan. Una solución
involucra el concepto de una Autoridad Certificante (CA:
«Certification Authority») formalizado por el estándar X.509. Este
término se refiere a una entidad que posee un par de llaves confiable
conocido como certificado raíz. Sólo se utiliza este certificado para
firmar otros certificados (pares de llaves), luego que se siguieron
suficientes pasos para revisar la identidad almacenada en el par de
llaves. Las aplicaciones que utilizan X.509 luego pueden verificar los
certificados que se les presente si conocen los certificados raíz
confiables. OpenVPN sigue esta regla.

Dado que los CA públicos sólo expiden certificados a cambio de un pago
(importante), también es posible crear una autoridad de certificación privada
dentro de la empresa. El paquete easy-rsa proporciona herramientas que dan soporte a
la infraestructura de certificados X.509, implementados como un conjunto
de scripts haciendo uso del comando openssl, y que precisamente nos
permiten eso, crear una autoridad de certificación privada. Veremos más
adelante como utilizar esta herramienta para crear nuestra propia CA y
emitir certificados para nuestros clientes de VPN.

Ejemplo práctico
----------------

Veremos un ejemplo de uso de VPN para teletrabajo. Para ello
utilizaremos el tipo más común de todos "VPN de acceso remoto". Contamos
con empleados que trabajan fuera de la empresa, y necesitan acceder
a equipos en la red interna de la misma de forma que lo hacen cuando están
en la misma.

Para llevar adelante nuestro cometido, empezaremos por implementar una
Autoridad Certificante (CA), la que emitirá certificados para el personal
externo (clientes VPN) de una empresa ficticia denominada "Mambo-tango".
Estos certificados serán las credenciales que validarán y permitirán la
conexión de los mismos.

Creando nuestra propia CA
~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: bash

    root@servidor:~# apt-get install openvpn
    root@servidor:~# make-cadir pki-mambotango

Esto generará un directorio pki-mambotango con el conjunto de scripts
necesarios para generar los certificados de los usuarios de nuestra CA.
Además podremos definir ciertos datos propios de la empresa, los que
formarán parte de los certificados que se emitan    .

A continuación debemos entrar en el directorio pki-mambotango y editar
el archivo "vars". En el podremos definir los siguientes atributos, con
información propia de la organización

.. code:: bash

    export KEY_COUNTRY="AR"
    export KEY_PROVINCE="SF"
    export KEY_CITY="SantaFe"
    export KEY_ORG="Mambo-Tango"
    export KEY_EMAIL="admin@mambo-tango.org.ar"

luego debemos generar el par de claves propios de la CA, para eso antes
debemos cargar las variables modificadas, hacer una limpieza y
luego si generarlas

.. code:: bash

    root@servidor:~# . ./vars
    root@servidor:~# ./clean-all
    root@servidor:~# ./build-ca

Durante este paso se almacenarán las dos partes del par de llaves en
**keys/ca.crt** y **keys/ca.key (clave privada)**

Ahora podemos crear el certificado para el servidor VPN, así como también
los parámetros Diffie-Hellman necesarios en el servidor para la conexión
SSL/TLS. Se identifica el servidor VPN por su nombre DNS
vpn.mambo-tango.org.ar; se reutiliza este nombre para los archivos de
llaves generados (keys/vpn.mambo-tango.org.ar.crt para el certificado
público, keys/vpn.mambo-tango.org.ar.key para la llave privada):

.. code:: bash

    root@servidor:~# ./build-key-server vpn.mambo-tango.org.ar
    root@servidor:~# ./build-dh

El siguiente paso crea los certificados para los clientes VPN; debemos
generar un certificado para cada equipo o persona autorizada para utilizar
la VPN:

.. code:: bash

    root@servidor:~# ./build-key cliente1

Esto generará dentro de la carpeta *keys* los archivos *cliente1.crt (certificado)*
y *cliente1.key (clave privada)*. Estos archivos son los que deberá utilizar
el cliente de VPN para conectarse, como veremos luego.

Configuración de servidor de VPN
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El script de inicialización de OpenVPN intenta, de forma predeterminada, iniciar
todas las redes privadas virtuales definidas en /etc/openvpn/\*.conf. Para
configurar un servidor VPN debemos almacenar el archivo de configuración
correspondiente en este directorio.

Con muy pocos parámetros podemos tener un servidor OpenVPN sencillo. A continuación
mostramos un archivo de configuración de ejemplo, que guardaremos como
/etc/openvpn/server.conf. En caso de requerir más parámetros, puede tomar el
archivo de ejemplo que viene con el paquete openvpn
/usr/share/doc/openvpn/examples/sample-config-files/server.conf.gz.

.. code:: bash

    port 1194
    proto udp
    dev tun
    ca /etc/ssl/certs/ca.crt
    cert /etc/ssl/certs/vpn.mambo-tango.org.ar.crt
    key /etc/ssl/private/vpn.mambo-tango.org.ar.key
    dh /etc/ssl/certs/dh2048.pem
    server 10.8.0.0 255.255.255.0
    ifconfig-pool-persist ipp.txt
    push "route 192.168.10.0 255.255.255.0"
    push "dhcp-option DNS 192.168.10.2"
    push "dhcp-option DNS 192.168.10.3"
    keepalive 10 120
    comp-lzo
    persist-key
    persist-tun
    status openvpn-status.log
    verb 3

Los primeros 3 parámetros sirven para especificar el puerto donde escuchará
el servidor de VPNs, bajo que protocolo y el tipo de
interfaz a utilizar.

Los siguiente 4 definen todo lo relacionado con los certificados
X509. En particular definimos cual es el certificado de la CA en la que vamos
a confiar (en este caso, nuestra propia CA), cual es el certificado que
identifica a nuestro servidor, y su clave privada.

La opción server especifica la red a la que pertenecerán los clientes de VPN,
es decir, a cada uno de los clientes que se conecten, se les dará una IP fija
en esta subred (10.8.0.0/24). La información respecto de que IP fue asignada a
que cliente vpn, es logueada en en el archivo ipp.txt definido en la opción
ifconfig-pool-persist.

Los siguientes 3 parámetros son información que se envía a los clientes luego
de establecer la conexión. En este caso se envía una ruta, para que los mismos
puedan llegar a la subred interna (192.168.10.0/24 en este caso) utilizando como
gateway al servidor de VPN (el que tendrá la ip 10.8.0.1). Además se envía información
respecto de los servidores de DNS internos, para que estos puedan resolver los nombres
tal y como si estuvieran dentro de la propia red interna.

Los restantes parámetros no son tan relevantes, simplemente diremos que definen
el tiempo para determinar si un cliente perdió la conexión, definen que los paquetes
irán comprimidos con el algoritmo lza y algunas opciones de log.


Antes de reiniciar el servidor para que tome la configuración, debemos copiar
los certificados a su ubicación definida

.. code:: bash

    root@servidor:~# cp pki-mambotango/keys/ca.crt /etc/ssl/certs/
    root@servidor:~# cp pki-mambotango/keys/vpn.mambo-tango.org.ar.crt /etc/ssl/certs/
    root@servidor:~# cp pki-mambotango/keys/dh2048.pem /etc/ssl/certs/
    root@servidor:~# cp pki-mambotango/keys/ca.key /etc/ssl/private/
    root@servidor:~# cp pki-mambotango/keys/vpn.mambo-tango.org.ar.key /etc/ssl/private/

luego si reiniciamos el servidor y chequeamos que este activo

.. code:: bash

    root@servidor:~# service openvpn restart
    root@servidor:~# netstat -lntpu|grep 1194
    udp        0      0 0.0.0.0:1194            0.0.0.0:*                           893/openvpn


Configuración de los clientes de VPN
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cada cliente para poder conectarse al servidor, debe contar con
openvpn instalado (apt-get install openvpn), su certificado emitido
por la CA en la que confía el servidor de VPN (nuestra CA en este caso),
su clave privada, el certificado de la CA y una configuración mínima como la siguiente
, la que guardaremos en el archivo /etc/openvpn/cliente.conf:

.. code:: bash

    dev tun
    proto udp
    ca /etc/ssl/certs/ca.crt
    cert /etc/ssl/certs/client.crt
    key /etc/ssl/private/client.key
    client
    remote vpn.mambo-tango.org.ar 1194
    resolv-retry infinite
    nobind
    persist-key
    persist-tun
    ns-cert-type server
    comp-lzo
    verb 3

Como verán los parámetros son muy parecidos a los utilizados para el servidor
con la salvedad que aquí especificamos que es un cliente (opción client),
en que dirección y puerto escucha el servidor (parámetro remote) entre otras.
Cabe aclarar que el archivo ca.crt, debe ser el mismo que se utiliza en el
servidor de VPN, dado que ambos equipos deben confiar en los certificados emitidos
por dicha CA.

Otra aclaración importante, es que la dirección especificada en remote, debe
ser la IP publica del servidor de VPN. En nuestro ejemplo, el mismo se encuentra detrás
del firewall, por lo que será la IP pública del firewall, y luego en este deberemos
aplicar una redirección de puerto para redirigir y permitir el tráfico con destino al
puerto 1194, protocolo UDP.
Puede encontrar mas parámetros de configuración en el archivo
/usr/share/doc/openvpn/examples/sample-config-files/client.conf

Por último debemos editar el archivo /etc/default/openvpn y configurar el siguiente
parámetro para que la VPN levante automáticamente cada vez que inicia el sistema.

.. code:: bash

    AUTOSTART="all"

.. image:: caratulas/unidad04.pdf
	:scale: 86%

Unidad 4: Monitorización de servicios y recursos
================================================

Una de las herramientas más importantes en la vida de un administrador de
sistemas es el monitoreo de los servicios y recursos que administra. Esto
permite ver el comportamiento de los sistemas, anticipar posibles fallas,
predecir la falta de recursos o reaccionar de forma inmediata ante un
inconveniente. Es una herramienta muy valiosa que permite estar siempre un paso
adelante, y que por lo tanto vale la pena tomar con seriedad. Un viejo amigo
solía decir que un buen administrador es un completo desconocido para sus
usuarios, dado que anticipa gran parte de los posibles inconvenientes, y por lo
tanto no tiene que escuchar las quejas de estos. El monitoreo es la clave si
decidimos emprender ese camino.

Herramientas de monitoreo
-------------------------

Existe una gran variedad de herramientas para monitorear, que van desde la
posibilidad de monitorear un equipo a monitorear varios equipos en simultáneo.

Herramientas de monitoreo para un solo host
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Una de las formas más básicas de monitoreo de un servidor, es por medio de
herramientas que nos muestren el estado actual del equipo. A continuación
hablaremos de las mas utilizadas

TOP
^^^

Top nos permite una mirada rápida y detallada de nuestro sistema, el mismo
muestra en tiempo real un listado de los procesos que se están ejecutando,
indicando el uso de CPU, RAM, tiempo de ejecución, etc.

.. figure:: imagenes/unidad04/image_1.png
   :alt: TOP
   :align: center
   :scale: 70 %

   Fig.1 Herramienta TOP

HTOP
^^^^

Al igual que TOP, esta herramienta nos permite visualizar todos los procesos
en ejecución de una forma más visual, e identificar aquellos procesos que
consumen demasiada memoria o procesador, a la vez que vemos como se distribuye
la carga entre los distintos núcleos del sistema.

.. figure:: imagenes/unidad04/image_2.png
   :alt: HTOP
   :align: center
   :scale: 70 %

   Fig.2 HTOP

IOTOP
^^^^^

Esta herramienta, de aspecto similar a top, es utilizada para ver el uso que
hacen los procesos de la entrada/salida del sistema. Principalmente el acceso
al disco rígido, tanto para lectura como escritura. Muy interesante para
detectar procesos que leen o escriben demasiado en el disco, y por lo tanto
generan carga en el sistema.

.. figure:: imagenes/unidad04/image_3.png
   :alt: IOTop
   :align: center
   :scale: 70 %

   Fig.3 IOTOP


IFTOP
^^^^^

Siguiendo en la misma línea, IFTOP nos permite visualizar las diferentes
conexiones desde nuestro equipo hacia equipos externos, y analizar el uso de
ancho de banda que consumen las distintas conexiones, como así también el
consumo total de las interfaces.

.. figure:: imagenes/unidad04/image_4.png
   :alt: Monitoreo interno vs Monitoreo externo
   :align: center
   :scale: 70 %

   Fig.4 IFTOP


DSTAT
^^^^^
Dstat es un reemplazo de los anteriores, que permite visualizar todos los
recursos del sistema de forma instantánea. Por ejemplo el acceso al disco
en combinación con las interrupciones a la controladora de disco, o 	comparar
el ancho de banda de la red, el uso del cpu, la carga del sistema y la I/O
del disco en el mismo intervalo.

.. figure:: imagenes/unidad04/image_8.png
  :alt: Monitoreo interno vs Monitoreo externo
  :align: center
  :scale: 120 %

  Fig.5 Dstat

Herramientas de monitoreo para multiples hosts
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A la hora de monitorear varios hosts o servidores en una red, las herramientas
antes mencionadas no son de mucha utilidad. Para estos casos necesitamos
herramientas que se encarguen de relevar y analizar los recursos y servicios de
forma automática, informándonos ante anomalías ó inconvenientes, y
permitiéndonos hacer una análisis de los datos históricos para dimensionar el
uso y anticipar futuros problemas (por ejemplo el uso del almacenamiento).

Formas de realizar el monitoreo
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

La monitorización puede ser tanto interna como externa, dependiendo del lugar
donde se ubique el equipo encargado de monitorear los servidores o la red.

Para ciertas circunstancias la monitorización externa es mucho más fiable,
puesto que es independiente de los problemas que puede haber dentro de la red
donde se encuentra/an el/los equipo/s a monitorear. Sin embargo, la
monitorización interna es mucho mas flexible y segura, dado que se puede
monitorear un número mayor de variables, sin exponer demasiado los equipos, o la
información de estos.

.. figure:: imagenes/unidad04/image_5.png
   :alt: Monitoreo interno vs Monitoreo externo
   :align: center
   :scale: 40 %

   Fig.6 Monitoreo interno vs Monitoreo externo

Que podemos monitorear
^^^^^^^^^^^^^^^^^^^^^^

Podríamos decir que casi todo se puede monitorear, desde cuestiones como:
uso de cpu, memoria, espacio libre de disco, cantidad de procesos corriendo,
cantidad de usuarios conectados, cantidad de inodos libres, etc; a cuestiones
como el proceso de login dentro de un sistema, el tiempo de respuesta ante un
determinado proceso web, la temperatura y humedad en el centro de datos ó la
energía consumida por el mismo, etc.

Para poder monitorear estas variables debemos contar con algún
mecanismo de obtención de estos datos. Existen diversos métodos, siendo los más
utilizados el protocolo SNMP y el uso de agentes. SNMP (simple network managed
protocol), es un estandar muy difundido que explicaremos más adelante. Respecto
de los agentes, estos son programas que se instalan en los equipos a monitorear,
y cada cierto tiempo envían información a un nodo central, o también puede ser
que sean consultados periódicamente por este nodo. Cada herramienta propone el
suyo, y la mayoría cuentan con cierta flexibilidad, permitiéndonos monitorear
un sin número de variables predefinidas o incluso crear nuestras propias
variables.


Componentes de un sistema de monitoreo
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Un buen sistema de monitoreo debe ser capaz de censar periódicamente
determinadas variables del sistema; reaccionar ante determinadas circunstancias y
emitir una notificación o alerta; informar cuando el inconveniente se ha resuelto,
o las variables monitoreadas vuelven a un estado normal. A su vez debe soportar
el monitoreo de muchos sistemas en simultáneo. Para llevar adelante este cometido
la mayoría de los sistemas de monitoreo cuentan con los siguientes componentes:

- Agentes para la obtención de datos: Estos pueden ser internos, o externos. En
  el caso de los agentes internos, se instalan en los sistemas, recolectan
  información y periódicamente la envían al servidor de monitoreo. Los agentes
  externos por lo general se ejecutan en el servidor de monitoreo y consideran
  al sistema a monitorear como una caja negra, realizando determinados test de
  manera periódica como por ejemplo medir el tiempo de respuesta de un ping,
  si un puerto se encuentra abierto, o realizar determinadas consultas por
  medio del protocolo HTTP.

- Sistema de notificaciones: Ante determinadas condiciones, el sistema debe
  ser capaz de emitir alertas. Estas por lo general son canalizadas a través de
  diversos medios como e-mails, SMS, jabber o simplemente la visualización en un
  tablero web.

- Base de datos de históricos: El poder almacenar los valores de determinadas
  variables a lo largo del tiempo, nos permite analizar el comportamiento de un
  sistema respecto de un conjunto de variables, para saber entre otras cosas,
  como evoluciona el uso, que podemos considerar como una situación normal, y en
  que períodos ocurrieron eventos extraordinarios. Esta información es muy
  relevante a la hora de tomar decisiones respecto de por ejemplo adquirir nuevo
  equipamiento.


.. figure:: imagenes/unidad04/image_6.png
   :alt: Componentes de un sistema de monitoreo
   :align: center
   :scale: 70 %

   Fig.7 Componentes de un sistema de monitoreo

Protocolo SNMP
^^^^^^^^^^^^^^

El Protocolo Simple de Administración de Red o SNMP (del inglés Simple Network
Management Protocol) es un protocolo que facilita el intercambio de información
de administración entre dispositivos de red. Los dispositivos que normalmente
soportan SNMP incluyen routers, switches, servidores, estaciones de trabajo,
impresoras y muchos más.

Permite a los administradores supervisar el funcionamiento de la red, buscar y
resolver sus problemas, y planear su crecimiento.

Se compone de un conjunto de normas para la gestión de la red, incluyendo
una capa de aplicación del protocolo , una base de datos de esquema, y un
conjunto de objetos de datos. Las versiones de SNMP más utilizadas son SNMP
versión 1 (SNMPv1) y SNMP versión 2 (SNMPv2), aunque también existe la versión
SNMPv3.

Las variables accesibles a través de SNMP están organizadas en jerarquías. Estas
jerarquías y otros metadatos (tales como el tipo y la descripción de la
variable), se describen por Bases de Información de Gestión (MIB).

Los puertos utilizados por el protocolo son el 161 y 162. En el 161 escuchan los
dispositivos las peticiones de información (las cuales realiza el servidor de
monitoreo de forma periódica). El puerto 162 se usa a la inversa, son los
dispositivos los que envían información al servidor de monitoreo cuando se cumplen
determinadas situaciones (por ejemplo una tormenta de broadcast en un puerto
determinado). Este último mecanismo en SNMP se denomina Traps (trampas).
Este protocolo puede ser instalado en GNU/Linux, mediante el paquete **snmpd**.

Herramientas de monitoreo
^^^^^^^^^^^^^^^^^^^^^^^^^

Son muchas las herramientas disponibles en GNU/Linux que cumplen con todo lo
anterior. Entre las mas interesantes podemos destacar:

- Nagios

- Cactic

- Zabbix

Hablaremos un poco de Zabbix, ya que es uno de los mas completos y potentes.

Zabbix
^^^^^^

Fue diseñado para monitorear la performance y disponibilidad de diferentes
componentes de una infraestructura IT. Permite obtener diferentes tipos de datos
de una red en tiempo real de manera sencilla, eficiente y escalable. Permite
monitorear miles de dispositivos sin perder la flexibilidad y sin tener
que realizar grandes cambios en el equipamiento utilizado para el monitoreo.

Cuenta con monitoreo distribuido mediante el uso de proxies de zabbix, lo que
nos permite por ejemplo monitorear diferentes sucursales de una empresa, desde
una única ubicación, sin tener que recolectar todos los datos en un nodo central,
es decir: ubicar un proxy de zabbix en cada sucursal y gestionarlo desde un
nodo central, lo que permite aprovechar las ventajas ofrecidas tanto por el
monitoreo interno como el externo.

Su interfaz web es muy intuitiva y sencilla de usar, y cuenta con gráficos de
diferentes tipos para analizar múltiples variables en simultáneo.

Su sistema de alarmas o triggers es muy sofisticado y flexible, y nos permite
personalizar cuando una alarma debe emitirse y porque medio (por ejemplo, que se
muestre en el panel web por un minuto, y que a los 5 minutos se envíe un e-mail).

Pero sin lugar a dudas, el agente de zabbix es uno de los mas destacados de esta
herramienta, dado que por defecto nos permite monitorear múltiples variables
predefinidas de un host, con un mínimo de configuraciones para conseguirlo. Otra
de sus virtudes es el uso de templates, los que simplifican la configuración de
los hosts a monitorear.

Conceptos
'''''''''

Es importante entender los diferentes términos del mundo zabbix para rápidamente
entrar en sintonía. Los mas releevantes son los siguientes:

- Host

	dispositivo de red con IP que se desea monitorear.

- Host group

	Una agrupación lógica de hosts. Pueden contener tanto hosts como
	templates. Se utilizan principalmente para otorgar permisos de acceso a un
	determinado grupo de usuarios.

- Item

	Una variable que se puede medir en el tiempo. Es realmente el corazón
	del monitoreo, puede ser el espacio disponible de disco, la memoria libre, la
	cantidad de instancias de un servicio corriendo, etc.

- Trigger

	Expresión lógica que define un límite que es utilizado para evaluar
	los valores de los ítems. Cuando el límite se supera los triggers cambian del
	estado "OK" al estado "Problem" disparando eventos asociados a este cambio (por
	ejemplo enviar un e-mail notificando el problema). Cuando el valor del/los item/s
	vuelve a estar por debajo del límite, el estado del trigger cambia de "Problem" a
	"OK" originando un nuevo evento, el que puede asociarse con la acción de enviar
	un nuevo e-mail notificando la vuelta a la normalidad.

- Evento

	Ocurrencia de algo que requiere atención, como por ejemplo el cambio de
	estado de un trigger.

- Action (Acción)

	Reacción asociada a un evento. Las acciones consisten en
	operaciones como envió de e-mails, ejecución de scripts ó comandos remotos, etc.
	La ejecución de comandos puede ser muy útil a la hora de tomar una primer medida
	de acción de forma automática, por ejemplo supongamos que el servidor web se ha
	detenido: podríamos intentar levantarlo mediante la ejecución de un comando.

- Notification (Notificación)

	Mensaje acerca de un evento, que es enviado al usuario por medio de un canal
	predefinido.

- Remote command (Comando remoto)

	Comando predefinido que es ejecutado en un host monitoreado, luego de que se
	cumpla un/unas condición/es predeterminadas.

- Template (Plantilla)

	Conjunto de entidades como items, triggers, graficas, etc) listas para ser
	aplicadas a uno o varios hosts.

- Application (Aplicación)

	Agrupación de items en un grupo lógico.

- Web scenario (Escenario web)

	Una o multiples peticiones HTTP utilizadas para chequear la disponibilidad de un
	sitio web.

- Zabbix agent (Agente de zabbix)

	Un proceso ejecutandose en el host objeto del monitoreo, activamente
	monitoreando los recursos locales de dicho host.

Uso de zabbix
`````````````
El frontend de zabbix es muy intuitivo, y nos presenta en un primer pantallazo
un estado general de los sistemas monitoreados y del servidor
de monitoreo.

.. figure:: imagenes/unidad04/image_7.png
   :alt: Tablero de control de zabbix
   :align: center
   :scale: 100 %

Entre la información más importante, podemos destacar los últimos triggers que
se dispararon con sus notificaciones, y el estado general del servidor.

Configurar zabbix consiste en agregar host para monitorear, asociar a cada uno
de ellos determinados templates (muchos ya predefinidos) o items, y configurar
determinadas alarmas (triggers).


Ejemplo práctico
^^^^^^^^^^^^^^^^

Para investigar un poco mas zabbix, utilizaremos una de las versiones disponibles
para descargar desde la página de Zabbix (https://www.zabbix.com/download),
denominada *Zabbix Appliance*. Estas imágenes están instaladas y
pre-configuradas, listas para un rápido deploy. Utilizaremos la versión
disponible para Virtualbox, en formato "Open virtualization format (.ovf)".

Nuestro objetivo en esta práctica será monitorear la maquina virtual que hemos
utilizado en la unidad 1.

Se propone seguir el video tutorial donde se explica el uso y configuración
de los hosts en zabbix.

.. image:: caratulas/unidad05.pdf
	:scale: 86%

Unidad 5: Virtualización
========================

La virtualización es uno de los avances más grandes de la informática en los 
últimos años. El término abarca varias abstracciones y técnicas de simulación 
de equipos virtuales con un grado variable de independencia de hardware real. 
Según Wikipedia es la creación a través de software de una versión virtual de 
algún recurso tecnológico, como puede ser una plataforma de hardware, un sistema 
operativo, un dispositivo de almacenamiento u otros recursos de red, y creo es 
una de las mejores definiciones. Nosotros nos concentraremos en la virtualización 
de plataformas de hardware.

Un servidor físico puede almacenar varios sistemas que funcionan de forma 
simultánea y aislada, lo que se denomina consolidación. Veamos un poco este 
concepto.

Consolidación
-------------

Era común encontrar data centers donde cada aplicación alojada tenía asignado 
uno o más servidores, lo que si bien era efectivo desde el punto de vista del 
servicio (mayor hardware disponible para afrontar la demanda), era muy poco 
eficiente desde el punto de vista del uso de los recursos de cada servidor.

Estudios demuestran que la demanda de recursos, por lo general, tiene una 
naturaleza cíclica en la que se observan momentos de mucha demanda y momentos 
donde la misma es muy baja ó nula. 

Este comportamiento se puede observar en la siguiente figura, donde se muestra 
la demanda de recursos de dos aplicaciones diferentes a lo largo del tiempo:

.. figure:: imagenes/unidad05/image_1.png
   :alt: TOP
   :align: center
   :scale: 160 %

   Fig.1 Naturaleza cíclica del uso de recursos

Como se observa, gran parte del tiempo (cerca de un 60%) dichas 
aplicaciones no demandan recursos de hardware, utilizando por lo general solo 
el 20 ó 30% de su potencia de cómputo. Cada servidor tiene un costo casi 
constante a lo largo del tiempo relacionado con su consumo eléctrico, 
refrigeración, reemplazo de hardware defectuoso, etc; por lo que es costoso 
desaprovechar estos recursos.	

Una solución eficiente a este problema es la consolidación de servicios ó 
simplemente consolidación. Consiste en agrupar dos o más servicios 
(aplicaciones) en un mismo servidor físico, utilizando los recursos de este de 
forma más eficiente (en promedio).

.. figure:: imagenes/unidad05/image_2.png
   :alt: TOP
   :align: center
   :scale: 160 %

   Fig.2 Consolidación de servicios

La virtualización es la forma mas utilizada en la actualiadad para consolidar 
servicios y aprovechar al máximo los rescursos de hardware disponibles, dado que
permite en mayor o menor medida (dependiendo de la tecnología que se utilice 
para virtualizar) un aislamiento de las aplicaciones que conviven en un mismo 
hardware.


Conceptos básicos
-----------------

La virtualización se basa en una capa de abstracción entre el hardware de la 
máquina física (que denominaremos host) y el sistema operativo de la máquina 
virtual (virtual machine, que denominaremos guest). Esta capa, comunmente 
denominada Hypervisor o VMM (Virtual Machine Monitor), divide los recursos 
en uno o más entornos de ejecución. 

El hypervisor maneja, gestiona y arbitra los cuatro recursos principales de una 
computadora (CPU, Memoria, Dispositivos Periféricos y Conexiones de Red) y así 
podrá repartir dinámicamente dichos recursos entre todas las máquinas virtuales 
definidas en el computador central. Esto hace que se puedan tener varias 
maquinas virtuales ejecutándose en el mismo ordenador físico.

Hay múltiples soluciones de virtualización, cada una con sus ventajas y 
desventajas. Nos concentraremos en Xen, KVM y LXC; pero otras 
implementaciones notables incluyen las siguientes:

- **QEMU**

 Es un emulador en software para un equipo completo; su rendimiento está 
 lejos de la velocidad que uno podría conseguir si ejecutara nativamente, pero 
 esto permite ejecutar en el hardware emulado sistemas operativos sin 
 modificación o experimentales. También permite emular una arquitectura de 
 hardware diferente: por ejemplo, un sistema amd64 puede emular una máquina arm. 
 QEMU es software libre.

- **Bochs**

 Es otra máquina virtual libre, pero sólo emula la arquitectura x86 (i386 y 
 amd64).

- **VMWare**

 Es una máquina virtual privativa; como es una de las más antiguas es también una 
 de las más conocidas. Funciona sobre cimientos similares a los de QEMU. 

- **VirtualBox** 

 Es una máquina virtual que es software libre en su mayor parte 
 (algunos componentes adicionales están disponibles bajo una licencia privativa). 
 Es más joven que VMWare y limitada a las arquitecturas i386 y amd64, pero 
 incluye cierta compatibilidad con instantáneas y otras funcionalidades 
 interesantes.

XEN
---

Xen es una solución que utiliza una técnica denominada «paravirtualización». En 
la misma el hypervisor es una fina capa de abstracción entre el hardware y los 
sistemas superiores, que actúa como árbitro controlando el acceso al hardware 
desde las máquinas virtuales. El hypervisor en este caso sólo gestiona unas pocas 
instrucciones, las demás se ejecutan directamente en el hardware en nombre de 
los sistemas. La principal ventaja es que no se degrada el rendimiento y los 
sistemas se ejecutan a velocidades cercanas a la nativa; la desventaja es que el 
núcleo de los sistemas operativos que uno desee utilizar en un hypervisor Xen 
necesita ser adaptado para correr sobre Xen. 

Conceptos en XEN
~~~~~~~~~~~~~~~~

El hypervisor es la capa más baja que se ejecuta directamente en el hardware, 
inclusive debajo del núcleo (kernel). Este hypervisor puede dividir el resto del 
software entre varios dominios («domains»), los que pueden interpretarse como 
máquinas virtuales. Se conoce a uno de estos dominios (el primero en iniciar) 
como dom0 y tiene un rol especial ya que sólo este dominio puede controlar al 
hypervisor y la ejecución de otros dominios. Se conocen a los otros dominios 
como domU. En otras palabras, desde el punto de vista del usuario, el dom0 es el 
«anfitrión» de los demás sistemas de virtualización, mientras que los domU son 
sus «huéspedes» (las maquinas virtuales).

Virtualización vs Paravirtualización
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Xen no solo permite utilizar la tecnica de paravirtualización, sino que también 
nos ofrece otra técnica denominada HVM o 'Full Virtualization', es decir,
virtualización completa, que consiste en la instalación de un domU como si
fuera un host independiente.

El usar HVM tiene la ventaja de que se puede virtualizar casi cualquier cosa, 
incluído Windows, ya que no es necesario tener un kernel especial para la 
virtualización, pero su principal desventaja es el rendimiento, ya que ciertos 
componentes deben ser emulados, es decir, se debe realizar una traducción 
entre los pedidos de las maquinas virtuales y el hardware real. 

Otra desventaja de HVM, es que necesitamos que el procesador soporte el conjunto 
de instrucción de virtualización. Estas instrucciones en plataformas Intel son 
llamadas *Intel-VT* y en plataformas AMD se llaman *AMD-V*. Cabe destacar que 
en la actualidad, la mayor parte del hardware disponible ya las incluye.

KVM
---

KVM, acrónimo de máquina virtual basada en el núcleo («Kernel-based Virtual 
Machine»), es primero que nada un módulo del núcleo que provee la mayor parte 
de la infraestructura que puede usar un virtualizador, pero no es un 
virtualizador en sí mismo. El control real de la virtualización es gestionado 
por una aplicación basada en QEMU. 

A diferencia de otros sistemas de virtualización, se integró KVM al núcleo 
Linux desde el comienzo. Sus desarrolladores eligieron aprovechar el conjunto 
de instrucciones de procesador dedicados a la virtualización (Intel-VT y AMD-V), 
lo que mantiene a KVM liviano, elegante y no muy hambriento de recursos. 

La contraparte, obviamente, es que KVM no funciona en ordenadores con 
procesadores distintos a estos. 

La técnica utilizada por KVM es la full-virtualización, por lo que cualquier 
sistema operativo puede ejecutarse sobre el mismo, dado que no se requiere la 
modificación de los mismos para su ejecución en un entorno virtualizado. Esto ha 
dado a KVM cierto prestigio, dada su flexibilidad.


LXC
--- 
LXC se basa en una técnica conocida como *Virtualización basada en Sistema 
Operativo*. No es estrictamente hablando un virtualizador, de hecho no 
posee un hypervisor, sino que mas bien es un sistema para aislar grupos de 
procesos entre sí aún cuando estos se ejecutan en el mismo equipo. Aprovecha un 
conjunto de evoluciones recientes del núcleo Linux, conocidos colectivamente 
como grupos de control («control groups»), mediante los que diferentes conjuntos 
de procesos llamados «grupos» tienen diferentes visiones de ciertos aspectos de 
todo el sistema. 

Entre estos aspectos, los más notables son los identificadores de procesos, 
la configuración de red y los puntos de montaje. 

Un grupo de procesos aislados no podrá acceder a otros procesos en el sistema y 
puede restringir su acceso al sistema de archivos a un subconjunto específico. 
También puede tener su propia interfaz de red y tabla de enrutamiento y se puede 
configurar para que sólo pueda ver un subconjunto de los dispositivos 
disponibles que están presentes en el sistema.   

Puede combinar estas funcionalidades para aislar una familia de procesos 
completa que inicia desde el proceso init, y el conjunto resultante es muy 
similar a una máquina virtual. El nombre oficial de esta configuración es 
«contenedor» (de allí LXC: contenedores Linux, «LinuX Containers»), pero una 
diferencia importante con máquinas virtuales «reales» como aquellas provistas 
por Xen o KVM es que no hay un segundo núcleo; el contenedor utiliza el mismo 
núcleo que el sistema anfitrión. Esto tiene tanto ventajas como desventajas: 
las ventajas incluyen un rendimiento excelente debido a una falta completa de 
sobrecarga y el hecho de que el núcleo tiene una visión global de todos los 
procesos que ejecutan en el sistema por lo que la gestión de procesos puede 
ser más eficiente que si existieran dos núcleos independientes administrando 
conjuntos de tareas. La mayor de las desventajas es la imposibilidad de ejecutar 
un núcleo diferente en un contenedor (sea una versión diferente de Linux o 
directamente un sistema operativo distinto).

Diferencias entre las diferentes técnicas de virtualización
-----------------------------------------------------------

A continuación se muestra gráficamente las diferencias entre las diferentes 
técnicas.

.. figure:: imagenes/unidad05/image_3.png
   :alt: TOP
   :align: center
   :scale: 60 %

   Fig.3 Diferencias entre las diferentes técnicas de virtualización

Ejemplo práctico
----------------

A continuación veremos como instalar y crear maquinas virtuales con XEN. La idea 
de esta práctica es tener una noción mínima desde el punto de vista funcional 
de esta herramienta que nos permite crear tanto instancias (maquinas virtuales) 
full-virtualizadas como paravirtualizadas. También veremos como crear contenedores 
con LXC.

Maquinas virtuales con XEN
~~~~~~~~~~~~~~~~~~~~~~~~~~


Utilizar Xen en Debian requiere tres componentes:

- El hypervisor en sí mismo. 

- Un núcleo que ejecuta sobre dicho hypervisor. 

- Si se trata de la arquitectura i386 también se necesita una biblioteca estándar 
  con los parches apropiados para aprovechar Xen; la que se encuentra en el 
  paquete libc6-xen.


Para poder evitar la molestia de seleccionar estos componentes a mano, tiene 
disponibles varios metapaquetes para cada arquitectura. En el caso de la 
arquitectura amd64, este se instala de la de la siguiente manera

.. code:: bash

    root@servidor:~# apt-get install xen-linux-system-amd64


Este metapaquete incluye una combinación de paquetes del núcleo e hypervisor que 
funcionan bien en Debian Jessie. El hypervisor también incluirá xen-utils-4.4, 
que contiene las herramientas para controlar el hypervisor desde el dom0. A su 
vez, éste incluirá la biblioteca estándar apropiada. 

Durante la instalación de todo esto, los scripts de configuración también 
crearán un nuevo elemento en el menú del gestor de arranque Grub para iniciar 
el núcleo elegido en un dom0 Xen. Sin embargo generalmente éste no será el 
primero en la lista y, por lo tanto, no estará seleccionado de forma 
predeterminada. Si este no es el comportamiento que desea, ejecutar lo 
siguiente lo cambiará:

.. code:: bash

    root@servidor:~# mv /etc/grub.d/20_linux_xen /etc/grub.d/09_linux_xen
    root@servidor:~# update-grub

El siguiente paso es probar el comportamiento del dom0 en sí mismo; esto incluye 
reiniciar para utilizar el hypervisor y núcleo Xen. El sistema debería iniciar 
como siempre, con unos pocos mensajes adicionales en la consola durante los 
primeros pasos de inicialización.

Ahora es el momento de instalar sistemas útiles en los sistemas domU, utilizando 
las herramientas en xen-tools. Este paquete provee el programa **xen-create-image**, 
que automatiza en gran parte esta tarea. El único parámetro obligatorio es 
**--hostname**, que le da un nombre al domU; otras opciones son importantes, pero 
puede guardarlas en el archivo de configuración /etc/xen-tools/xen-tools.conf y 
si no las especificamos no se generará ningún error. Por lo tanto es importante 
revisar el contenido de este archivo antes de crear imágenes o utilizar los 
parámetros adicionales en la invocación de xen-create-image. Los parámetros 
importantes a saber incluyen los siguientes:

- **--memory** para especificar la cantidad de RAM dedicada a este nuevo sistema 
  creado (cabe aclarar que al momento de iniciar la maquina virtual,  esta 
  memoria será tomada de la memoria principal del host).

- **--size** y **--swap** para definir el tamaño de los «discos virtuales» 
  disponibles al domU; 

- **--debootstrap** para causar que se instale el nuevo sistema con debootstrap; 
  en tal caso, generalmente también utilizará la opción --dist (con el nombre de 
  una distribución como jessie).

- **--dhcp** indica que el domU debe obtener su configuración de red a través de 
  DHCP, mientras que **--ip** permite definir una dirección IP estática.

Por último, se debe elegir un método de almacenamiento para las imágenes a crear 
(que el domU verá como discos duros). El método más simple, que corresponde a la 
opción **--dir**, es crear un archivo en el dom0 para cada dispositivo que se le 
provee al domU. La alternativa en sistemas que utilizan LVM es la opción **--lvm** 
seguida del nombre de un grupo de volúmenes; xen-create-image luego creará un 
nuevo volumen lógico dentro de dicho grupo y éste estará disponible en el domU 
como un disco duro.

Finalmente podemos crear una maquina virtual con el siguiente comando:

.. code:: bash

	root@servidor:~# xen-create-image --hostname testxen --dhcp --dir /srv/testxen --size=2G --dist=jessie --role=udev

	[…]
	General Information
	--------------------
	Hostname       :  testxen
	Distribution   :  jessie
	Mirror         :  http://ftp.debian.org/debian/
	Partitions     :  swap            128Mb (swap)
		          /               2G    (ext3)
	Image type     :  sparse
	Memory size    :  128Mb
	Kernel path    :  /boot/vmlinuz-3.16.0-4-amd64
	Initrd path    :  /boot/initrd.img-3.16.0-4-amd64
	[…]
	Logfile produced at:
		 /var/log/xen-tools/testxen.log

	Installation Summary
	---------------------
	Hostname        :  testxen
	Distribution    :  jessie
	MAC Address     :  00:16:3E:8E:67:5C
	IP-Address(es)  :  dynamic
	RSA Fingerprint :  0a:6e:71:98:95:46:64:ec:80:37:63:18:73:04:dd:2b
	Root Password   :  adaX2jyRHNuWm8BDJS7PcEJ

Ahora tenemos una máquina virtual, pero no está ejecutando (por lo tanto sólo 
utiliza espacio en el disco duro del dom0). Por supuesto, podemos crear más 
imágenes, posiblemente con diferentes parámetros.

Antes de encender estas máquinas virtuales, necesitamos definir cómo accederemos 
a ellas. Por supuesto, podemos considerarlas máquinas aisladas a las que sólo 
podemos acceder a través de su consola de sistema, pero rara vez esto pueda ser 
muy útil. La mayoría de las veces, consideraremos un domU como un servidor 
remoto al que sólo podemos acceder a través de la red. Sin embargo, 
sería un gran inconveniente agregar una tarjeta de red para cada domU; es por 
esto que Xen permite crear interfaces virtuales que cada dominio puede ver y 
utilizar de la forma estándar. Estas tarjetas, aunque sean virtuales, sólo serán 
útiles cuando estén conectadas a una red, inclusive una virtual. Xen tiene varios 
modelos de red para esto:

- El modelo más simple es el modelo puente («bridge»); todas las tarjetas de red 
  eth0 (tanto en los sistemas domU como en el dom0) se comportarán como si 
  estuvieran conectadas directamente a un switch Ethernet.

- Luego está el modelo enrutamiento («routing») en el que el dom0 se comporta 
  como el router entre los sistemas domU y la red (física) externa.

- Finalmente, en el modelo NAT, nuevamente el dom0 se encuentra entre los 
  sistemas domU y el resto de la red, pero no se puede acceder a los sistemas 
  domU directamente desde afuera y el tráfico atraviesa una traducción de 
  direcciones de red en el dom0.

Estos tres modos de red involucran una cantidad de interfaces con nombres 
inusuales, como vif*, veth*, peth* y xenbr0. El hypervisor Xen los acomoda en la 
distribución definida bajo el control de las herramientas en espacio de usuario. 
Debido a que los modelos NAT y de enrutamiento sólo se adaptan a casos 
particulares nos concentraremos en el modelo de puente (bridge).

La configuración estándar de los paquetes Xen no modifica la configuración de 
red del sistema. Sin embargo, se configura el demonio xend para integrar las 
interfaces de red virtuales en un puente de red preexistente (xenbr0 tiene 
precedencia si existen varios de ellos). Por lo tanto, debemos configurar un 
puente en /etc/network/interfaces (lo que requiere que instalemos el paquete 
bridge-utils, razón por la que lo recomienda el paquete xen-utils-4.4) para 
reemplazar el elemento eth0 existente:

.. code:: bash
	auto xenbr0
	iface xenbr0 inet dhcp
	    bridge_ports eth0
	    bridge_maxwait 0

Esto asocia la interfaz física eth0 al puente xenbr0, luego cada maquina virtual 
que se cree se asociara a una interfaz en este puente, por lo que la misma estara 
conectada a un switch virtual en el que también esta conectada la interfaz eth0.

Luego de reiniciar para asegurarse que se crea el puente automáticamente, podemos 
iniciar el domU con las herramientas de control de Xen, en particular el programa 
xl. Este programa permite varias manipulaciones de los dominios, entre ellas: 
enumerarlos, iniciarlos y detenerlos.

.. code:: bash
	root@servidor:~# xl list
	Name                                        ID   Mem VCPUs      State   Time(s)
	Domain-0                                     0   463     1     r-----      9.8
	# xl create /etc/xen/testxen.cfg
	Parsing config from /etc/xen/testxen.cfg
	# xl list
	Name                                        ID   Mem VCPUs      State   Time(s)
	Domain-0                                     0   366     1     r-----     11.4
	testxen                                      1   128     1     -b----      1.1


Listo, nuestra máquina virtual está iniciando. Podemos acceder a ella de dos 
formas. La forma usual es conectarnos «remotamente» a través de la red, como lo 
haríamos con una máquina real; esto usualmente requerirá configurar un servidor 
DHCP o alguna configuración de DNS. La otra forma, que puede ser la única forma 
si la configuración de red era incorrecta, es utilizar la consola hvc0 ejecutando 
xl console:

.. code:: bash
	root@servidor:~# xl console testxen
	[…]

	Debian GNU/Linux 8 testxen hvc0

	testxen login: 

Uno puede abrir una sesión, tal como si estuviera sentado frente al teclado de 
la máquina virtual. Puede desconectarse de esta consola con la combinación de 
teclas Control+].

Una vez que el domU está ejecutando, puede utilizarlo como cualquier otro servidor 
(al fin y al cabo es un sistema GNU/Linux). Sin embargo, su existencia como 
máquina virtual permite cierta funcionalidad adicional. Por ejemplo, puede pausar 
y resumir temporalmente un domU, ejecutando xl pause y xl unpause. Sepa que aunque 
un domU pausado no utiliza el procesador, la memoria reservada a él sigue en uso. 
Puede ser interesante considerar las órdenes xl save y xl restore: guardar un 
domU libera los recursos utilizados por este domU, incluyendo la RAM. Cuando 
restaure (o resuma) un domU, éste no notará nada a excepción del paso del tiempo. 
Si un domU está ejecutando cuando se apague el dom0, los scripts empaquetados 
automáticamente guardarán el domU y lo restaurarán cuando vuelva a iniciar. Esto, 
por supuesto, tiene los mismos inconvenientes estándar que cuando hiberna un 
equipo portátil, por ejemplo; en particular, si se suspende por demasiado tiempo 
al domU, pueden expirar las conexiones de red. Sepa también que, hasta el momento, 
Xen es incompatible con gran parte de la gestión de energía ACPI, lo que evita 
que pueda suspender el sistema anfitrión (dom0).

Contenedores con LXC
~~~~~~~~~~~~~~~~~~~~

El primer paso será instalar LXC y otros paquetes necesarios

.. code:: bash
	root@servidor:~# apt-get install lxc rsync debootstrap

A continuación, y al igual que se hizo en el caso de XEN, debemos configurar 
la red para que trabaje en modo bridge. A este bridge conectaremos los diferentes 
contenedores, los cuales tendrán una interfaz virtual.

Para gestionar los bridge necesitamos del paquete bridge-utils

.. code:: bash
	root@servidor:~# apt-get install bridge-utils

Al igual que en el caso de anterior, debemos definir un bridge y asociar la 
interfaz física a dicho bridge. Editamos para esto el archivo 
**/etc/network/interface**

.. code:: bash
	#auto eth0
	#iface eth0 inet dhcp

	auto br0
	iface br0 inet dhcp
	  bridge-ports eth0

Configuremos ahora el sistema que utilizará el contenedor. Debido a que esta 
«máquina virtual» no ejecutará directamente sobre el hardware, son necesarios 
algunos ajustes comparados con un sistema de archivos estándar, especialmente 
en aquello que involucra al núcleo, los dispositivos y las consolas. Afortunadamente, 
el paquete lxc incluye scripts que automatizan la mayoría de esta configuración. 
Por ejemplo, las siguientes órdenes instalará un contenedor Debian:

.. code:: bash
	root@servidor:~# lxc-create -n testlxc -t debian
	debootstrap is /usr/sbin/debootstrap
	Checking cache download in /var/cache/lxc/debian/rootfs-jessie-amd64 …
	Downloading debian minimal ...
	I: Retrieving Release 
	I: Retrieving Release.gpg 
	[…]
	Download complete.
	Copying rootfs to /var/lib/lxc/testlxc/rootfs…
	[…]
	Root password is 'n6D4nsZT', please change !
	root@servidor:~# 

Inicialmente se crea el sistema de archivos en /var/cache/lxc y luego 
es mudado a su directorio de destino. Esto permite crear contenedores idénticos 
mucho más rápido ya que luego sólo necesita copiarlo.

El script de creación de plantillas acepta la opción --arch para especificar la 
arquitectura del sistema a instalar y la opción --release si desea instalar algo 
diferente a la versión estable actual de Debian. 

El sistema de archivos recientemente creado ahora contiene un sistema Debian 
mínimo y, de forma predeterminada, el contenedor no tendrá interfaz de red 
(con el permiso de la interfaz local de loopback). Debido a que esta no es la 
configuración deseada, editaremos el archivo de configuración del contenedor 
(/var/lib/lxc/testlxc/config) y agregar algunos elementos lxc.network.*:

.. code:: bash
	lxc.network.type = veth
	lxc.network.flags = up
	lxc.network.link = br0
	lxc.network.hwaddr = 4a:49:43:49:79:20

Estas líneas significan, respectivamente, que se creará una interfaz virtual en 
el contenedor; que será iniciada automáticamente cuando inicie el contenedor; 
que será conectada automáticamente al puente br0 en el anfitrión; y que su 
dirección MAC será la especificada. En caso que esta última línea no exista o 
esté desactivada, se generará una dirección MAC aleatoria.

Otro elemento útil en dicho archivo es la configuración del nombre del equipo:

.. code:: bash
	lxc.utsname = testlxc

Ahora que nuestra máquina virtual está lista, iniciemos el contenedor:	

.. code:: bash
	root@servidor:~# lxc-start --daemon --name=testlxc
	root@servidor:~# lxc-console -n testlxc
	Debian GNU/Linux 8 testlxc tty1

	testlxc login: root
	Password: 
	Linux testlxc 3.16.0-4-amd64 #1 SMP Debian 3.16.7-ckt11-1 (2015-05-24) x86_64

	The programs included with the Debian GNU/Linux system are free software;
	the exact distribution terms for each program are described in the
	individual files in /usr/share/doc/*/copyright.

	Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
	permitted by applicable law.
	root@testlxc:~# 

Ahora estamos dentro del contenedor; nuestro acceso a los procesos está 
restringido a aquellos iniciados dentro del mismo contenedor y nuestro acceso 
al sistema de archivos está limitado de forma similar al subconjunto dedicado 
del sistema de archivos completo (/var/lib/lxc/testlxc/rootfs). Podemos salir a 
la consola con *Control+a+q*.

Tenga en cuenta que ejecutamos el contenedor como un proceso en segundo plano 
gracias a la opción --daemon de lxc-start. Podemos interrumpir el contenedor 
ejecutando **lxc-stop --name=testlxc**.

El paquete lxc contiene un script de inicialización que puede automatizar el 
inicio de uno o más contenedores cuando el sistema principal arranca 
(confía en el comando lxc-autostart el cual inicia los contenedores que tienen 
la opción lxc.start.auto configurada a 1). Se puede obtener un control más 
detallado del orden de inicio con lxc.start.order y lxc.group: por defecto, el 
script de inicialización inicia los contenedores que son parte del grupo onboot 
y luego los contenedores que no forman parte de este grupo. En ambos casos el 
orden dentro de un grupo es definido por la opción lxc.start.order.


.. image:: caratulas/unidad06.pdf
	:scale: 86%

Unidad 6: Administración Centralizada
=====================================

Introducción
------------
Un administrador de sistemas se encuentra por lo general con
decenas, cientos y hasta incluso miles de hosts a administrar. Llevar adelante
esta tarea de forma manual es práctimanete imposible.

Pensemos por un momento lo que implicaría agregar en una infraestructura de
100 ó más instancias virtuales, a un nuevo administrador de sistemas que se
a sumado al equipo de trabajo. Tendríamos que ingresar uno por uno a todos los
hosts y agregar el usuario, permitirle el acceso ssh, permitir que ejecute
comandos con sudo, etc; ó imaginemos que tenemos que realizar el deploy de una
nueva aplicación que involucra cambios en varios servidores web, en
redirectores HTTP y varios motores de bases de datos: tendríamos que ir uno por
uno realizando las modificaciones necesarias en estos equipos, con el riesgo de
cometer errores y por sobre todo con un gran tiempo offline del servicio. Si a
esto le sumamos que todas estas tareas a su vez previamente deben ser testeadas
en un entorno de testing ó stagging, o a veces incluso ambos; prácticamente
estaríamos perdiendo todo el pelo en un solo cambio.

Infraestructuras como la mencionada anteriormente son muy comunes, convirtiendo
a la automatización en algo totalmente indispensable.

Administración centralizada
---------------------------
La Administración Centralizada, o también conocida como administración de
configuración (Configuration management) consiste en administrar los cambios en
la configuración de un sistema utilizando un método definido, manteniendo así
el sistema y su integridad a lo largo del tiempo. Inicialmente fue concebida para
documentar las configuraciones en los sistemas, es decir, saber como estaba
cada sistema configurado. Con el paso del tiempo y la evolución de diferentes
herramientas, se convirtió en un modo de definir el estado deseado de un sistema.
Podemos definir "como" debe estar configurado un sistema y luego son estas
herramientas las que se encargan automáticamente de garantizar que dicho sistema
se mantengan de ese modo, aplicando los cambios necesarios en su configuración.
Por ejemplo, podemos definir que tal directorio debe tener tales permisos, o que
debe tener instalado tales paquetes de software y otros no pueden estar
instalados, ó que el sistema debe tener los usuarios administradores definidos, etc.

Infraestructura como código
---------------------------
Si definimos el conjunto de las configuraciones desde un equipo central, definiendo
recetas que especifican como deben estar configurados los diferentes nodos de
nuestra infraestructura, en lugar de ingresar a los nodos y configurarlos,
estamos en presencia de lo que se conoce como "Infraestructura como código"
(Infraestructure as code). Al tratarse de código (recetas), podemos tener la
definición de toda una infraestructura en un repositorio bajo control de versión
(svn, git, bazzar, etc), lo que ofrece una potencia muy grande, y una versatilidad
mayor aún, agilizando enormemente las tareas diarias de administración. En este
contexto, solo tenemos que limitarnos a aplicar cambios en las recetas, y
dejar que luego una herramienta se encargue de la configuración de los nodos,
algo muy conveniente cuando el número de hosts a administrar es muy grande.

Si bien existen un montón de herramientas que pueden ayudarnos a llevar adelante
esta tarea (por ejemplo Puppet, CFEngine, Chef entre otros), en esta unidad nos
centraremos en  Ansible, una herramienta muy completa que se ha vuelto muy popular
recientemente y que ofrece y promete mucho.

Ansible
-------
Ansible nació en el año 2012 como un proyecto menor en github, y rápidamente fue
captando adeptos y colaboradores, que lo convirtieron en una de las herramientas
de automatización y administración mas potentes. La misma combina instalación
multi-nodo, ejecuciones de tareas ad hoc y administración de configuraciones.
Maneja nodos a través de SSH y no requiere ningún software remoto adicional, es
decir es agentless, dado que no requiere la instalación de ningún agente en los
nodos a administrar, una de las principales ventajas respecto de otras
herramientas similares. Dispone de módulos que trabajan sobre JSON y la salida
estándar puede ser escrita en cualquier lenguaje. Utiliza YAML para describir
las recetas de configuraciones reusables. La plataforma fue creada por Michael
DeHaan, también autor de la aplicación de aprovisionamiento Cobbler y co-autor
del framework para administración remota Func.

Conceptos
~~~~~~~~~

Playbooks
~~~~~~~~~
Las recentas en Ansible son escritas en lo que se conoce como playbooks(ó libros
de jugadas). Estos son archivos YAML de lectura sencilla tanto para humanos como
para máquinas. En los playbooks se utiliza una sintaxis muy sencilla para
describir que es lo que Ansible debe realizar con tal o cual nodo o conjunto de
nodos.

Empecemos con un ejemplo. En primer lugar le diremos a Ansible donde se debe
aplicar la receta (el playbook). Esto se especifica con la sentencia hosts:

.. code:: json

	---
	- hosts: all

Esto quiere decir, en todos los host definidos en el inventario. El inventario
en Ansible es un archivo en texto plano que define sobre que nodos
se trabajará. Este archivo entre otras cosas permite definir grupos
de host, variables, rangos de ips, etc. La ubicación por defecto de
este archivo es /etc/ansible/hosts, pero podemos definir un archivo cualquiera y
luego pasarle por parámetros el mismo en la ejecución del playbook.
Un ejemplo de inventario sería el siguiente

.. code:: json

	nodo1.intranet

	[servidoresWeb]
	nodo2.intranet

	[servidoresDB]
	nodo3.intranet
	nodo2.intranet

Este archivo cuenta con 3 hosts, y dos grupos. Como se puede observar, un host
puede pertenecer a mas de un grupo, y no todos los host deben pertenecer a uno.
La sentencia anterior definida en el playbook (host: all) aplica sobre todos los
nodos definidos en el inventario, es decir sobre nodo1, nodo2 y nodo3 en este
caso. Es muy importante aclarar que Ansible trabaja utilizando ssh, motivo por el
cual es necesario previamente realizar un intercambio de claves, de modo de evitar
tener que ingresar la password cada vez. Por otro lado, la conexión hacia el nodo
se realizará utilizando el usuario que ejecuta el comando  Ansible, como veremos
más adelante, y utilizará los privilegios del mismo para realizar las tareas en
el nodo, por lo que es recomendable que dicho usuario tenga permisos de sudo en el
host.
Para realizar el intercambio de claves, solo debemos ejecutar el comando
ssh-copy-id de la siguiente manera

.. code:: bash

	usuario@mipc:~$ ssh-copy-id nodo1.intranet
	/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
	/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
	usuario@nodo1.intranet's password:

	Number of key(s) added: 1

	Now try logging into the machine, with:   "ssh 'nodo1.intranet'"
	and check to make sure that only the key(s) you wanted were added.


Ahora que Ansible sabe donde debe ejecutar la receta, y que hemos realizado
el intercambio de claves para conectarnos a los nodos, debemos decirle que es lo
que tiene que hacer, o garantizar. Esto se realiza mediante la sentencia task
(tareas), donde definimos de manera secuencial que es lo que debe garantizarse. Y
decimos garantizarse en vez de ejecutarse, porque Ansible se asegurara de que lo
definido se cumpla, y luego decidirá si debe ejecutar una acción o no. Para esto
Ansible se basa en los denominados "Módulos", que sirven para
diferentes tareas como testear conexiones, instalar software, setear permisos,
configuar servicios, etc, etc, etc. Hablaremos de ellos un poco más adelante.

Sigamos con nuestro playbook, y definamos como primer tarea que se debe garantizar
conectividad a los nodos, es decir, Ansible debe ser capaz de conectarse a los
mismo (de momento solo nodo1.intranet). Para ello utilizaremos el módulo "ping"
de la siguiente manera:

.. code:: json

	---
	- hosts: all
	  tasks:
	  - name: Garantizar conectividad al nodo
	    ping:

La ejecución de este playbook se realiza por medio del comando ansible-playbook,
especificando el playbook en cuestión y el inventario

.. code:: bash

	usuario@mipc:~$ ansible-playbook -i inventario playbook.yml

El comando anterior debería mostrar una salida como la siguiente

.. code:: bash

	usuario@mipc:~$ ansible-playbook -i inventario playbook.yml

	PLAY ***************************************************************************

	TASK [setup] *******************************************************************
	ok: [nodo1.intranet]

	TASK [Garantizar conectividad a los nodos] *************************************
	ok: [nodo1.intranet]

	PLAY RECAP *********************************************************************
	nodo1.intranet             : ok=2    changed=0    unreachable=0    failed=0

En este caso vemos que la tarea "Garantizar conectividad a los nodos" se ejecuto
correctamente. Incluso al final, en la sección PLAY RECAP (resumen de la jugada),
se muestra un resumen de las tareas que se ejecutaron, sobre que nodos y el
resultado obtenido.

Habrán notado que hemos definido 1 sola tarea, pero se han ejecutado
2. Esto es debido a que Ansible siempre ejecuta de manera inicial la tarea "setup",
que como veremos más adelante, se encarga de relevar información del nodo y definir
ciertas variables propias del mismo, las que luego pueden utilizarse en el playbook...
pero no nos adelantemos, que aún falta.

De momento no tiene demasiado valor agregado saber que nos podemos conectar, pero
es un primer paso. Hagamos algo un poco mas interesante, instalemos mediante ansible
el Servidor Web Apache. Para esto modifiquemos el playbook de la siguiente manera,
y agreguemos tareas que se encarguen de garantizar que Apache este instalado y
corriendo. Utilizaremos 2 módulos nuevos, apt y service. El primero se encarga
de instalar paquetes y el segundo se encarga de garantizar el estado de un servicio

.. code:: bash

	 ---
	 - hosts: all
	   tasks:
	   - name: Garantizar conectividad a los nodos
	     ping:

	   - name: Instalar el servidor Web Apache
	     become: true
	     apt:
	       name: apache2
	       state: present

	   - name: Garantizar que el servidor Web Apache este corriendo
	     become: true
	     service:
	       name: apache2
	       state: started

Come verán, los modulos apt y services, a diferencia del móduloping, aceptan
parámetros, los cuales nos permiten definir, para el caso de apt, que paquete y
que estado debe tener en el nodo (present, latest, absent), o que servicio debe estar
en que estado (started, stopped), para el caso del móduloservice. Cada módulo
tiene sus propios parámetros y se cuenta con una documentación muy amplia en
http://docs.ansible.com/ .

Volviendo al playbook, notarán que hemos agregado la opción become a las
tareas "Instalar el servidor Web Apache" y "Garantizar que el servidor Web Apache
este corriendo", esto se debe a que dichas tareas necesitan permisos de root para
su ejecución. En este caso, la opción become convierte al usuario en super-usuario
utilizando sudo.
Para ejecutar el playbook debemos tener en cuenta que debemos ingresar la
contraseña del usaurio para que pueda hacer sudo(recordemos que Ansible se
conecta al nodo con el usuario que esta corriendo el comando ansible-playbook),
para ello agregamos el parámetro "--ask-sudo-pass" al comando ansible-playbook

.. code:: bash

	usuario@mipc:~$ ansible-playbook --ask-sudo-pass -i inventario playbook.yml
	SUDO password:

	PLAY ***************************************************************************

	TASK [setup] *******************************************************************
	ok: [nodo1.intranet]

	TASK [Garantizar conectividad a los nodos] *************************************
	ok: [nodo1.intranet]

	TASK [Instalar el servidor Web Apache] *****************************************
	changed: [nodo1.intranet]

	TASK [Garantizar que el servidor Web Apache este corriendo] ********************
	ok: [nodo1.intranet]

	PLAY RECAP *********************************************************************
	nodo1.intranet             : ok=4    changed=1    unreachable=0    failed=0

Perfecto, ya tenemos Apache instalado y corriendo en el nodo. Pero que pasa si
ejecutamos nuevamente el playbook?, veamos...

.. code:: bash

	usuario@mipc:~$ ansible-playbook --ask-sudo-pass -i inventario playbook.yml
	SUDO password:

	PLAY ***************************************************************************

	TASK [setup] *******************************************************************
	ok: [nodo1.intranet]

	TASK [Garantizar conectividad a los nodos] *************************************
	ok: [nodo1.intranet]

	TASK [Instalar el servidor Web Apache] *****************************************
	ok: [nodo1.intranet]

	TASK [Garantizar que el servidor Web Apache este corriendo] ********************
	ok: [nodo1.intranet]

	PLAY RECAP *********************************************************************
	nodo1.intranet             : ok=4    changed=0    unreachable=0    failed=0

Todas las tareas devuelven "ok", y ninguna esta marcada como "changed". Es decir
Ansible no tuvo que hacer nada porque todas las condiciones que definimos en el
playbook se cumplen, solo verificó que se cumpliera lo definido. Esto se denomina
**Idempotencia**, y es una propiedad que tienen los módulos en  Ansible, solo aplican
lo necesario para que se cumpla lo definido en la tarea. Esta es la principal
diferencia con un script de Bash, Phython ó Perl que podamos utilizar para
automatizar tareas. Por ejemplo, no se instalará un paquete que ya esta instalado,
o no se realizará un "start" de un servicio que ya se encuentra corriendo. Podemos
ejecutar N veces el playbook y siempre arrojara el mismo resultado, la condición
que hemos planteado en la receta.

Juguemos un poco más con el módulo apt, y ahora modifiquemos la receta para que
desinstale Apache, instale Nginx y MySQL

.. code:: bash

	---
	- hosts: all
	  tasks:
	  - name: Garantizar conectividad a los nodos
	    ping:
	  - name: Desinstalar el servidor web Apache
	    become: true
	    apt:
	      name: apache2
	      state: absent
	  - name: Instalar el servidor web Nginx
	    become: true
	    apt:
	      name: nginx
	      state: present
	      update-cache: yes
	  - name: Instalar el servidor de bases de datos MySQL
	    become: true
	    apt:
	      name: mysql-server-5.6
	      state: present
	      update-cache: yes

	  - name: Garantizar que el servidor web Nginx este corriendo
	    become: true
	    service:
	      name: nginx
	      state: started
	  - name:  Garantizar que el servidor de Bases de Datos MySQL esten corriendo
	    become: true
	    service:
	      name: mysql
	      state: started

Si ejecutamos el playbook vemos como se desinstala Apache e instalan los paquetes
seleccionados

.. code:: bash

	usuario@mipc:~$ ansible-playbook --ask-sudo-pass -i inventario playbook.yml
	SUDO password:

	PLAY ***************************************************************************

	TASK [setup] *******************************************************************
	ok: [nodo1.intranet]

	TASK [Garantizar conectividad a los nodos] *************************************
	ok: [nodo1.intranet]

	TASK [Desinstalar el servidor Web Apache] **************************************
	changed: [nodo1.intranet]

	TASK [Instalar el servidor web nginx] ******************************************
	changed: [nodo1.intranet]

	TASK [Instalar el servidor de Bases de Datos MySQL] ****************************
	changed: [nodo1.intranet]

	TASK [Garantizar que el servidor Web Nginx esten corriendo] ********************
	ok: [nodo1.intranet]

	TASK [Garantizar que el servidor de Bases de Datos MySQL esten corriendo] ******
	ok: [nodo1.intranet]

	PLAY RECAP *********************************************************************
	nodo1.intranet             : ok=7    changed=3    unreachable=0    failed=0

Para emprolijar un poco mas nuestra receta, podemos agrupar todas las tareas
comunes, mediante el uso de *loops*, y sacar el uso del móduloping, dado que con
solo ejecutar Ansible sabremos si hay conectividad o no. El playbook quedaría
simplificado de la siguiente manera:

.. code:: bash

	---
	- hosts: all
	  tasks:
	  - name: Desinstalar el servidor Web Apache
	    become: true
	    apt:
	      name: apache2
	      state: absent

	  - name: Instalar Nginx y MySQL
	    become: true
	    apt:
	      name: "{{ item }}"
	      state: present
	      update-cache: yes
	      with_items:
	        - nginx
	        - mysql-server-5.6

	  - name: Garantizar que Nginx y MySQL esten corriendo
	    become: true
	    service:
	      name: "{{ item }}"
	      state: started
	      with_items:
	        - nginx
	        - mysql

Este playbook es el mismo que el anterior, pero simplificado.
Si ha prestado atención, notará que hemos agregado también el parámetro
*update-cache: yes* en el módulo apt. Investigue sobre el mismo en
http://docs.ansible.com/ansible/apt_module.html

Inventario
^^^^^^^^^^
Hemos hablado un poco ya del inventario, pero es importante entender sus
características y como organizarlo.
El inventario es una parte importante de toda herramienta de administración
centralizada, dado que nos permite definir en donde se deben ejecutar las tareas.
Vimos que Ansible por defecto lee el inventario de /etc/ansible/hosts, pero también
podemos optar por definirlo en cualquier archivo e invocarlo mediante la opción
-i del comando ansible-playbook.

El inventario esta escrito en formato INI, pero también puede ser escrito en
formato JSON, utilizandose este último cuando el mismo se genera de forma
dinámica. Nosotros utilizaremos el formato INI.

Podemos listar a los nodos por su nombre de DNS ó su IP, e incluso podemos
utilizar rangos tanto de nombres como de IPs. Por ejemplo

.. code:: bash

	nodo1.intranet
	nodo2.intranet
	nodo[3:5].intranet
	192.168.1.201
	192.168.1.[202:2:210]
	192.168.1.100:2222

Los primeros 3 son definiciones utilizando el nombre de DNS de los hosts, o lo
definido en el archivo /etc/hosts del equipo donde se ejecuta ansible. El tercero
utiliza rangos de Python, y es equivalente a

.. code:: bash

	nodo3.intranet
	nodo4.intranet
	nodo5.intranet

La 4ta opción es por medio de una dirección IP, y la 5ta es utilizando rangos con
un paso de 2, es decir, la 5ta opción equivale a

.. code:: bash

	192.168.1.202
	192.168.1.204
	192.168.1.206
	192.168.1.208
	192.168.1.210

Por útlimo, la 6ta opción es especificando la dirección IP y el puerto donde escucha
SSH (recordemos que Ansible se comunica con los diferentes hosts por este medio)

Estas son las formas mas comunes de representar a los hosts.
Otra característica importante que vimos es que se pueden utilizar grupos:

.. code:: bash

	[web_servers]
	nodo1.intranet

	[database_servers]
	nodo1.intranet
	nodo2.intranet

	[loadbalancers]
	nodo[4:5].intranet

	[servidores_internos:children]
	web_servers
	database_servers

La última opción, que hace uso de :childrens, nos permite especificar grupos de
grupos. Es decir tanto el grupo web_servers como el grupo database_servers
pertenecen al grupo servidores_internos.
Estos grupos son útiles para luego especificar donde debe correr nuestro playbook,
tal como veremos en breve.

Si nos detenemos a pensar un poco en el archivo de inventario, veremos que no solo
es útil para  Ansible, sino que también nos permite tener documentada nuestra
infraestructura, sabiendo de primera mano que host hace que cosa.

Ejemplo práctico
================

Tomemos el toro por las hasta y probemos hacer una receta con ejemplos de la vida
diaria de un sysadmin, y de este modo aprendamos haciendo.
En este ejemplo encaremos la instalación y configuración de un Wordpress
con su entorno. Para esto nos basaremos en los pasos recomendados por Debian
(https://wiki.debian.org/WordPress), pero convertiremos los mismos en una receta
de ansible.

Wordpress requiere que tengamos instalados PHP, ciertos paquetes extras de php,
un servidor de bases de datos y un servidor web, optaremos por Apache2 y MySQL.
Nuestro playbook empezaría así

.. code:: bash

	- hosts: all
	  tasks:
	  - name: Instalar Worpress y sus dependencias
	    become: true
	    apt:
	      name: "{{ item }}"
	      state: present
	      update-cache: yes
	    with_items:
	      - wordpress
	      - curl
	      - apache2
	      - mysql-server

Siempre es recomendable ir ejecutando Ansible a medida que vamos escribiendo la
receta. Por lo que ejecutemos esta misma receta sobre el nodo1.intranet que
veniamos utilizando, aunque podríamos aplicarlo sobre cualquier equipo, incluso
uno recién instalado, una maquina virtual.

.. code:: bash

	usuario@mipc:~$ ansible-playbook --ask-sudo-pass -i inventario playbook.yml
	SUDO password:

	PLAY ***************************************************************************

	TASK [setup] *******************************************************************
	ok: [nodo1.intranet]

	TASK [Instalar Worpress y sus dependencias] ************************************
	changed: [nodo1.intranet] => (item=[u'wordpress', u'curl', u'apache2', u'mysql-server'])

	TASK [Agregar usuarios] ********************************************************
	ok: [nodo1.intranet]

	PLAY RECAP *********************************************************************
	nodo1.intranet             : ok=3    changed=1    unreachable=0    failed=0

Ahora según la documentación de Debian, debemos crear un archivo
/etc/apache2/sites-available/wp.conf y poner allí lo siguiente

.. code:: bash

	Alias /wp/wp-content /var/lib/wordpress/wp-content
	Alias /wp /usr/share/wordpress
	<Directory /usr/share/wordpress>
	    Options FollowSymLinks
	    AllowOverride Limit Options FileInfo
	    DirectoryIndex index.php
	    Require all granted
	</Directory>
	<Directory /var/lib/wordpress/wp-content>
	    Options FollowSymLinks
	    Require all granted
	</Directory>

Para realizar esta tarea, haremos uso del módulo copy de  Ansible, que se encarga
de copiar archivos desde la ubicación donde ejecutamos  Ansible, a un directorio
en el equipo de destino. Para esto creamos primero una carpeta files y dentro ponemos
un archivo llamado wp.conf con este contenido. Paso siguiente, debemos anexar a
nuestro playbook lo siguiente

.. code:: bash

  - name: Crear sitio de Wordpress en Apache
    become: true
    copy:
      src: files/wp.conf
      dest: /etc/apache2/sites-available/wp.conf
      owner: root
      group: root
      mode: 0644

Si lo ejecutamos nuevamente al playbook, deberíamos ver el siguiente cambio

.. code:: bash

	TASK [Crear sitio de Wordpress en Apache] **************************
	changed: [nodo1.intranet]

El paso siguiente en la documentación es "habilitar" el sitio, y en la misma
se propone hacerlo por medio del comando a2ensite. Este comando lo que hace es
crear un enlace simbólico en /etc/apache2/sites-enabled que apunte a la configuración
en /etc/apache2/sites-available, por lo que sabiendo esto aprovecharemos y en nuestra
receta haremos que se cree dicho enlace simbólico. Obviamente que para esto también
hay un módulo, y se llama *file*. Este módulo permite setear permisos, configurar
enlaces, definir atributos y muchas opciones mas (ver http://docs.ansible.com/ansible/file_module.html para
mayor información). Ahora lo utilizaremos para crear este enlace simbólico.
Agreguemos lo siguiente al playbook

.. code:: bash

  - name: Habilitar sitio de Wordpress (crear enlace simbólico)
    become: true
    file:
      src: /etc/apache2/sites-available/wp.conf
      dest: /etc/apache2/sites-enabled/wp.conf
      owner: root
      group: root
      state: link

Ahora con crear el enlace no alcanza, necesitamos recargar Apache
para que tome la misma (service apache2 reload). Esto lo podríamos hacer con el
módulo *service* que hemos utilizado anteriormente, agregando lo siguiente al playbook

.. code:: bash

	- name: Recargar apache
		become: true
		service:
			name: apache2
			state: reload

Pero esto implicaría que cada vez que ejecutemos el playbook se recargue la
configuración de Apache. Lo ideal es que solo se recargue cuando agregamos la
configuración. Es en estos casos donde entra en juego otro concepto
muy potente de  Ansible, los "handlers" (manejadores). Los Handlers son tareas que
se ejecutan ante determinadas circunstancias, como por ejemplo en nuestro caso,
solo queremos que se recargue el Apache cuando se creo el enlace simbólico y no
cada vez que se ejecuta el playbook. Para definirlos, se utiliza una sintaxis muy
parecida a "task", es decir:

.. code:: bash

  handlers:
    - name: Recargar apache
      service:
        name: httpd
        state: reloaded

Luego en el playbook debemos definir que cuando se produzca un cambio en la tarea
de habilitación del sitio, se llame a este manejador. Esto se hace por medio de la
palabra reservada "notify" (reescribimos la tarea de "Habilitar
sitio de Wordpress (crear enlace simbólico)" ), el playbook completo hasta ahora
debería ser de la siguiente manera:

.. code:: bash

	---
	- hosts: all
		tasks:
		- name: Instalar Worpress y sus dependencias
			become: true
			apt:
				name: "{{ item }}"
				state: present
				update-cache: yes
			with_items:
				- wordpress
				- curl
				- apache2
				- mysql-server

		- name: Copiar configuración de Wordpress para apache2
			become: true
			copy:
				src: files/wp.conf
				dest: /etc/apache2/sites-available/wp.conf
				owner: root
				group: root
				mode: 0644

		- name: Habilitar sitio de Wordpress (crear enlace simbólico)
			become: true
			file:
				src: /etc/apache2/sites-available/wp.conf
				dest: /etc/apache2/sites-enabled/wp.conf
				owner: root
				group: root
				state: link
			notify:
				- Recargar apache

		handlers:
			- name: Recargar apache
				service:
					name: apache2
					state: reloaded

Si ahora ejecutamos el playbook veremos que se creará el enlace simbólico y se
ejecutará la tarea "Recargar apache"

.. code:: bash

	usuario@mipc:~$ ansible-playbook -i inventario playbook.yml -K
	SUDO password:

	PLAY ***************************************************************************

	TASK [setup] *******************************************************************
	ok: [nodo1.intranet]

	TASK [Instalar Worpress y sus dependencias] ************************************
	ok: [nodo1.intranet] => (item=[u'wordpress', u'curl', u'apache2', u'mysql-server'])

	TASK [Copiar configuración de Wordpress para apache2] **************************
	ok: [nodo1.intranet]

	TASK [Habilitar sitio de Wordpress (crear enlace simbólico)] *******************
	changed: [nodo1.intranet]

	RUNNING HANDLER [Recargar apache] **********************************************
	changed: [nodo1.intranet]

	PLAY RECAP *********************************************************************
	nodo1.intranet             : ok=5    changed=2    unreachable=0    failed=0


pero si lo ejecutamos nuevamente no.

.. code:: bash

	usuario@mipc:~$ ansible-playbook -i inventario playbook.yml -K
	SUDO password:

	PLAY ***************************************************************************

	TASK [setup] *******************************************************************
	ok: [nodo1.intranet]

	TASK [Instalar Worpress y sus dependencias] ************************************
	ok: [nodo1.intranet] => (item=[u'wordpress', u'curl', u'apache2', u'mysql-server'])

	TASK [Copiar configuración de Wordpress para apache2] **************************
	ok: [nodo1.intranet]

	TASK [Habilitar sitio de Wordpress (crear enlace simbólico)] *******************
	ok: [nodo1.intranet]

	PLAY RECAP *********************************************************************
	nodo1.intranet             : ok=4    changed=0    unreachable=0    failed=0


Con eso concluímos la configuración de apache. Ahora debemos avanzar con la
configuración de Wordpress y de MySQL.
Para la configuración de wordpress debemos crear un archivo en
/etc/wordpress/config-nodo1.intranet.php con ciertas variables a definir
como el nombre de usuario de conexión a la base de datos, la password, la dirección
de conexión al motor, etc. El archivo propuesto por la documentación de debian es el siguiente

.. code:: bash

	<?php
		define('DB_NAME', 'wordpress');
		define('DB_USER', 'wordpress');
		define('DB_PASSWORD', 'password');
		define('DB_HOST', 'localhost');
		define('WP_CONTENT_DIR', '/var/lib/wordpress/wp-content');
	?>

Es un buen momento para hablar de las variables en ansible. Las
variables en Ansible se pueden definir en muchos lugares, como el inventario,
el playbook o un directorio...inclusive se pueden pasar como parámetros en la
ejecución de comando ansible-playbook. Estas luego pueden invocarse desde el playbook
y permiten adaptar el comportamiento de las tareas, según el valor de las mismas.
Para nuestro caso, y por una cuestión de simplicidad las definiremos en el mismo
playbook, mediante la palabra clave vars:

.. code:: bash

	  vars:
	    wordpress_db_name: 'wordpress_db'
	    wordpress_db_user: 'wordpress'
	    wordpress_db_password: 'superpassword'
	    wordpress_db_host: 'localhost'
	    wordpress_content_dir: '/var/lib/wordpress/wp-content'

A continuación deberíamos generar el archivo antes mencionado, haciendo uso de
estas variables que hemos definido y utilizando para ello el módulo template.

El módulo "template" utiliza el motor de templates Jinja2 de python, y
permite, por medio de una sintaxis muy sencilla, generar un archivo en el destino
utilizando valores definidos en variables. Si no conoce Jinja2 no se preocupe,
el uso que le daremos aquí es muy simple, pero si le interesa aprender
más al respecto, no estaría mal visitar http://jinja.pocoo.org/. Para seguir con
nuestro ejemplo y crear el archivo de configuración de Wordpress, debemos primero
crear un archivo .j2, por ejemplo wordpress_config.j2 con el siguiente contenido

.. code:: bash

	<?php
	define('DB_NAME', '{{ wordpress_db_name }}');
	define('DB_USER', '{{ wordpress_db_user }}');
	define('DB_PASSWORD', '{{ wordpress_db_password }}');
	define('DB_HOST', '{{ wordpress_db_host }}');
	define('WP_CONTENT_DIR', '{{ wordpress_content_dir }}');
	?>

Como pueden ver, el archivo es el mismo, solo hemos reemplazado los valores, por
las variables que hemos definido en el playbook. La sintaxis para invocar a las
variables es la doble llave "{{ }}"

Luego necesitamos crear una tarea que utilice el módulo template y genere el
archivo deseado en /etc/wordpress/config-nodo1.intranet.php

.. code:: bash

		- template:
		    src: wordpress_config.j2
		    dest:  /etc/wordpress/config-nodo1.intranet.php
		    owner: www-data
		    group: www-data
		    mode: 0644

Perfecto, con esto ya hemos configurado wordpress, solo nos resta crear la Bases
de datos, el usuario y configurar permisos para el usuario definido en la
configuración de wordpress. Obviamente que la base de datos no la crearemos a mano,
sino que nos valdremos de módulos para que realicen dichas tareas. Los módulos a
utilizar en este caso son mysql_user y mysql_db. El primero permite crear y
configurar usuarios, y el segundo bases de datos. Su uso es muy sencillo, pero a
diferencia de los demás módulos, requieren que el host destino tenga instalado
el paquete python-mysqldb, por lo que modificamos la tarea
"Instalar Worpress y sus dependencias", y lo agregamos para que se instale ;) :

.. code:: bash

  - name: Instalar Worpress y sus dependencias
    become: true
    apt:
      name: "{{ item }}"
      state: present
      update-cache: yes
    with_items:
      - wordpress
      - curl
      - apache2
      - mysql-server
      - python-mysqldb

Ahora sí, agregamos las dos tareas restantes y concluimos con la creación del
usuario y de mysql y de su base de datos.

.. code:: bash

  - name: Crear usuario wordpress
    become: true
    mysql_user:
      name: '{{ wordpress_db_user}}'
      password: '{{ wordpress_db_password }}'
      priv: '*.*:ALL'
      state: present

  - name: Crear base de datos de wordpress
    become: true
    mysql_db:
      name: '{{ wordpress_db_name }}'
      login_user: '{{ wordpress_db_user }}'
      login_password: '{{ wordpress_db_password }}'
      login_host: 'localhost'
      state: present

Listo, ejecutamos el playbook y si todo sale bien, deberíamos poder ingresar a
http://nodo1.intranet/wp/ y ver que ya se encuentra Instalado y configurado Wordpress :D .
Contar con esta receta que hemos definido nos ofrece una gran flexibilidad, dado
que podemos con la misma instalar cientos de Wordpress (solo cambiando las variables
que utilizaría cada uno), e incluso podemos destruir por completo el nodo, y
simplemente ejecutando ésta receta, tendríamos todo listo para empezar a utilizar
Wordpress, incluso sin la necesidad de tener que hacer SSH al nodo, ni una sola vez.

Si combinamos esto, con el uso de repositorios, realmente podríamos tener la definición
de casi toda nuestra infraestructura en un repositorio, algo muy ágil y potente. Incluso
si un nuevo administrador se incorpora a trabajar con nosotros, y entiende de  Ansible,
rápidamente puede comprender nuestra infraestructura y no tener que pasarse meses
estudiando como están configuradas las cosas. Ansible propone como su mayor ventaja
(bajo mi exclusivo criterio), la posibilidad de definir con un lenguaje claro y
sencillo nuestra infraestructura.

El playbook final es el siguiente

.. code:: bash

	---
	- hosts: all
	  vars:
	    wordpress_db_name: 'wordpress_db'
	    wordpress_db_user: 'wordpress'
	    wordpress_db_password: 'superpassword'
	    wordpress_db_host: 'localhost'
	    wordpress_content_dir: '/var/lib/wordpress/wp-content'

	  tasks:
	  - name: Instalar Worpress y sus dependencias
	    become: true
	    apt:
	      name: "{{ item }}"
	      state: present
	      update-cache: yes
	    with_items:
	      - wordpress
	      - curl
	      - apache2
	      - mysql-server
	      - python-mysqldb

	  - name: Copiar configuración de Wordpress para apache2
	    become: true
	    copy:
	      src: files/wp.conf
	      dest: /etc/apache2/sites-available/wp.conf
	      owner: root
	      group: root
	      mode: 0644

	  - name: Habilitar sitio de Wordpress (crear enlace simbólico)
	    become: true
	    file:
	      src: /etc/apache2/sites-available/wp.conf
	      dest: /etc/apache2/sites-enabled/wp.conf
	      owner: root
	      group: root
	      state: link
	    notify:
	      - Recargar apache

	  - name: Configurar wordpress
	    become: true
	    template:
	      src: wordpress_config.j2
	      dest:  /etc/wordpress/config-nodo1.intranet.php
	      owner: www-data
	      group: www-data
	      mode: 0644

	  - name: Crear usuario wordpress
	    become: true
	    mysql_user:
	      name: '{{ wordpress_db_user}}'
	      password: '{{ wordpress_db_password }}'
	      priv: '*.*:ALL'
	      state: present

	  - name: Crear base de datos de wordpress
	    become: true
	    mysql_db:
	      name: '{{ wordpress_db_name }}'
	      login_user: '{{ wordpress_db_user }}'
	      login_password: '{{ wordpress_db_password }}'
	      login_host: 'localhost'
	      state: present

	  handlers:
	    - name: Recargar apache
	      become: true
	      service:
	        name: apache2
	        state: reloaded

