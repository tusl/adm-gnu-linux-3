# Directorio scripts #

Este directorio contiene los diferentes scripts utilizados en el proyecto.

- generar-pdfs-unidades: Toma cada archivo .rst del directorio unidades, lo compila y genera su corresondiente archivo.pdf en el directorio pdfs/unidades.
- generar-pdfs-ejercicios: Toma cada archivo .rst del directorio ejercicios, lo compila y genera su corresondiente pdf en el directorio pdfs/ejercicios.
- genera-todo: invoca a los dos scripts anteriores para regenerar todo.

## Como invocarlos ##

Se deben invocar desde dentro de la carpeta scripts

```bash
cd scripts
./genera-todo
```

### Dependencias ###
Para la compilación se requiere de las siguientes dependencias:

- rst2pdf
- python-reportlab
