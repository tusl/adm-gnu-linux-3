# Acerca del repositorio #

Repositorio de la materia Administración GNU/Linux III, perteneciente a la Tecnicatura Universitaria en Software Libre de la Facultad de Ingeniería y Ciencias Hídricas - Universidad Nacional del Litoral.

La materia cuenta con 6 Unidades, cuyos contenidos son teóricos y prácticos.  Las mismas son las siguientes:

- Unidad 1: Sistemas de almacenamiento de datos (RAID y LVM).
- Unidad 2: Configuración y políticas de firewall.
- Unidad 3: Redes privadas virtuales.
- Unidad 4: Monitorización de servicios y recursos.
- Unidad 5: Virtualización.
- Unidad 6: Administración centralizada.

Todo el material es distribuido bajo licencia Creative Commons.

## Estructura de directorios ##

El repositorio se encuentra dividido en diferentes directorios para organizar el contenido de la materia, la cual esta redactada en el formato [reStructuredText](http://docutils.sourceforge.net/rst.html), y luego es convertida a PDF mediante unos scripts. A continuación se detallan los directorios y sus función dentro del respositorio.

- **ejercicios**: contiene los archivos .rst pertenecientes a los ejercicios de cada unidad. La idea es que exista un solo archivo por unidad.
- **estilos**: contiene el/los archivos de estilos utilizados para la compilación a PDF.
- **imágenes**: contiene una carpeta por cada unidad, y dentro de estas se encuentran las diferentes imagenes utilizadas y referenciadas en las unidades.
- **pdfs**: contiene dos carpetas (ejercicios y unidades). Dentro de estas son generados los archivos PDFs finales, con los ejercicios por un lado y las unidades por otro.
- **scripts**: Contiene 3 scripts, uno utilizado para generar solo los PDFs de las unidades, o tro para generar los pdfs de los ejercicios, y uno que invoca a los dos anteriores para generar todo. Estos scripts buscan dentro de la carpeta unidades y ejercicios, los diferentes archivos .rst, los van compilando y guardando en la carpeta _pdfs_ correspondiente. Para la compilación se utiliza el archivo tusl.style de la carpeta estilos.
- **unidades**: contiene un archivo por cada unidad, en formato reStructuredText.

NOTA: El archivo apunte-completo-NO\_EDITAR.rst es generado automaticamente en base a las diferentes unidades. NO MODIFIQUE este archivo, porque será sobreescrito cuando se ejecute el script _generar-pdf-apunte-completo_.

## Como compilar el proyecto ##

Para regenerar toda el contenido de la materia, se debe ejecutar el script generar-todo de la carpeta scripts de la siguiente manera, previo a cumplir con las siguientes dependencias:

### Dependencias ###

- rst2pdf
- python-reportlab

### Compilación ###
```bash
cd scripts/
./generar-todo
```
El material sera generado dentro de la carpeta _pdfs_.
