Cuestionario Unidad 5.1
-----------------------

1- ¿Qué es la consolidación?

2- ¿Qué es el Hypervisor o VMM (Virtual Machine Monitor)?

3- Mencione los 4 recursos principales que gestiona un Hypervisor.

4- ¿Qué es la paravirtualización, y cuales es su principal ventaja y cual su
mayor desventaja?

5- ¿En qué consiste la técnica de "Virtualización basada en Sistema Operativo"?

Preguntas extras
----------------

1- ¿Cual es la función del Dom0 de XEN?

2- ¿Qué son los DomUs en XEN?

3- ¿Cuál es la principal ventaja de la Full Virtualización que ofrece XEN?

4- ¿KVM soporte ParaVirtualización?

5- ¿Cómo se ejecuta KVM en el sistema operativo anfitrión?
