Cuestionario Unidad 4.1
-----------------------

1- Si deseo monitorear el I/O de un equipo en particular. Que herramientas de
las de monitoreo de un host debería utilizar?

2- Que es un trigger en Zabbix?

3- Enumere las ventajas y desventajas del monitoreo interno y del monitoreo externo.

4- Que es SNMP?

5- Que son los agentes de monitoreo, o agentes de obtención de datos?

Preguntas extras
----------------

1- Si deseo vizualizar las conexiones entrantes y salientes de un equipo. Que
herramienta de las de monitoreo de un host debería utilizar?.

2- Que es un escenario web en zabbix?

3- En zabbix, cual es la diferencia entre una notificación y una acción?

4- Que es un template en Zabbix y que beneficios aporta?

5- Si deseo monitorear un equipo de red en zabbix, que protocolo necesito habilitar
en estos?
