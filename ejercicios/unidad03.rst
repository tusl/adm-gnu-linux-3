Cuestionario Unidad 3.1
-----------------------

1- Las información transmitida por medio de una VPN, viaja necesariamente
encriptada.

2- ¿Cual es la diferencia entre las VPNs router a router y las VPNs firewall a
firewall?. Cuando utilizaría una u otra. De un ejemplo de cada una.

3- ¿Cual es la diferencia entre las VPNs de acceso remoto y las VPNs over LAN?.

4- Las interfaces TUN se utilizan para:

  a) Túneles a nivel de IP.

  b) Túneles a nivel de Ethernet.

  c) Para túneles en general.

5- ¿Que es la integridad en un túnel VPN?.


Preguntas extras
----------------

1- ¿Para que se utilizan el par de claves públicas y privadas?

  a) Para garantizar confidencialidad, integridad,
  autenticación y no repudio.

  b) Para cifrar el túnel de la VPN.

  c) Para autenticar los extremos del túnel.

2- ¿Cual es la función de una Autoridad Certificante?

  a) Emitir el certificados

  b) Emitir el par de claves públicas y privadas.

  c) Encargado de garantizar que las claves sean las correctas.

3- ¿Cual es la diferencia entre las autoridades certificantes públicas y las privadas?

4- ¿Para que se utiliza el paquete Easy-RSA?

5- ¿Que es lo que puede cambiar sin afectar la conexión, en una VPN móvil?
