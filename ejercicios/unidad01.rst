Cuestionario Unidad 1.1
-----------------------
1- Describa con sus palabras como funciona un RAID5.

2- ¿Que es la paridad y qué desventajas presenta en implementaciones por
software?

3- ¿Cual es la diferencia entre un RAID0 y un RAID1?

4- Si posee 5 discos de 500Gb y desea agruparlos en un único RAID5, sin
discos de spare. ¿Que capacidad de almacenamiento estaría disponible
para su uso?

5- ¿Que es el modo degradado?

Preguntas extras
----------------
1- Supongamos que cuenta con 6 discos de 1TB cada uno, y desea contar
con doble redundancia, es decir, que el RAID pueda soportar que dos
discos fallen y aún seguir funcionando. Si implementa RAID5, ¿cual
sería la capacidad total disponible?, ¿cuantos discos se utilizarían de
spare?

2- ¿Que es un volumen lógico?

3- ¿Que es un volúmen físico?

Ejercicios
----------
1- Investigue los niveles RAID 6 y RAID 1+0, y comente las ventajas y
desventajas de los mismos, y donde sería recomendable el uso de uno u
otro.
