Cuestionario Unidad 6.1
-----------------------

1- ¿Qué es la administración centralizada y que ventajas ofrece?

2- ¿Qué es la Infraestructura como cómo código?

3- ¿Qué es un playbook en Ansible?

4- ¿Qué es la Idempotencia?

5- ¿Qué es un handler en Ansible?


Preguntas extras
----------------

1- ¿Comó se comunica Ansible con los nodos?

2- ¿Que es Ansible?. Mencione sus principales características.

3- ¿Qué es un módulo en Ansible?

4- ¿Qué es el inventario en Ansible?

5- ¿En que formato se escriben las recetas en Ansible?


Tarea extra
-----------

1- Investigue sobre ansible-galaxy en https://galaxy.ansible.com/, y comente
que son los roles.

2- Elija un módulo de http://docs.ansible.com/ansible/latest/list_of_all_modules.html
que le interese, y comente brevemente su función.
