Cuestionario Unidad 2.1
-----------------------

1- ¿Que es la política por defecto de un firewall?.

2- Enumere los tipos de políticas por defecto.

3- ¿Que opción es la correcta?.

    a) Las tablas están compuestas por reglas, que contienen cadenas y estas
    definen que acción realizar.

    b) Las tablas están formadas por cadenas, las cadenas por reglas y cada
    regla contienen una acción.

    c) Las tablas agrupa un conjunto de reglas, que determinan las acciones a
    realizar.


4- Si un paquete tiene como origen  y destino un host diferente del
firewall y deseamos filtrar este tipo de paquetes.
¿En que tabla y cadena debo agregar dicha regla?.

5- ¿En que lugares de la red es habitual ubicar el firewall? (seleccione 2).

    a) Antes de los servidores web.

    b) Por delante de los servidores de bases de datos.

    c) Como nexo entre la red interna y la externa.

    d) Como nexo entre 2 redes.

Preguntas extras
----------------

1- Si deseo modificar los paquetes recibidos por el firewall para aplicar
una redirección de puertos, ¿en que tabla y cadena debo aplicar dicha regla?.

2- ¿Se pueden crear y utilizar cadenas que no sean las estándares?.

3- Enumere las cadenas de la tabla Filter.

4- Enumere las cadenas de la tabla NAT.

5- ¿Cual es la diferencia entre DROP y REJECT.

6- Se desea rechazar el tráfico saliente desde la red interna a internet,
para el servicio SMTP (puerto 25), a excepción del propio host del servidor
de correos, que se encuentra dentro de la intranet. En este caso que debo
aplicar primero:

    a) La regla que habilita al servidor de correo.

    b) La regla que rechaza el uso del puerto 25 para la red interna.


Ejercicios
----------

1- Se desea filtrar el tráfico entrante desde internet a la red interna, para los
servicios SSH (puerto TCP/22) y netbios (puertos TCP 137/138/139). Escriba las
reglas de iptables necesarias para conseguir esto.

2- Se debe permitir que los hosts de la subred de administración 10.0.1.0/24
puedan acceder a cualquier hosts en cualquier red. Los hosts de la red de
auditoria 10.0.2.0/24 deben acceder a los hosts de la red de contabilidad (
subred 10.0.3.0/24) y compras (subred 10.0.4.0/24). El resto de los hosts de
la red solo puede acceder a cualquier red, en los puertos TCP 80 (http), TCP 443
(https), UPD 53 (DNS) . Escriba el conjunto de reglas de iptables que permita esto.
